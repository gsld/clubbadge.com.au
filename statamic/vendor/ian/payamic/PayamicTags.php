<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Tags;

use Statamic\API\GlobalSet;
use Statamic\API\User;

use Session;
use Request;
use Log;

use DateTime;
use DateInterval;

class PayamicTags extends Tags
{
    private $common;
    private $error_return = '';             // TODO: Work out where to redirect on a config error

    public function __construct(Common $common)
    {
        $this->common = $common;
    }
    
    /**
     * The {{ payamic }} tag
     *
     * @return string|array
     */
    public function index()
    {
        return "";
    }

    /**
     * The {{ payamic:get_result }} tag
     *
     * @return array
     */
    public function getResult()
    {
        $result = json_decode(Session::get('payamic'), TRUE);
        if (!is_null($result)) {
            return $this->parse($result);
        } else {
            return $this->parse(array('payment' => 'empty'));
        }
    }

    /**
     * The {{ payamic:getUserTransactions }} tag
     *
     * @return array
     */
    public function getUserTransactions()
    {
        // Get our transactions
        $trans = $this->common->getTransactions();
        // Filter this array by our user
        $out = array_reverse(array_filter($trans, [$this, 'userFilter']));
        if (empty($out)) {
            return $this->parseNoResults();
        }
        return $this->parseLoop($out);
    }

    /**
     * The {{ payamic:getStripePubKey }} tag
     *
     * @return string
     */
    public function getStripePubKey()
    {
        // Load our global vars
        if (!$this->common->loadVars()) {
            // There was an error loading our vars so crash
            Session::flash('payment_errors', 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
            return 'Error!';
        }
        return $this->common->stripe_pub_key;
    }
    
    /**
     * The {{ payamic:getLevy }} tag
     *
     * @return string
     */
    public function getLevy()
    {
        $val = $this->getConfig($this->getParam('gateway').'_levy');
        return (isset($val) && $val != '' && floatval($val) > 1 ? floatval($val) : 0);
    }
    
    /**
     * The {{ payamic:getMinimum }} tag
     *
     * @return string
     */
    public function getMinimum()
    {
        return $this->getConfig($this->getParam('gateway').'_minimum');
    }
    
    /**
     * The {{ payamic:getCountry }} tag
     *
     * @return string
     */
    public function getCountry()
    {
        $countries = $this->getConfig('countries');
        return isset($countries[strtoupper($this->getParam('code'))]) ? $countries[strtoupper($this->getParam('code'))] : '(unknown)';
    }
    
    /**
     * The {{ payamic:getCountryOptions }} tag
     *
     * @return string
     */
    public function getCountryOptions()
    {
        $countries = $this->getConfig('countries');
        $out = '';
        foreach ($countries as $key => $val) {
            $out .= '<option value="'.strtolower($key).'"'.($key == 'AU' ? ' selected' : '').'>'.$val.'</option>'."\n";
        }
        return $out;
    }
    
    /**
     * The {{ payamic:action }} tag
     *
     * @return string
     */
    public function action()
    {
        return $this->actionUrl('checkout');
    }
    
    /**
     * The {{ payamic:getPaymentStr }} tag
     * @param  amount   The amount for this payment
     * @param  desc     The description for this payment
     * @param  item_id  The Item ID for this payment
     *
     * @return string
     */
    public function getPaymentStr()
    {
        $amount = $this->getParam('amount');
        $desc = $this->getParam('desc');
        $item_id = $this->getParam('item_id');
        
        // TODO: Need to validate the values above
        $str = $this->common->createPaymentStr($amount, $item_id, $desc);
        return $str;
    }

    /**
     * The {{ payamic:getMemberRenewStr }} tag
     * @param  user_id  The user ID for this payment
     *
     * @return string
     */
    public function getMemberRenewalStr()
    {
        $user = User::find($this->getParam('user_id'));
        if (is_null($user)) {
            return "ERROR: User not found";
        }
        $type = $user->get('membership_type');
        $prices = $this->common->getMembershipPrices();
        $amount = $prices[$type]['price'];
        $desc = ucfirst($type).' Membership Renewal';
        $item_id = $type;
        
        $str = $this->common->createPaymentStr($amount, $item_id, $desc);
        return $str;
    }

    /**
     * The {{ payamic:getPaymentURL }} tag
     *
     * @return string
     */
    public function getPaymentURL()
    {
        $amount = $this->getParam('amount', Request::input('amount'));
        $desc = $this->getParam('desc', Request::input('desc'));
        $item_id = $this->getParam('item_id', Request::input('item_id'));

        return $this->common->getPaymentURL($amount, $desc, $item_id, ($this->getParam('redirect') == 'true'));
    }

    /**
     * The {{ payamic:getCustomPaymentURL }} tag
     *
     * @return string
     */
    public function getCustomPaymentURL()
    {
        return $this->common->getCustomPaymentURL($this->getParam('redirect') == 'true');
    }

    /**
     * The {{ payamic:getPaymentDetails }} tag
     *
     * @return string|array
     */
    public function getPaymentDetails()
    {
        // Get our hidden payment field(s) - currently just "payment"
        $segments = Request::segments();
        $payment = array_pop($segments);
        $vars = $this->common->checkPayment($payment);
        if ($vars !== FALSE) {
            $vars['error'] = '';
        } else {
            $vars = array(
                'amount' => 'Unknown',
                'desc' => 'Unknown',
                'item_id' => 'Unknown',
                'error' => "ERROR: The payment details are missing or invalid."
            );
        }
        $vars['payment'] = $payment;
        return $vars;
    }

    /**
     * The {{ payamic:getPaymentErrors }} tag
     *
     * @return string|array
     */
    public function getPaymentErrors()
    {
        return Session::get('payment_errors');
    }

    /**
     * The {{ payamic:setReferrer }} tag
     *
     * @return string|array
     */
    public function setReferrer()
    {
        $this->common->setReferrer();
    }

    /**
     * The {{ payamic:getReferrer }} tag
     *
     * @return string|array
     */
    public function getReferrer()
    {
        return Session::get('payment_referrer');
    }

    /**
     * The {{ payamic:regoDue }} tag
     *
     * @return bool
     */
    public function regoDue()
    {
        // Check if the current user's renewal date is within a month of now
        $inUser = $this->getParam('user');
        $user = $inUser != '' ? User::find($inUser) : User::getCurrent();
        if (!is_null($user)) {
            $renewal = new DateTime($user->get('valid_until'));
            return (time() > $renewal->sub(new DateInterval('P30D'))->format('U'));
        } else {
            return FALSE;
        }
    }
    
    /**
     * Function for filtering transactions by the current user
     *
     * @param  $trans   Transaction to test
     * @return bool
     */
    private function userFilter($trans) {
        // Check if this transaction is for this user
        $user = User::getCurrent();
        return (isset($trans['user']) && $trans['user'] == $user->getAuthIdentifier());
    }
}
