<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Controller;

use Statamic\API\User;

use Session;
use Log;

class PayamicController extends Controller
{
    private $common;

    public function __construct(Common $common)
    {
        $this->common = $common;
    }

    /**
     * Show our list of transactions
     *
     * @return view
     */
    public function index()
    {
        // Get our transactions
        $transactions = $this->common->getTransactions();
        // Build our view
        return $this->view('index', ['title' => 'Payamic', 'transactions' => array_reverse($transactions)]);
    }

    /**
     * Show the details of the selected transaction
     *
     * @return view
     */
    public function detail()
    {
        // Get our ID
        $id = request()->input('id');
        // Get our transactions
        $transactions = $this->common->getTransactions();
        // Build our view
        return $this->view('detail', ['title' => 'Transaction', 'id' => $id, 'transaction' => isset($transactions[$id]) ? $transactions[$id] : null]);
    }

    /**
     * Return our list of transactions as either a CSV or JSON file
     *
     * @return response
     */
    public function export()
    {
        // Get our transactions
        $transactions = $this->common->getTransactions();
        $tmpfname = tempnam(sys_get_temp_dir(), 'sta');
        // Build the output - TODO
        $type = request()->input('type');
        if ($type == 'json') {
            file_put_contents($tmpfname, json_encode($transactions));
        } else {
            // Build our CSV file
            $type = 'csv';
            $fp = fopen($tmpfname, 'w');
            if (isset($transactions[0])) {
                fputcsv($fp, array_keys($transactions[0]));
            }
            foreach ($transactions as $fields) {
                fputcsv($fp, $fields);
            }
            fclose($fp);
        }
        // Serve and delete our temp file
        return response()->download($tmpfname, 'transactions.'.$type)->deleteFileAfterSend(true);
    }

    /**
     * Our custom (variable price) front-end function - /!/Payamic/custom
     *
     * @return mixed
     */
    public function getCustom()
    {
        // Set our referrer and then redirect to the payment page with a custom payment
        $this->common->setReferrer();
        return $this->common->getCustomPaymentURL(TRUE);
    }

    /**
     * Our payment URL front-end function (for Ajax submissions requiring payment redirect) - /!/Payamic/paymenturl
     *
     * @return mixed
     */
    public function getPaymentUrl()
    {
        // Return the link to the payment page with a custom payment
        $amount = request()->input('amount');
        $desc = request()->input('desc');
        $item_id = request()->input('item_id');

        return json_encode(array('url' => $this->common->getPaymentURL($amount, $desc, $item_id, FALSE)));
    }

    /**
     * Our checkout front-end function - /!/Payamic/checkout
     *
     * @return mixed
     */
    public function postCheckout()
    {
        // Load our vars
        if (!$this->common->loadVars()) {
            // There was an error loading our vars so crash
            return $this->handleError('ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
        }

        // Check we have the necessary fields (gateway and payment)
        $payment = request()->input('payment');
        $gateway = request()->input('payment_gateway');
        $quantity = request()->input('quantity', 1);
        $delivery = request()->input('shipping_option');
        $name = $this->common->getName(request()->input());
        $email = request()->input('email');
        $summary = request()->input('summary');
        $form = request()->input('email_template', $this->common->email_template_payment_default);

        if (is_null($payment) || $payment == '' || is_null($gateway) || $gateway == '') {
            // We are missing data so error
            return $this->handleError('ERROR: Payment details missing');
        }
        // Check we have a valid payment
        $payment = $this->common->checkPayment($payment);
        if ($payment === FALSE) {
            // Handle the error
            return $this->handleError('ERROR: Payment details missing');
        }

        if ($payment['amount'] == 'custom') {
            $payment['item_id'] = 'custom';
            $payment['amount'] = request()->input('amount');
            if (!is_numeric($payment['amount']) || floatval($payment['amount']) <= 0) {
                // This isn't a valid amount so error
                return $this->handleError('ERROR: Invalid payment amount entered');
            }
            $payment['amount'] = number_format($payment['amount'], 2);
            $payment['desc'] = request()->input('desc');
            if ($payment['desc'] == '') {
                // We need a description so error
                return $this->handleError('ERROR: Please enter a description');
            }
        }

        // Adjust our amount based on the quantity
        $price = $payment['amount'];
        $payment['amount'] = $payment['amount'] * $quantity;
        // Add our delivery fee if necessary
        if ($delivery == 'free') {
            $delivery = 0;
        } elseif ($delivery == 'custom') {
            $delivery = floatval(request()->input('custom_delivery'));
        } else {
            $delivery = floatval($delivery);
        }
        $payment['amount'] += $delivery;

        if ($gateway == 'stripe' && $payment['amount'] < $this->getConfig('stripe_minimum')) {
            // The amount of this payment is below our minimum
            return $this->handleError('ERROR: Payment amount is below the minimum for this gateway');
        }

        // And any gateway levy
        if ($this->getConfig($gateway.'_levy') != '' && floatval($this->getConfig($gateway.'_levy')) > 0) {
            $payment['amount'] = $payment['amount'] + ($payment['amount'] * floatval($this->getConfig($gateway.'_levy')) / 100);
        }

        // Trim our payment to cents
        $payment['amount'] = round($payment['amount'], 2);

        // Find our user
        if (stripos($payment['desc'], 'Membership Renewal') !== FALSE) {
            $user = User::find(request()->input('user_id'));
            if (is_null($user)) {
                $user = User::getCurrent();
            }
        } else {
            $user = User::getCurrent();
        }

        // Record our fields so far
        $this->common->setPaymentDetail('user', ($user ? $user->getAuthIdentifier() : 'guest'));
        $this->common->setPaymentDetail('name', $name);
        $this->common->setPaymentDetail('email', $email);
        $this->common->setPaymentDetail('gateway', $gateway);
        $this->common->setPaymentDetail('amount', $payment['amount']);
        $this->common->setPaymentDetail('desc', $payment['desc']);
        $this->common->setPaymentDetail('summary', $summary);
        $this->common->setPaymentDetail('item_id', $payment['item_id']);
        $this->common->setPaymentDetail('quantity', $quantity);
        $this->common->setPaymentDetail('price', $price);
        $this->common->setPaymentDetail('delivery', $delivery);
        $this->common->setPaymentDetail('gw_levy', $this->getConfig($gateway.'_levy', 0));
        $this->common->setCustomFields();

        // Process the payment
        $result = FALSE;
        if ($gateway == 'offline') {
            // Just store the details and redirect to the success page
            $this->common->setPaymentDetail('timestamp', time());
            $this->common->setPaymentDetail('trans_id', 'off-'.time());
            $this->common->setTransCount();
            $result = TRUE;
        } elseif ($gateway == 'stripe') {
            // Check that we have our Stripe token
            $token = request()->input('stripe_token');
            if (is_null($token) || $token == '') {
                // We are missing data so error
                return $this->handleError('ERROR: Payment details missing');
            }
            // Process our payment
            $result = $this->common->processStripe($payment, $token);
        } else {
            // wtf is this?
            return $this->handleError('ERROR: Invalid payment details');
        }

        Session::flash('payamic', json_encode($this->common->payment_details));
        if ($result) {
            // Record this transaction in the log
            $this->common->recordTrans();
            // Send some notification emails
            $this->common->sendEmails($form);
            // Check if this is a subscription payment and if so, update the user profile
            if (stripos($payment['desc'], 'Membership Renewal') !== FALSE) {
                // Update the user profile
                $this->common->updateUser($payment['item_id'], $user, $gateway);
            }
            // Check if this is a booking payment and if so, create the booking
            if (stripos($payment['desc'], 'Booking') !== FALSE) {
                // Update the user profile
                $booking = [
                  'property' => request()->input('property'),
                  'start_date' => request()->input('start_date'),
                  'end_date' => request()->input('end_date'),
                ];
                $this->common->createBooking($booking, $gateway);
            }
            // Redirect to our success page
            if (request()->ajax()) {
                return json_encode(array_merge($this->common->payment_details, ['result' => 'success']));
            } else {
                return redirect(request()->input('payment_success', '/'.$this->common->page_success));
            }
        } else {
            // Redirect to our error page
            if (request()->ajax()) {
                return json_encode(array_merge($this->common->payment_details, ['result' => 'error', 'errors' => Session::get('payment_errors')]));
            } else {
                return redirect(request()->server('HTTP_REFERER'));
            }
        }
    }

    private function handleError($error) {
        Log::error($error);
        if (request()->ajax()) {
            return $error;
        } else {
            Session::flash('payment_errors', $error);
            return redirect(request()->server('HTTP_REFERER'));
        }
    }
}
