<?php

namespace Statamic\Addons\Payamic;

use DateTime;
use DateInterval;
use Session;
use Request;
use Log;

use Statamic\API\Email;
use Statamic\API\Folder;
use Statamic\API\GlobalSet;
use Statamic\API\User;
use Statamic\Data\Entries\EntryFactory;

use Statamic\Extend\Extensible;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error;
use Stripe\Subscription;

class Common
{
    use Extensible;

    public $globals = array();
    public $prices = array();
    public $building_levies = array();
    public $admin_email = '';
    public $admin_name = '';
    public $from_email = '';
    public $from_name = '';
    public $bcc_email = '';
    public $site_name = '';
    public $email_template_payment_default = '';
    public $email_template_subscription_success = '';
    public $email_template_subscription_reminder = '';
    public $page_checkout = '';
    public $page_success = '';
    public $stripe_priv_key = '';
    public $stripe_pub_key = '';
    public $error = '';

    // Set up our initial array of payment detail fields to ensure they exist
    public $payment_details = array(
        'name' => '',
        'email' => '',
        'amount' => '',
        'desc' => '',
        'item_id' => '',
        'gateway' => '',
        'timestamp' => '',
        'trans_id' => '',
        'gateway_vars' => '',
    );

    public function encode($str)
    {
        return base64_encode($str);
    }

    public function decode($str)
    {
        return base64_decode($str);
    }

    public function getCustomPaymentURL($redirect)
    {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            Session::flash('payment_errors', 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
            return 'Error!';
        }

        // Build our URL
        $url = '/'.$this->page_checkout.'/'.$this->createPaymentStr('custom', 'custom', 'Custom Payment');
        if ($redirect) {
            return redirect($url);
        }
        return $url;
    }

    public function getPaymentURL($amount, $desc, $item_id, $redirect) {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            Session::flash('payment_errors', 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
            return 'Error!';
        }

        // TODO: Need to validate the values above
        $url = '/'.$this->page_checkout.'/'.$this->createPaymentStr($amount, $item_id, $desc);
        if ($redirect) {
            return redirect($url);
        }
        return $url;
    }

    public function setReferrer()
    {
        // Get our referrer and trim it back to just the domain
        $referrer = Request::server('HTTP_REFERER');
        $start = strpos($referrer, '://') + 3;
        $referrer = substr($referrer, $start > 3 ? $start : 0);
        $referrer = substr($referrer, 0, strpos($referrer, '/'));
        Session::flash('payment_referrer', $referrer);
    }

    public function checkPayment($str)
    {
        $payment = array();
        // Check if our input string is an actual payment and unpack the details
        if (!isset($str) || $str == '') {
            return FALSE;
        }
        $decoded = $this->decode($str);
        // Check this is an encoded payment string (contains the /// seperator)
        if (strpos($decoded, '///') === FALSE) {
            return FALSE;
        }
        list($price, $item_id, $desc) = explode('///', $decoded);
        if (!$this->validatePayment($price, $desc)) {
            // This isn't a valid payment so error
            return FALSE;
        }
        $payment['amount'] = ($price == 'custom' ? $price : number_format($price, 2, '.', ''));
        $payment['desc'] = $desc;
        $payment['item_id'] = $item_id;
        return $payment;
    }

    public function createPaymentStr($price, $item_id, $desc = "")
    {
        // Check our inputs are reasonable (non-empty with positive float price)
        if (!$this->validatePayment($price, $desc)) {
            // This isn't a valid payment so error
            return FALSE;
        }

        // Create our payment string from the input details
        return $this->encode($price.'///'.$item_id.'///'.$desc);
    }

    public function validatePayment($price, $desc)
    {
        // Check our inputs are reasonable (non-empty with positive float price or special "custom" price)
        if ($price == '' || (floatval($price) <= 0 && $price != 'custom') || $desc == '') {
            return FALSE;
        }
        return TRUE;
    }

    public function loadVars()
    {
        // Load our vars if necessary
        if (!isset($this->admin_email) || $this->admin_email == '') {
            // Check we have an admin email address
            $this->admin_email = $this->getConfig('admin_email');
            if (!isset($this->admin_email) || $this->admin_email == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the site email address from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
            // Load a pretty name for the from address and BCC email if they exist
            $this->admin_name = $this->getConfig('admin_name');
            $this->bcc_email = $this->getConfig('bcc_email');
            $this->from_email = $this->getConfig('from_email');
            $this->from_name = $this->getConfig('from_name');

            // Check we have a site name
            $this->site_name = $this->getConfig('site_name');
            if (!isset($this->site_name) || $this->site_name == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the site name from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }

            // Check we have our email templates
            $this->email_template_payment_default = $this->getConfig('default_payment_email_template_prefix');
            $this->email_template_subscription_success = $this->getConfig('subscription_success_email_template_prefix');
            $this->email_template_subscription_due = $this->getConfig('subscription_reminder_email_template_prefix');
            if (!isset($this->email_template_payment_default) || $this->email_template_payment_default == ''
                    || !isset($this->email_template_subscription_success) || $this->email_template_subscription_success == ''
                    || !isset($this->email_template_subscription_due) || $this->email_template_subscription_due == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the notification email templates from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
            // Check we have our Stripe Keys
            $this->stripe_priv_key = $this->getConfig('stripe_priv_key', NULL, NULL, FALSE, FALSE);
            $this->stripe_pub_key = $this->getConfig('stripe_pub_key', NULL, NULL, FALSE, FALSE);
            if (!isset($this->stripe_priv_key) || $this->stripe_priv_key == ''
                    || !isset($this->stripe_pub_key) || $this->stripe_pub_key == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the Stripe keys from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
            // Check we have our checkout and success pages
            $this->page_checkout = $this->getConfig('page_checkout');
            $this->page_success = $this->getConfig('page_success');
            if (!isset($this->page_checkout) || $this->page_checkout == ''
                    || !isset($this->page_success) || $this->page_success == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the default checkout and success pages from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
        }
        return TRUE;
    }

    public function sendAdminEmail($data, $title, $addy = NULL)
    {
        if (!isset($addy) || $addy == '') {
            $addy = (isset($this->admin_email) && $this->admin_email != '' ? $this->admin_email : 'payamic@gsld.com.au');
        }
        $email = Email::create();
        $email->to($addy)
            ->subject($title)
            ->with($data)
            ->automagic(TRUE);
        if ($this->from_name != '') {
            $email->from($this->from_email, $this->from_name);
        } elseif ($this->admin_name != '') {
            $email->from($addy, $this->admin_name);
        } else {
            $email->from($addy);
        }
        if ($this->bcc_email != '') {
            $email->bcc($this->bcc_email);
        }
        $email->send();
    }

    public function setPaymentDetail($var, $val) {
        $this->payment_details[$var] = $val;
    }

    public function setTransCount() {
        // Get our transaction count
        $count = intval($this->storage->get('order_counter', 100)) + 1;
        // Set the detail
        $this->setPaymentDetail('trans_count', $count);
        // Save the new count
        $this->storage->put('order_counter', $count);
    }

    public function setCustomFields() {
        foreach ($this->getConfig('fields') as $field) {
            $val = Request::input($field['name']);
            $this->setPaymentDetail($field['name'], $val);
        }
    }

    // Trim all the data
    static function trimArray(&$item, $key) {
        $item = trim($item);
    }

    public function recordTrans()
    {
        array_walk_recursive($this->payment_details, 'self::trimArray');
        // Store this transaction
        $this->storage->putYAML('trans-'.date('YmdHis', time()), $this->payment_details);
    }

    public function sendEmails($form = NULL)
    {
        $this->setPaymentDetail('time', date('Y M d H:i:s', $this->payment_details['timestamp']));
        if (is_null($form) || $form == '') {
            $form = $this->default_email_template;
        }
        // Send a success email to the customer if we have an email address for them
        $email_addy = $this->payment_details['email'];
        if ($email_addy != '') {
            $email = Email::create();
            $email->to($email_addy)
                ->subject(request()->input('success_subject', $this->site_name.' Payment Successful'))
                ->with(array_merge(
                    $this->payment_details,
                    request()->input()
                ))
                ->template($form.'-success');
            if ($this->from_name != '') {
                $email->from($this->from_email, $this->from_name);
            } elseif ($this->admin_name != '') {
                $email->from($this->admin_email, $this->admin_name);
            } else {
                $email->from($this->admin_email);
            }
            if ($this->bcc_email != '') {
                $email->bcc($this->bcc_email);
            }
            $email->send();
        }
        // Send a success email to the site admins
        $email = Email::create();
        $email->to($this->admin_email)
            ->subject(request()->input('admin_subject', 'Successful Payment by '.$this->payment_details['name']))
            ->with(array_merge(
                $this->payment_details,
                request()->input()
            ))
            ->template($form.'-admin');
        if ($this->from_name != '') {
            $email->from($this->from_email, $this->from_name);
        } elseif ($this->admin_name != '') {
            $email->from($this->admin_email, $this->admin_name);
        } else {
            $email->from($this->admin_email);
        }
        if ($this->bcc_email != '') {
            $email->bcc($this->bcc_email);
        }
        $email->send();
    }

    public function processStripe($payment, $token) {
        // Set our secret key
        Stripe::setApiKey($this->stripe_priv_key);

        // Create the charge on Stripe's servers - this will charge the user's card
        $success = TRUE;
        $fatal = FALSE;
        $error = '';
        try {
            $charge = Charge::create(array(
                "amount" => $payment['amount'] * 100, // amount in cents, again
                "currency" => $payment['currency'] ?? 'aud',
                "source" => $token,
                "description" => $payment['desc'].' for '.$this->payment_details['email'],
                "metadata" => array('name' => $this->payment_details['name'], 'email' => $this->payment_details['email'])
            ));
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            $success = FALSE;
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error = 'ERROR: An error occurred processing the payment. The returned message was: '.$err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an invalid data error. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error authenticating with the payment server. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error contacting the payment server.';
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        }
        if ($success) {
            // Build our data set
            $this->setPaymentDetail('timestamp', $charge['created']);
            $this->setPaymentDetail('trans_id', 'str-'.$charge['id']);
            $this->setTransCount();
            $this->setPaymentDetail('gateway_vars', array('card' => array(
                'type' => $charge['source']['brand'],
                'account' => $charge['source']['funding'],
                'ending' => $charge['source']['last4']
            )));
            // Return success
            return TRUE;
        } else {
            // Check if this was a user or system error
            if ($fatal) {
                // Message the site creators with the error message (and extra details?)
                if (method_exists($e, 'getJsonBody')) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                } else {
                    $err['type'] = '[unknown]';
                    $err['message'] = $error;
                }
                $output = array('Payment processing error' => $error,
                    'Amount' => $payment['amount'],
                    'Desc' => $payment['desc'],
                    'Request ID' => (method_exists($e, 'getRequestId') ? $e->getRequestId() : ''),
                    'Status' => (method_exists($e, 'getHttpStatus') ? $e->getHttpStatus()  : ''),
                    'Type' => $err['type'],
                    'Code' => (isset($err['code']) ? $err['code'] : ''),
                    'Param' => (isset($err['param']) ? $err['param'] : ''),
                    'Message' => (isset($err['message']) ? $err['message'] : '')
                );
                // Send our email
                $this->sendAdminEmail($output, 'Payment processing error!', $this->admin_email);
                // Add an extra bit to our returned message
                $error .= ' Your payment has not been processed. Please try again later.';
            }
            Log::error($error);
            Session::flash('payment_errors', $error);
            return FALSE;
        }

    }

    public function processStripeCustomer($name, $email, $token) {
        $success = TRUE;
        $fatal = FALSE;
        $error = '';

        // Set our secret key
        Stripe::setApiKey($this->stripe_priv_key);

        // Create the customer on Stripe's servers - this will store the customer but not charge anything
        try {
            $customer = Customer::create(array(
                "source" => $token,
                "email" => $email,
                "description" => $name
            ));
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            $success = FALSE;
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error = 'ERROR: An error occurred processing the payment. The returned message was: '.$err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an invalid data error. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error authenticating with the payment server. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error contacting the payment server.';
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        }
        if ($success) {
            return $customer;
        } else {
            // Check if this was a user or system error
            if ($fatal) {
                // Message the site creators with the error message (and extra details?)
                if (method_exists($e, 'getJsonBody')) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                } else {
                    $err['type'] = '[unknown]';
                    $err['message'] = $error;
                }
                $output = array('Payment processing error' => $error,
                    'Subscription Plan' => $plan,
                    'Request ID' => (method_exists($e, 'getRequestId') ? $e->getRequestId() : ''),
                    'Status' => (method_exists($e, 'getHttpStatus') ? $e->getHttpStatus()  : ''),
                    'Type' => $err['type'],
                    'Code' => (isset($err['code']) ? $err['code'] : ''),
                    'Param' => (isset($err['param']) ? $err['param'] : ''),
                    'Message' => (isset($err['message']) ? $err['message'] : '')
                );
                // Send our email
                $this->sendAdminEmail($output, 'Payment processing error!', $this->admin_email);
                // Add an extra bit to our returned message
                $error .= ' Your payment has not been processed. Please try again later.';
            }
            Log::error($error);
            return $error;
        }
    }

    public function processStripeSubscription($customer, $plan, $quantity) {
        // Set our secret key
        Stripe::setApiKey($this->stripe_priv_key);

        // Create the charge on Stripe's servers - this will charge the user's card
        $success = TRUE;
        $fatal = FALSE;
        $error = '';
        try {
            $subscription = Subscription::create([
              "customer" => $customer,
              "items" => [
                [
                  "plan" => $plan,
                  "quantity" => $quantity
                ],
              ],
              "metadata" => [
                'name' => $this->payment_details['name'],
                'email' => $this->payment_details['email']
              ]
            ]);
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            $success = FALSE;
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error = 'ERROR: An error occurred processing the payment. The returned message was: '.$err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an invalid data error. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error authenticating with the payment server. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error contacting the payment server.';
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        }
        if ($success) {
            // Build our data set
            $this->setPaymentDetail('timestamp', $subscription['created']);
            $this->setPaymentDetail('trans_id', 'str-'.$subscription['id']);
            $this->setTransCount();
            $this->setPaymentDetail('gateway_vars', array(
                'customer' => $subscription['customer'],
                'subscription' => $subscription['items']['data'][0]['subscription'],
                'quantity' => $subscription['items']['data'][0]['quantity'],
                'plan' => array(
                    'id' => $subscription['plan']['id'],
                    'amount' => $subscription['plan']['amount'],
                    'product' => $subscription['plan']['product'],
                    'interval' => $subscription['plan']['interval'],
                    'interval_count' => $subscription['plan']['interval_count']
                )
            ));
            // Return success
            return TRUE;
        } else {
            // Check if this was a user or system error
            if ($fatal) {
                // Message the site creators with the error message (and extra details?)
                if (method_exists($e, 'getJsonBody')) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                } else {
                    $err['type'] = '[unknown]';
                    $err['message'] = $error;
                }
                $output = array('Payment processing error' => $error,
                    'Customer' => $customer,
                    'Subscription Plan' => $plan,
                    'Request ID' => (method_exists($e, 'getRequestId') ? $e->getRequestId() : ''),
                    'Status' => (method_exists($e, 'getHttpStatus') ? $e->getHttpStatus()  : ''),
                    'Type' => $err['type'],
                    'Code' => (isset($err['code']) ? $err['code'] : ''),
                    'Param' => (isset($err['param']) ? $err['param'] : ''),
                    'Message' => (isset($err['message']) ? $err['message'] : '')
                );
                // Send our email
                $this->sendAdminEmail($output, 'Payment processing error!', $this->admin_email);
                // Add an extra bit to our returned message
                $error .= ' Your payment has not been processed. Please try again later.';
            }
            Log::error($error);
            Session::flash('payment_errors', $error);
            return FALSE;
        }

    }

    public function updateTransaction($data) {
        // Grab our filename and remove it from the array
        $filename = $data['filename'];
        unset($data['filename']);
        // Ensure our updated data is trimmed
        array_walk_recursive($data, 'self::trimArray');
        // Update this transaction
        $this->storage->putYAML($filename, $data);
    }

    public function getTransactions() {
        $transactions = [];
        $gateways = [];
        foreach (GlobalSet::whereHandle('gvcustomform')->get('cf_payment_gateways') as $gateway) {
            $gateways[$gateway['gateway_value']] = $gateway;
        }
        // Get our list of transaction record files
        $files = Folder::disk('storage')->getFiles('/addons/Payamic/');
        // Load just our transactions
        foreach ($files as $file) {
            if (substr($file, 15, 6) == 'trans-') {
                $transaction = $this->storage->getYAML(substr($file, 15, -5));
                $transaction['filename'] = substr($file, 15, -5);
                $transaction['gateway_pretty'] = $gateways[$transaction['gateway']]['gateway_name'] ?? ucfirst($transaction['gateway']);
                $transactions[$transaction['trans_id']] = $transaction;
            }
        }
        return $transactions;
    }

    public function updateUser($in_type = '', $user = NULL, $gateway) {
        // Build a list of our memebership types
        $types = $this->getMembershipPrices();

        // Check if we have a type set
        if (is_null($user)) {
            $user = User::getCurrent();
        }
        $type = $user->get('membership_type');
        if (is_null($type) || $type == '') {
            // There is no type set yet, so set it based on the item
            $user->set('membership_type', $in_type);
            // Set our membership as valid for 1 year from now
            $valid_until = new DateTime();
        } elseif (!isset($types[$type]['anniversary'])) {
            // Calculate our new date as 1 year from the current subscription end
            $valid_until = new DateTime($user->get('valid_until'));
        } else {
            // Calculate our new date as 1 year from the base date
            $valid_until = DateTime::createFromFormat('m-d', $types[$type]['anniversary']);
        }
        $valid_until->add(new DateInterval('P'.$types[$type]['duration']));
        $user->set('valid_until', $valid_until->format('Y-m-d'));
        // Update our user status
        $user->set('member_status', $gateway == 'stripe' ? 'active' : 'pending');
        // Save our updated user
        $user->save();
    }

    public function createBooking($data, $gateway) {
        // Create our new entry with the requested data
        $entry = Statamic\API\Entry::create('my-post')
            ->collection('bookings')
            ->with([
                    'property' => $data['property'],
                    'start_date' => $data['start_date'],
                    'end_date' => $data['end_date'],
                    'paid' => ($gateway == 'stripe' ? true : false)
                ])
            ->get();
        // Save this entry
        $entry->save();
    }

    public function checkSubs($date) {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            $error = 'ERROR! Failure loading our vars. Please check the logs for details.';
            Log::error($error);
            echo $error;
            return;
        }

        // Get our subs reminder times
        $times = explode(',', $this->getConfig('reminder_times', '30D, 14D, 7D'));

        // Get a list of our memebership types and check for anniversary reminders
        $types = $this->getMembershipPrices();
        $bases = [];
        foreach ($types as $type) {
            // Check if this type has a common anniversary and if this is a reminder date
            if (isset($type['anniversary'])) {
                // Check if the input date matches one of the reminder times before our base dates
                $base = DateTime::createFromFormat('m-d', $type['anniversary']);
                echo "Date: ".$base->format('Y-m-d')."\n";
                foreach ($times as $time) {
                    $base_test = clone $base;
                    if ($date->format('Y-m-d') == $base_test->sub(new DateInterval('P'.trim($time)))->format('Y-m-d')) {
                        echo "Test Date: ".$base_test->format('Y-m-d')."\n";
                        $bases[$type['value']] = [
                            'date' => $base,
                            'time' => $time
                        ];
                    }
                }
            }
        }
        print_r($bases);

        // Loop over all the members
        foreach (User::all() as $member) {
            $type = $member->get('membership_type');
            echo $member->username()." - ".$type."\n";
            // Check this is a member type we are checking and if they are active (not expired or with a payment awaiting confirmation)
            if (!array_key_exists($type, $types) || $member->get('member_status') != 'active') {
                echo "Not our type, or inactive, bailing.\n";
                continue;
            }
            $valid_until = new DateTime($member->get('valid_until'));
            echo 'User valid_until: '.$valid_until->format('Y-m-d')."\n";
            // Check for subscription reminders
            if (!isset($types[$type]['anniversary'])) {
                echo "Got normal user\n";
                // Loop over all the reminder dates
                foreach($times as $time) {
                    // Check if $date is $reminder_time before their $valid_until date
                    $reminder = clone $valid_until;
                    $reminder->sub(new DateInterval('P'.trim($time)));
                    if ($date->format('Y-m-d') == $reminder->format('Y-m-d')) {
                        // Send our notification to this member
                        echo "This is ".$member->username()."'s ".$time." reminder date (".$reminder->format('d-m-Y').") so send the reminder.\n";
                        $this->sendSubsReminder($member, $valid_until);
                        break;
                    }
                }
            } else {
                echo "Got special user\n";
                // Check if this is a special reminder day for this member type, and if our valid until date is within 180 days (due or expired)
                if (isset($bases[$type])) {
                    // Send our notification to this member
                    echo "This is a special member (".$member->username()." - ".$type.") and today is the ".$bases[$type]['time']." special reminder day, so send their reminder\n";
                    $this->sendSubsReminder($member, $bases[$type]['date']);
                }
            }
            // Check for expired memberships
            if ($valid_until < $date) {
                echo "Until: ".$valid_until->format('d-m-Y').', Date: '.$date->format('d-m-Y')."\n";
                // Set our member status to "inactive"
                echo $member->username()."'s membership has just expired so flag them inactive and send the reminder.\n";
                $member->set('member_status', 'inactive');
                $member->save();
                $this->sendSubsReminder($member, $valid_until);
            }
        }
    }

    public function sendSubsReminder($member, $date) {
        // Build our email
        print "Sending email with this template: ".$this->email_template_subscription_due."\n";
        $prices = $this->getMembershipPrices();
        $amount = money_format('%.2n', $prices[$member->get('membership_type')]['price']);
        $email = Email::create();
        $email->to($member->email())
            ->subject($this->site_name.' Membership Reminder')
            ->with(array_merge(
                $member->data(),
                array('email' => $member->email(), 'due_date' => $date->format('d-m-Y'), 'amount' => $amount)
            ))
            ->template($this->email_template_subscription_due);
        if ($this->from_name != '') {
            $email->from($this->from_email, $this->from_name);
        } elseif ($this->admin_name != '') {
            $email->from($this->admin_email, $this->admin_name);
        } else {
            $email->from($this->admin_email);
        }
        $email->send();
    }

    /**
     * Search our user list for the current highest user number and increment
     *
     * @return int
     */
    public function findNextUserNum() {
        $users = User::all();
        $num = 1000;
        foreach ($users as $user) {
            if (intval($user->get('user_num')) > $num) {
                $num = intval($user->get('user_num'));
            }
        }
        return $num + 1;
    }

    public function getName($data) {
        // Look for first and last name compoments in common field names
        $name = $data['name'] ?? '';
        if ($name == '') {
            $name = $data['firstname'] ?? '';
        }
        if ($name == '') {
            $name = $data['first_name'] ?? '';
        }
        if (isset($data['surname'])) {
            $name .= ' '.$data['surname'];
        } elseif (isset($data['lastname'])) {
            $name .= ' '.$data['lastname'] ?? '';
        } elseif (isset($data['last_name'])) {
            $name .= ' '.$data['last_name'] ?? '';
        }

        return $name;
    }

    public function getMembershipPrices() {
        $types = [];
        $prices = GlobalSet::whereHandle('gvcustommembers')->get('membership_prices');
        foreach ($prices as $price) {
            $types[$price['value']] = $price;
        }
        return $types;
    }
}
