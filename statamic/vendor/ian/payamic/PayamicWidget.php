<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Widget;

use Statamic\API\Folder;

class PayamicWidget extends Widget
{
    private $common;
    
    public function __construct(Common $common)
    {
        $this->common = $common;
    }
    
    /**
     * The HTML that should be shown in the widget
     *
     * @return string
     */
    public function html()
    {
        // Get our transactions
        $transactions = $this->common->getTransactions();
        // Build our view
        return $this->view('widget', [
            'fields' => $this->get('fields', []),
            'transactions' => array_reverse(array_slice($transactions, -5, 5))
        ]);
    }
}
