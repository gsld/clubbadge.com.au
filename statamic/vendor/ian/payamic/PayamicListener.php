<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Listener;

use Session;
use Log;
use Request;

use Statamic\Data\Users\User;
use Statamic\API\Nav;
use Statamic\API\GlobalSet;
use Statamic\Contracts\Forms\Submission;

class PayamicListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'cp.nav.created' => 'addNavItems',
        'user.registering' => 'handleUserPayment',
        'user.registered' => 'handleUserSuccess',
        'Form.submission.creating' => 'handleFormPayment'
    ];

    private $common;

    public function __construct(Common $common)
    {
        $this->common = $common;
    }

    /**
     * The method for adding our Nav Item
     *
     * @var array
     */
    public function addNavItems($nav)
    {
        // Create the navigation item
        $item = Nav::item('Payamic')->icon('credit')->route('payamic.index');

        // Finally, add our navigation item to the navigation under the 'tools' section.
        $nav->addTo('tools', $item);
    }

    /**
     * The method for processing our payment as part of the user registration process
     *
     * @var array
     */
    public function handleUserPayment(User $user)
    {
        // Check we need to take payment
        if (!$this->getConfig('subscriptions')) {
            return;
        }

        // Load our vars
        if (!$this->common->loadVars()) {
            // There was an error loading our vars so crash
            $error = 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.';
            Log::error($error);
            return ['errors' => ['payamic' => $error]];
        }

        // Check we have the necessary fields (gateway and membership type)
        $type = request()->input('membership_type');
        $gateway = request()->input('payment_gateway');
        $name = $user->get('first_name').' '.$user->get('last_name');
        $email = $user->email();

        if (is_null($type) || $type == '' || is_null($gateway) || $gateway == '') {
            // We are missing data so error
            $error = 'ERROR: Payment details missing';
            Log::error($error);
            return ['errors' => ['payamic' => $error]];
        }
        // Try to find our membership price and error if not found
        $prices = GlobalSet::whereHandle('gvcustommembers')->get('membership_prices');
        foreach ($prices as $price) {
            if ($price['value'] == $type) {
                $payment = [
                    'amount' => round($price['price'], 2),
                    'desc' => 'Membership Registration',
                    'item_id' => $type
                ];
                break;
            }
        }
        if (empty($payment)) {
            // We are missing data so error
            $error = 'ERROR: Invalid membership type';
            Log::error($error);
            return ['errors' => ['payamic' => $error]];
        }

        if ($gateway == 'stripe' && $payment['amount'] < $this->getConfig('stripe_minimum')) {
            // The amount of this payment is below our minimum
            $error = 'Payment error: Payment amount is below the minimum for this gateway';
            Log::error($error);
            return ['errors' => ['payamic' => $error]];
        }

        // Record our fields so far
        $this->common->setPaymentDetail('user', $user->getAuthIdentifier());
        $this->common->setPaymentDetail('name', $name);
        $this->common->setPaymentDetail('email', $email);
        $this->common->setPaymentDetail('gateway', $gateway);
        $this->common->setPaymentDetail('amount', $payment['amount']);
        $this->common->setPaymentDetail('desc', $payment['desc']);
        $this->common->setPaymentDetail('item_id', $payment['item_id']);
        $this->common->setPaymentDetail('quantity', 1);
        $this->common->setPaymentDetail('price', $payment['amount']);
        $this->common->setPaymentDetail('delivery', 0);
        $this->common->setPaymentDetail('gw_levy', 0);
        $this->common->setCustomFields();

        // Process the payment
        $result = FALSE;
        if ($gateway == 'stripe') {
            // Check that we have our Stripe token
            $token = request()->input('stripe_token');
            if (is_null($token) || $token == '') {
                // We are missing data so error
                $error = 'ERROR: Payment details missing - Missing Stripe token';
                Log::error($error);
                return ['errors' => ['payamic' => $error]];
            }
            // Process our payment
            $result = $this->common->processStripe($payment, $token);
        } else {
            // Just store the details
            $this->common->setPaymentDetail('timestamp', time());
            $this->common->setPaymentDetail('trans_id', 'off-'.time());
            $this->common->setTransCount();
            $result = TRUE;
        }

        if ($result) {
            // Record this transaction in the log
            $this->common->recordTrans();
            // Send some notification emails
            $this->common->sendEmails($this->common->email_template_subscription_success);
            // Update the user profile
            $this->common->updateUser($payment['item_id'], $user, $gateway);
        } else {
            // There was an error so flag that so we return to the registration page
            return ['errors' => ['payamic' => $result]];
        }
    }

    /**
     * The method for updating our payment details upon successful registration
     *
     * @var array
     */
    public function handleUserSuccess(User $user)
    {
        $user->set('start_date', date('Y-m-d'));
        // Search for the most recent registration payment with a matching email address
        $id = '';
        $transactions = array_reverse($this->common->getTransactions());
        foreach ($transactions as $trans) {
            if ($trans['desc'] == 'Membership Registration' && $trans['email'] == $user->email()) {
                // This is our transaction so update it with our new user ID
                $trans['user'] = $user->getAuthIdentifier();
                $this->common->updateTransaction($trans);
                // We're done so bail
                break;
            }
        }
        // Check if we have a user_num set (cos it doesn't seem to prefill the default?)
        if (!is_integer($user->get('user_num')) || $user->get('user_num') < 1000) {
            $user->set('user_num', $this->common->findNextUserNum());
            $user->save();
        }
    }

    public function handleFormPayment(Submission $submission) {
        $result = [];
        $amount = $submission->get('cf_payment_total');
        $subs = $submission->get('cf_subscription_id');
        // Check if we have paymnt details to process
        if ($subs != '' || (is_numeric($amount) && floatval($amount) > 0)) {
            // Load our vars
            if (!$this->common->loadVars()) {
                // There was an error loading our vars so crash
                $error = 'There was an error with the payment system configuration. We will attempt to fix this as soon as possible.';
                Log::error($error);
                return [ 'submission' => $submission, 'errors' => [ $error ] ];
            }

            // Check if we have a subscription or a regular payment
            if ($subs != '') {
                // Handle our subscription
                $result = $this->handleSubscription($submission);
            } elseif (is_numeric($amount) && floatval($amount) > 0) {
                $result = $this->handlePayment($submission);
            } else {
                // What on earth are we doing here?!?
                return ['submission' => $submission];
            }
            // Carry the extra fields in our submission forwards
            $submission = $result['submission'];
            return $result;
        } else {
            // Nothing to see here, move along
            return ['submission' => $submission];
        }
    }

    public function handlePayment($submission) {
        $data = $submission->data();
        $globals = GlobalSet::whereHandle('global')->data();
        // Check we have the necessary fields (gateway and payment details)
        $gateway = $data['cf_payment_gateway'] ?? '';
//        $quantity = (isset($data['cf_quantity']) ? $data['cf_quantity'] : 1);
//        $delivery = (isset($data['cf_shipping_option']) ? $data['cf_shipping_option'] : 'free');
        // Try to build the full name from various possible fields
        $name = $this->common->getName($data);
        $email = $data['email'] ?? '';

        if (!isset($data['cf_payment_amount']) || floatval($data['cf_payment_amount']) <= 0) {
            // We are missing data so error
            $error = 'Payment details missing: Invalid or missing payment amount';
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        } elseif (is_null($gateway) || $gateway == '') {
            // We are missing data so error
            $error = 'Payment details missing: Missing payment gateway';
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        } elseif (is_null($email) || $email == '') {
            // We are missing data so error
            $error = 'Payment details missing: Missing user email field';
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        }
        $payment = [
            'amount' => $data['cf_payment_amount'],
            'item_id' => $submission->formset()->name(),
            'desc' => $globals['gv_form_name'].' '.ucwords(str_replace(['-', '_'], ' ', $submission->formset()->name())),
            'currency' => strtolower($data['cf_payment_currency'] ?? 'aud')
        ];

        // Adjust our amount based on the quantity
        $price = $payment['amount'];
/*
        $payment['amount'] = $payment['amount'] * $quantity;
        // Add our delivery fee if necessary
        if ($delivery == 'free') {
            $delivery = 0;
        } elseif ($delivery == 'custom') {
            $delivery = floatval(Request::input('custom_delivery'));
        } else {
            $delivery = floatval($delivery);
        }
        $payment['amount'] += $delivery;
*/
        if ($gateway == 'stripe' && $payment['amount'] < $this->getConfig('stripe_minimum')) {
            // The amount of this payment is below our minimum
            $error = 'Payment error: Payment amount is below the minimum for this gateway';
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        }

        // And any gateway levy
        $do_levy = Request::input('cf_gateway_levy_'.$gateway, 0);
        $levy = 0;
        if ($do_levy != 0 && $this->getConfig($gateway.'_levy') != '' && floatval($this->getConfig($gateway.'_levy')) > 0) {
            $levy = $payment['amount'] * floatval($this->getConfig($gateway.'_levy')) / 100;
            $payment['amount'] = $payment['amount'] + $levy;
        }

        // Trim our payment to cents
        $payment['amount'] = round($payment['amount'], 2);

        // Tidy up our summary
        $summary = $this->parseSummary($data['cf_payment_summary'], FALSE, $payment['currency'], $payment['amount'], $gateway, $levy);

        // Record our fields so far
        $user = request()->user();
        $this->common->setPaymentDetail('user', ($user ? $user->getAuthIdentifier() : 'guest'));
        $this->common->setPaymentDetail('name', $name);
        $this->common->setPaymentDetail('email', $email);
        $this->common->setPaymentDetail('gateway', $gateway);
        $this->common->setPaymentDetail('amount', number_format($payment['amount'], 2));
        $this->common->setPaymentDetail('desc', $payment['desc']);
        $this->common->setPaymentDetail('summary', $summary);
        $this->common->setPaymentDetail('item_id', $payment['item_id']);
        $this->common->setPaymentDetail('quantity', 1);
        $this->common->setPaymentDetail('price', $price);
        $this->common->setPaymentDetail('delivery', 0);
        $this->common->setPaymentDetail('currency', $payment['currency']);
        $this->common->setPaymentDetail('gw_levy', ($do_levy != 0 ? $this->getConfig($gateway.'_levy', 0) : ''));
        $this->common->setCustomFields();

        // Process the payment
        $result = FALSE;
        if ($gateway == 'stripe') {
            // Check that we have our Stripe token
            $token = Request::input('stripe_token');
            if (is_null($token) || $token == '') {
                // We are missing data so error
                $error = 'Payment details missing: Missing Stripe token.';
                Log::error($error);
                return [ 'submission' => $submission, 'errors' => [ $error ] ];
            }
            // Process our payment
            $result = $this->common->processStripe($payment, $token);
        } else {
            // Just store the details
            $this->common->setPaymentDetail('timestamp', time());
            $this->common->setPaymentDetail('trans_id', 'off-'.time());
            $this->common->setTransCount();
            $result = TRUE;
        }

        if ($result) {
            // Record this transaction in the log
            $this->common->recordTrans();
            // Add our transaction details to the submission
            $data['cf_payment_desc'] = $this->common->payment_details['desc'];
            $data['cf_payment_item_id'] = $this->common->payment_details['item_id'];
//            $data['cf_payment_quantity'] = $this->common->payment_details['quantity'];
//            $data['cf_payment_delivery'] = $this->common->payment_details['delivery'];
            $data['cf_payment_summary'] = $this->common->payment_details['summary'];
            $data['cf_payment_gw_levy'] = $this->common->payment_details['gw_levy'];
            $data['cf_payment_timestamp'] = $this->common->payment_details['timestamp'];
            $data['cf_trans_id'] = $this->common->payment_details['trans_id'];
            $data['cf_trans_count'] = $this->common->payment_details['trans_count'];
            if ($gateway == 'stripe') {
                $data['cf_card_type'] = $this->common->payment_details['gateway_vars']['card']['type'];
                $data['cf_card_account'] = $this->common->payment_details['gateway_vars']['card']['account'];
                $data['cf_card_ending'] = $this->common->payment_details['gateway_vars']['card']['ending'];
            }
            $submission->data($data);
        } else {
            // Return our error
            $error = Session::get('payment_errors');
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        }
        return ['submission' => $submission];
    }

    function handleSubscription($submission) {
        $error = '';
        $result = FALSE;
        $data = $submission->data();
        Log::info($data);
        // Check we have the necessary details
        $gateway = (isset($data['cf_payment_gateway']) ? $data['cf_payment_gateway'] : '');
        $amount = floatval(Request::input('cf_subscription_price', 0));
        $quantity = (isset($data['cf_subscription_quantity']) ? floatval($data['cf_subscription_quantity']) : 1);
        if ($quantity == 'other') {
            $quantity = (Request::input('cf_subscription_quantity_custom', '') ? floatval(Request::input('cf_subscription_quantity_custom')) / $amount : 0);
        }
        $sub = $data['cf_subscription_id'] ?: '';
        $delivery = (isset($data['cf_shipping_option']) ? $data['cf_shipping_option'] : 'free');
        $name = $this->common->getName($data);
        $email = (isset($data['email']) ? $data['email'] : '');
        $stripe_source = Request::input('stripe_source');

        if ($amount <= 0 || $quantity <= 0) {
            $error = 'Payment details missing: Invalid or missing payment amount';
        } elseif ($gateway == 'stripe' && $amount * $quantity < $this->getConfig('stripe_minimum')) {
            $error = 'Payment error: Payment amount is below the minimum for this gateway';
        } elseif ($sub == '') {
            $error = 'Payment details missing: Invalid or missing subscription identifier';
        } elseif ($name == '') {
            $error = 'Payment details missing: Missing customer name';
        } elseif ($email == '') {
            $error = 'Payment details missing: Missing customer email';
        } elseif ($gateway == '') {
            $error = 'Payment details missing: Missing payment gateway';
        } elseif (is_null($stripe_source) || $stripe_source == '') {
            $error = 'Payment details missing: Missing credit / debit card token';
        }
        // Check for an init error
        if ($error != '') {
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        }

        // Tidy up our summary
        if (isset($data['cf_payment_summary'])) {
            $summary = $this->parseSummary($data['cf_payment_summary'], TRUE, 'aud', $amount * $quantity, $gateway, 0);
        } else {
            $summary = $quantity.' x Subscriptions to '.$sub.' for $'.number_format($amount * $quantity, 2);
        }

        // Record our fields so far
        $user = request()->user();
        $this->common->setPaymentDetail('user', ($user ? $user->getAuthIdentifier() : 'guest'));
        $this->common->setPaymentDetail('name', $name);
        $this->common->setPaymentDetail('email', $email);
        $this->common->setPaymentDetail('gateway', $gateway);
        $this->common->setPaymentDetail('item_id', $submission->formset()->name());
        $this->common->setPaymentDetail('desc', 'Subscription to ID: '.$sub);
        $this->common->setPaymentDetail('summary', $summary);
        $this->common->setPaymentDetail('amount', number_format($amount * $quantity, 2));

        if ($gateway == 'stripe') {
            // Create our customer
            $customer = $this->common->processStripeCustomer($name, $email, $stripe_source);
            if (is_string($customer)) {
                // There was an error so return it
                $error = $customer;
                Log::error($error);
                return [ 'submission' => $submission, 'errors' => [ $error ] ];
            }

            // Create our subscription
            $result = $this->common->processStripeSubscription($customer, $sub, $quantity);
        } else {
            // Ummmm, just save the deets like a normal payment?
            $this->common->setPaymentDetail('timestamp', time());
            $this->common->setPaymentDetail('trans_id', 'off-subs_'.time());
            $this->common->setTransCount();
            $result = TRUE;
        }

        if ($result) {
            // Record this transaction in the log
            $this->common->recordTrans();
            // Add the customer and subscription details to our submission details
            $data['cf_subscription_timestamp'] = $this->common->payment_details['timestamp'];
            $data['cf_trans_id'] = $this->common->payment_details['trans_id'];
            $data['cf_payment_total'] = $this->common->payment_details['amount'];
            $data['cf_payment_summary'] = $this->common->payment_details['summary'];
            if ($gateway == 'stripe') {
                $data['cf_subscription_id'] = $this->common->payment_details['gateway_vars']['plan']['id'];
                $data['cf_subscription_stripe_cust_id'] = $this->common->payment_details['gateway_vars']['customer'];
                $data['cf_subscription_result'] = $this->common->payment_details['gateway_vars']['subscription'];
                $data['cf_subscription_amount'] = $this->common->payment_details['gateway_vars']['plan']['amount'];
                $data['cf_subscription_quantity'] = $this->common->payment_details['gateway_vars']['quantity'];
                $data['cf_subscription_product_id'] = $this->common->payment_details['gateway_vars']['plan']['product'];
                $data['cf_subscription_interval'] = $this->common->payment_details['gateway_vars']['plan']['interval'];
                $data['cf_subscription_interval_count'] = $this->common->payment_details['gateway_vars']['plan']['interval_count'];
            }
            $submission->data($data);
        } else {
            // Return our error
            $error = Session::get('payment_errors');
            Log::error($error);
            return [ 'submission' => $submission, 'errors' => [ $error ] ];
        }
        return ['submission' => $submission];
    }

    private function parseSummary($str, $sub, $cur, $amount, $gateway, $levy) {
        $summary = htmlspecialchars_decode($str);
        $summary = str_replace('<span', '- <span', $summary);
        $summary = str_replace('">', '">'.$this->getCurSymbol($cur), $summary);
        $summary = str_replace('<br>', "\n", $summary);
        $summary = strip_tags($summary);
        $summary .= "\nTOTAL: ".$this->getCurSymbol($cur).number_format($amount, 2);
        if (!$sub && $gateway == 'stripe' && $levy > 0) {
            $summary .= ' (includes Credit / Debit Card fee of '.$this->getConfig($gateway.'_levy').'%)';
        }
        return $summary;
    }

    static public function getCurSymbol($cur) {
        switch (strtolower($cur)) {
            case 'gbp':
                return '£';
            case 'eur':
                return '€';
            default:
                return '$';
        }
    }
}
