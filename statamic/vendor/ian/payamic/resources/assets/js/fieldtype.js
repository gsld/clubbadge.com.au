Vue.component('payamic-fieldtype', {

    mixins: [Fieldtype],

    template: '<div><span>{{ data }}</span></div>',

    data: function() {
        return {
            //
        };
    },

    computed: {
        //
    },

    methods: {
        //
    },

    ready: function() {
        //
    }

});
