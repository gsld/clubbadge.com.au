@extends('layout')

@section('content')
<div class="flexy mb-3">
        <h1 class="fill"><a href="{{ cp_route('payamic.index') }}">Recent Transactions</a></h1>
@if (!empty($transactions))
        <div class="btn-group">
            <a href="{{ route('payamic.export', ['type' => 'csv', 'download' => 'true']) }}"
               type="button" class="btn btn-default">{{ t('export') }}</a>
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">{{ translate('cp.toggle_dropdown') }}</span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="{{ route('payamic.export', ['type' => 'csv', 'download' => 'true']) }}">{{ t('export_csv') }}</a></li>
                <li><a href="{{ route('payamic.export', ['type' => 'json', 'download' => 'true']) }}">{{ t('export_json') }}</a></li>
            </ul>
        </div>
@endif
</div>
<div class="card flush">
    <div class="dossier-table-wrapper">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Item ID</th>
                    <th>Full Name</th>
                    <th>Amount</th>
                    <th>Email</th>
                    <th>Gateway</th>
                </tr>
            </thead>
            <tbody>
        @if (empty($transactions))
                <tr>
                    <td colspan="7">There are no transactions to display</td>
                </tr>
        @else
            @foreach ($transactions as $id => $transaction)
                <tr>
                    <td><a href="{{ route('payamic.detail', ['id' => $id]) }}">{{ date("Y/m/d H:i", $transaction['timestamp']) }}</a></td>
                    <td>{{ $transaction['item_id'] }}</td>
                    <td>{{ $transaction['name'] }}</td>
                    <td>
                        @if (isset($transaction['currency']))
                            {{ Statamic\Addons\Payamic\PayamicListener::getCurSymbol($transaction['currency']) }}
                        @else
                            $
                        @endif
                        {{ $transaction['amount'] }}
                    </td>
                    <td>{{ $transaction['email'] }}</td>
                    <td>{{ ucfirst($transaction['gateway']) }}</td>
                </tr>
            @endforeach
        @endif
            </tbody>
        </table>
    </div>
</div>
@endsection
