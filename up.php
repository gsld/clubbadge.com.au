<!doctype html>
<html>
<head>
  <style>
    body { background-color: #222; padding: 12%; }
    h1 { color: #fff; }
    p { color: #fff; margin: 20px 0;}
    a { color: #fff; }
  </style>
</head>
<body>
  <h1>Hello traveller.</h1>
	<p>This is a test to make sure this website is up and running.</p>
	<a href="https://www.gsld.com.au">GSLD</a>
</body>
</html>
