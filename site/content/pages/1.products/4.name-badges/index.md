title: Name Badges
show_page_auto_content: false
content_blocks:
  - 
    type: content_block_set
    block_set_full_width: true
    block_set_hide: false
    block_sets:
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
      - 
        type: col_set
        col_equal_height: true
        col_vertical_align: false
        col_full_width: false
        col_widths: threeseven
        add_col_margin: false
        add_col_margin_bottom: false
        col_sets:
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_image_set
                align: left
                img_size: md
                orient: ls
                action: max
                toggle_lightbox: false
                toggle_stacked: true
                show_captions: false
                show_border: false
                run_lazyload: true
                show_full_width: false
                image_mask: none
                file:
                  - /assets/2022/images/site-logo.png
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_text_set
                align: left
                kind: normal
                text_default_margin: false
                text_default_margin_bottom: true
                text: |
                  <h1>CUSTOM NAME BADGES <br>
                  <small>design & manufacture</small></h1>
                  
                  Club Badge Australia specialise in the design and supply of custom name badges for all organizations.
                  
                  We can incorporate your logo into the name badge design and other options include epoxy resin coating, a choice of coloured surrounds and pin or magnet attachment.
                  
                  Please do not hesitate to [contact us](/contact) for all your name badge requirements.
              - 
                type: col_image_set
                align: left
                img_size: sm
                orient: ls
                action: max
                toggle_lightbox: false
                toggle_stacked: true
                show_captions: false
                show_border: false
                run_lazyload: true
                show_full_width: false
                image_mask: none
                file:
                  - /assets/2022/images/name-badges/name-badge-back1.png
                  - /assets/2022/images/name-badges/name-badge-back2.png
      - 
        type: break_set
        break_above: 10
        break_bg_color: none
        break_full_width: false
item_heading: Name Badges
no_gv_bg: false
header_sets:
  - 
    type: slide_set
    slide_bg_color: black
    slide_vertical_align: false
    slide_content_full_width: false
    slide_content_sets:
      - 
        type: slider_break_set
        break_above: 8sw
        break_bg_color: none
        break_full_width: false
    file: /assets/2022/images/headers/name-badge1-2.jpg
    image_height: "300"
  - 
    type: slide_set
    slide_bg_color: black
    slide_vertical_align: false
    slide_content_full_width: false
    slide_content_sets:
      - 
        type: slider_break_set
        break_above: 8sw
        break_bg_color: none
        break_full_width: false
    file: /assets/2022/images/headers/name-badge2-2.jpg
    image_height: "300"
header_margin: xs
header_slider_speed: 5000
header_slider_show: 1
template: default
fieldset: default
page_menu_kind: rabbit-holes
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: false
page_noindex: false
toggle_page_menu: false
toggle_hidden: false
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
id: 8abde27b-28fd-40cd-9d3f-e1276c270094
