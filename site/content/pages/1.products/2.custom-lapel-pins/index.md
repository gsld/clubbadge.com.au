title: Custom Lapel Pins
show_page_auto_content: false
content_blocks:
  - 
    type: content_block_set
    block_set_full_width: true
    block_set_hide: false
    block_sets:
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
      - 
        type: col_set
        col_equal_height: true
        col_vertical_align: false
        col_full_width: false
        col_widths: threeseven
        add_col_margin: false
        add_col_margin_bottom: false
        col_sets:
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_image_set
                align: left
                img_size: md
                orient: ls
                action: max
                toggle_lightbox: false
                toggle_stacked: true
                show_captions: false
                show_border: false
                run_lazyload: true
                show_full_width: false
                image_mask: none
                file:
                  - /assets/2022/images/site-logo.png
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_text_set
                align: left
                kind: normal
                text_default_margin: false
                text_default_margin_bottom: true
                text: |
                  <h1>LAPEL PINS <br>
                  <small>from Australia's #1 badge maker</small></h1>
          
                  Club Badge Australia specialise in the design and supply of the best range of custom lapel pins in Australia. With a wide range of options available including custom shapes and sizes, soft enamel pins, epoxy dome finish, we are also able to incorporate your custom logo into any design. We  also manufacture low minimum order quantities to suit your requirements.  Club Badge Australia offers a free pin design service, allowing you to preview and approve your design prior to manufacture. **[Contact us](/contact) for more information or to order.**
      - 
        type: break_set
        break_above: 10
        break_bg_color: none
        break_full_width: false
item_heading: Custom Lapel Pins
no_gv_bg: false
header_sets:
  - 
    type: slide_set
    slide_bg_color: black
    slide_vertical_align: false
    slide_content_full_width: false
    slide_content_sets:
      - 
        type: slider_break_set
        break_above: 12sw
        break_bg_color: none
        break_full_width: false
    file: /assets/2022/images/headers/lapel1.jpg
    image_height: "400"
  - 
    type: slide_set
    file: /assets/2022/images/headers/lapel2.jpg
    slide_bg_color: black
    image_height: "400"
    slide_vertical_align: false
    slide_content_full_width: false
header_margin: xs
header_slider_speed: 5000
header_slider_show: 1
template: default
fieldset: default
page_menu_kind: rabbit-holes
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: false
page_noindex: false
toggle_page_menu: false
toggle_hidden: false
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
id: 14e3ba1f-b151-4e22-9342-a5ea69fa7c8f
