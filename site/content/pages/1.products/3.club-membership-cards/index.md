title: Club Membership Cards
show_page_auto_content: false
content_blocks:
  - 
    type: content_block_set
    block_set_full_width: true
    block_set_hide: false
    block_sets:
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
      - 
        type: col_set
        col_equal_height: true
        col_vertical_align: false
        col_full_width: false
        col_widths: threeseven
        add_col_margin: false
        add_col_margin_bottom: false
        col_sets:
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_image_set
                align: left
                img_size: md
                orient: ls
                action: max
                toggle_lightbox: false
                toggle_stacked: true
                show_captions: false
                show_border: false
                run_lazyload: true
                show_full_width: false
                image_mask: none
                file:
                  - /assets/2022/images/site-logo.png
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_text_set
                align: left
                kind: normal
                text_default_margin: false
                text_default_margin_bottom: true
                text: |
                  <h1>CLUB MEMBERSHIP CARDS <br>
                  <small>design, print & supply</small></h1>
                    
                  Club Badge is a leading supplier of membership cards for associations, sporting clubs and not-for-profit organisations across Australia.

                  Membership cards can be designed to include the following:

                  - Photo ID
                  - Membership number
                  - QR code
                  - Barcode
                  - Magnetic Stripe

                  Club Badge Australia can assist with design and production of all your membership card requirements.
      - 
        type: break_set
        break_above: 10
        break_bg_color: none
        break_full_width: false
item_heading: Club Membership Cards
no_gv_bg: false
header_sets:
  - 
    type: slide_set
    slide_bg_color: black
    slide_vertical_align: false
    slide_content_full_width: false
    slide_content_sets:
      - 
        type: slider_break_set
        break_above: 14sw
        break_bg_color: none
        break_full_width: false
    file: /assets/2022/images/headers/membership1.jpg
    image_height: "400"
  - 
    type: slide_set
    file: /assets/2022/images/headers/membership2.jpg
    slide_bg_color: black
    image_height: "400"
    slide_vertical_align: false
    slide_content_full_width: false
header_margin: xs
header_slider_speed: 5000
header_slider_show: 1
template: default
fieldset: default
page_menu_kind: rabbit-holes
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: false
toggle_page_menu: false
toggle_hidden: false
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
id: 76dc5afc-9468-49e4-8419-fced53acbff1
