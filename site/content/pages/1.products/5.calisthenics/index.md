title: Calisthenics
show_page_auto_content: false
content_blocks:
  - 
    type: content_block_set
    block_set_full_width: true
    block_set_hide: false
    block_sets:
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
      - 
        type: col_set
        col_equal_height: true
        col_vertical_align: false
        col_full_width: false
        col_widths: threeseven
        add_col_margin: false
        add_col_margin_bottom: false
        col_sets:
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_image_set
                align: left
                img_size: md
                orient: ls
                action: max
                toggle_lightbox: false
                toggle_stacked: true
                show_captions: false
                show_border: false
                run_lazyload: true
                show_full_width: false
                image_mask: none
                file:
                  - /assets/2022/images/site-logo.png
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_text_set
                align: left
                kind: normal
                text_default_margin: false
                text_default_margin_bottom: true
                text: |
                  <h1>CALISTHENICS MEDALS <br>
                  <small>design & manufacture</small></h1>
                  
                  Club Badge Australia is a leading manufacturer and supplier of quality Calisthenics Medallions. We offer a wide range of generic medals and custom design medals. We will work with you to design and manufacture to your club's specifications.
                  
                  Please do not hesitate to contact us for all your Calisthenic medallion requirements.
                  
                  **[Click here to download the medal order form](/assets/2021/medal-sheet-order-form.jpg)**
                  
                  The medals below are available in gold, silver, and bronze finish.
      - 
        type: break_set
        break_above: 10
        break_bg_color: none
        break_full_width: false
      - 
        type: image_set
        align: center
        img_size: lg
        orient: ls
        action: max
        toggle_lightbox: false
        toggle_stacked: false
        show_captions: false
        show_border: false
        run_lazyload: true
        show_full_width: false
        image_mask: none
        file:
          - /assets/2021/products/calisthenics/new-medal-sheet-page-2.png
        exact_size: "800"
      - 
        type: image_set
        align: center
        img_size: lg
        orient: ls
        action: max
        toggle_lightbox: false
        toggle_stacked: false
        show_captions: false
        show_border: false
        run_lazyload: true
        show_full_width: false
        image_mask: none
        file:
          - /assets/2021/products/calisthenics/new-medallion-sheet-page-1.png
        exact_size: "800"
      - 
        type: gallery_set
        gallery_full_width: false
        show_captions: false
        caption_filename: false
        run_lazyload: true
        kind: masonry
        size: sm
        image_mask: none
        gallery_number: 1
        slider_show: 1
        slider_thumbnails: none
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
      - 
        type: content_set
        align: center
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: '### [Contact us](/contact) to order or find out more'
      - 
        type: button_set
        kind: one
        style: solid
        align: center
        size: md
        is_block: false
        mobile_align_override: none
        cta:
          - 
            text: Contact
            link: /contact
            new_window: false
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
item_heading: Calisthenics
no_gv_bg: false
header_sets:
  - 
    type: slide_set
    slide_bg_color: black
    slide_vertical_align: false
    slide_content_full_width: false
    slide_content_sets:
      - 
        type: slider_break_set
        break_above: 11sw
        break_bg_color: none
        break_full_width: false
    file: /assets/2022/images/headers/calisthenics1.jpg
    image_height: "350"
  - 
    type: slide_set
    file: /assets/2022/images/headers/calisthenics2.jpg
    slide_bg_color: black
    image_height: "350"
    slide_vertical_align: false
    slide_content_full_width: false
header_margin: xs
header_slider_speed: 5000
header_slider_show: 1
template: default
fieldset: default
page_menu_kind: rabbit-holes
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: false
toggle_page_menu: false
toggle_hidden: false
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
id: bcc714eb-af55-4fd7-a492-d10e5f7cb80e
