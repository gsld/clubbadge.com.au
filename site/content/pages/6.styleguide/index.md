title: Styleguide
content_blocks:
  - 
    type: content_block_set
    block_set_full_width: true
    block_sets:
      - 
        type: break_set
        break_above: 30
        break_bg_color: none
        break_full_width: false
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          (regular text)
          
          # Heading One
          
          ## Heading Two
          
          ### Heading Three
          
          #### Heading Four
          
          ##### Heading Five
          
          ###### Heading Six
          
          **Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. _Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum._
      - 
        type: content_set
        align: left
        kind: lead
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          **This is a paragraph set to leading text.**
          
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          (text size small)
          
          # Heading One
          
          ## Heading Two
          
          ### Heading Three
          
          #### Heading Four
          
          ##### Heading Five
          
          ###### Heading Six
          
          **Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. _Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum._
        text_block_settings:
          - 
            text_color: none
            text_bg_color: none
            text_size: sm
            text_weight: normal
            text_transform: none
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          (text size large)
          
          # Heading One
          
          ## Heading Two
          
          ### Heading Three
          
          #### Heading Four
          
          ##### Heading Five
          
          ###### Heading Six
          
          **Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. _Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum._
        text_block_settings:
          - 
            text_color: none
            text_bg_color: none
            text_size: lg
            text_weight: normal
            text_transform: none
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          (text size extra large)
          
          # Heading One
          
          ## Heading Two
          
          ### Heading Three
          
          #### Heading Four
          
          ##### Heading Five
          
          ###### Heading Six
          
          **Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. _Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum._
        text_block_settings:
          - 
            text_color: none
            text_bg_color: none
            text_size: xl
            text_weight: normal
            text_transform: none
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          # Long headings
          
          ## This is an h2 in long text lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non lectus id eros ornare mollis. Integer sagittis dolor ut nulla sodales, vitae blandit tellus tincidunt.

          With some text lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non lectus id eros ornare mollis. Integer sagittis dolor ut nulla sodales, vitae blandit tellus tincidunt. 
          
          ### H3 looks like quisque nulla nisi, rhoncus sed varius vitae, ultricies quis purus. Mauris sed metus sit amet lacus ultricies accumsan auctor at enim. Morbi venenatis pretium cursus. Mauris ut varius turpis. Maecenas hendrerit justo a tortor rutrum consectetur.

          With some morbi venenatis pretium cursus. Mauris ut varius turpis. Maecenas hendrerit justo a tortor rutrum consectetur. Quisque ac accumsan velit. Sed id purus auctor leo mattis semper. Curabitur a commodo mi. Suspendisse semper ac sem a iaculis.

          #### The H4 is used a fair bit for quisque ac accumsan velit. Sed id purus auctor leo mattis semper. Curabitur a commodo mi. Suspendisse semper ac sem a iaculis.

          Curabitur a commodo mi. Suspendisse semper ac sem a iaculis. Aenean malesuada fermentum libero non feugiat. Morbi porta nibh velit, in aliquet turpis lacinia semper.
          
          ##### Does anyone actually ever use H5s? I do not aenean malesuada fermentum libero non feugiat. Morbi porta nibh velit, in aliquet turpis lacinia semper.

          Quisque ac accumsan velit. Sed id purus auctor leo mattis semper. Curabitur a commodo mi. Suspendisse semper ac sem a iaculis. Aenean malesuada fermentum libero non feugiat. Morbi porta nibh velit, in aliquet turpis lacinia semper.
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
    block_set_hide: false
  - 
    type: content_block_set
    block_set_full_width: true
    block_sets:
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          # This is a block colour test
          
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          
          ## Heading 2
          
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          
          ### Heading 3
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        text_block_settings:
          - 
            text_color: none
            text_bg_color: none
            text_size: md
            text_weight: normal
            text_transform: none
      - 
        type: read_more_set
        heading: Read More Button Heading
        btn_align: left
        btn_width: btn
        btn_style: outline
        btn_color: two
        read_more_sets:
          - 
            type: read_more_text_set
            align: left
            kind: normal
            text_default_margin: false
            text_default_margin_bottom: true
            text: |
              # This is a test
              
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              
              ## Heading 2
              
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
              
              ### Heading 3
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      - 
        type: info_set
        col_count: three
        headers: false
        highlight: false
        info:
          - 
            type: row_set
            col1: |
              # This is a test
              
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            col2: |
              ## Heading 2
              
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            col3: |
              ### Heading 3
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      - 
        type: accordion_set
        accordion:
          - 
            type: accordion_item
            heading: Lorem ipsum dolor sit amet
            text: >
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
              est laborum.
          - 
            type: accordion_item
            heading: Excepteur sint occaecat cupidatat non proident
            text: >
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
              et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
              aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum.
        btn_color: two
        btn_style: outline
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
    block_bg_settings:
      - 
        bg_margin: xs
        bg_color: none
        bg_gradient: false
    block_set_hide: false
  - 
    type: content_block_set
    block_set_full_width: true
    block_sets:
      - 
        type: content_set
        align: left
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: |
          # This is a block colour test
          
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          
          ## Heading 2
          
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          
          ### Heading 3
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        text_block_settings:
          - 
            text_color: none
            text_bg_color: none
            text_size: md
            text_weight: normal
            text_transform: none
      - 
        type: read_more_set
        heading: Read More Button Heading
        btn_align: left
        btn_width: btn
        btn_style: outline
        read_more_sets:
          - 
            type: read_more_text_set
            align: left
            kind: normal
            text_default_margin: false
            text_default_margin_bottom: true
            text: |
              # This is a test
              
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              
              ## Heading 2
              
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
              
              ### Heading 3
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est loutlinem.
        btn_color: white
      - 
        type: info_set
        col_count: three
        headers: false
        highlight: false
        info:
          - 
            type: row_set
            col1: |
              # This is a test
              
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            col2: |
              ## Heading 2
              
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            col3: |
              ### Heading 3
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      - 
        type: accordion_set
        accordion:
          - 
            type: accordion_item
            heading: Lorem ipsum dolor sit amet
            text: >
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
              est laborum.
          - 
            type: accordion_item
            heading: Excepteur sint occaecat cupidatat non proident
            text: >
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
              et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
              aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum.
        btn_color: white
        btn_style: solid
      - 
        type: break_set
        break_above: "50"
        break_below: "50"
        break_bg_color: none
        break_full_width: false
        border_settings:
          - 
            break_border: true
            border_style: dashed
            border_thickness: "2"
            border_color: dddddd
            border_width: 100
            border_align: center
    block_bg_settings:
      - 
        bg_margin: xs
        bg_color: dark
        bg_gradient: false
    block_set_hide: false
no_gv_bg: false
item_heading: Styleguide
header_margin: none
header_slider_speed: 5000
header_slider_show: 1
template: page/colophon
fieldset: pages/specific
page_menu_kind: default
cache_exclude: false
toggle_page_menu: false
toggle_hidden: true
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
header_size: md
slider_speed: 8000
id: eb691dd8-860e-4023-b8f3-c7f6056caa01
