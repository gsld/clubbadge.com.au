title: 'Terms & Conditions'
content_blocks:
  - 
    type: content_block_set
    block_sets:
      - 
        type: break_set
        break_border: false
        break_above: 60
        break_full_width: false
      - 
        type: content_set
        text: |
          Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern {{ site_name }} relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.
        text_default_margin: false
        text_default_margin_bottom: true
      - 
        type: content_set
        text: |
          ## Ecommerce, Shop and Online Payments Terms and Conditions
          
          **1) Scope**
          
          All goods ("Products") supplied by {{ site_name }} also known as {{ site_nice_url }} are supplied on these terms and conditions.
          
          **2) Order acceptance**
          
          No order shall be binding on {{ site_nice_url }} until accepted by {{ site_name }} . An individual contract for the supply of Products, on these terms and conditions, is formed on acceptance by {{ site_name }} of an order from the Customer. {{ site_name }} reserves the right to accept any order in whole or in part. Where {{ site_name }} makes a part delivery of any order, such delivery shall constitute a separate contract. No order may be cancelled or varied after acceptance by {{ site_name }}.
          
          **2.1) Sale Items**
          
          {{ site_name }} will always do their best to ensure all sale items are in stock in all of our outlets, however, in circumstances there may be limited stock only available in one, some or none of our outlets. 'End of line' and other sale items that are out of stock, may not be re-ordered. There may be instances where out of stock items remain on the website for a period after going out of stock. {{ site_name }} will always remove these products in the most timely manner possible.
          
          **3) Payment terms**
          
          Payment must be made in full and on checkout in the online shopping cart without set off or deduction unless we have the option for offline payment such as bank transfer and if the customer chooses this option. {{ site_name }} will investigate any disputed amounts, and resolve them by issuing a credit to the Customer if and when required.
          
          **4) Title to products**
          
          Ownership of the product does not pass to the Customer until the relevant order has been fulfilled and dispatched from the {{ site_name }} location. {{ site_name }} reserves the right to void any order if deemed necessary prior to dispatch.
          
          **5) Delivery**
          
          - {{ site_name }} reserves the right to charge for delivery of the Products at any time, notwithstanding that it may not have previously done so. Administrative fees may also be imposed for orders under certain dollar values. Where prices are stated as inclusive of delivery, delivery is to the delivery point specifically accepted by {{ site_name }}.
          - A handling fee may be incurred on all orders
          - Items over 1m in length or 3kg in weight may travel by road freight courier. Road courier deliveries require a signature on delivery and cannot be sent either to a PO box or C/O a post office.
          - Bulky items, over 3kg in weight, may incur a freight surcharge. Any such surcharge will be notified to the customer prior to any such charges for additional freight being added to the amount charged by credit card, unless these are already indicated on and included in, the price from the website.
          
          **5.1) Delivery Insurance**
          
          If a customer purchases optional insurance on their Products during checkout, the insurance is limited to protecting the items whilst in transit. Once the Products have been confirmed by the freight company to have been delivered, the order is thereafter not insured by {{ site_name }}.
          
          {{ site_name }} is not responsible for the loss or damage to goods if chosen to leave at address upon completion of delivery.
          
          **6) Returns - {{ site_name }} has a hassle free returns policy**
          
          Please refer to our [Return Policy Page]({{ gv_to_shop_policy_returns }}). Any Products which are damaged or defective, or which are not otherwise in accordance with the Customer's order, or which the law provides may otherwise be returned to {{ site_name }} within 7 days after the Customer has received the Products, at no cost to the Customer.
          
          - Products must be in their original packaging, unopened, of a current make and model, and otherwise as new and in a saleable condition.
          - Return product at the Customer's own expense.
          - 15% restocking or return fee may be charged.
          
          {{ site_name }} will not accept a returned Product where the Customer has caused the Product to become un-merchandisable or failed to take steps to prevent the Product from becoming un-merchandisable or the Product has become damaged by abnormal use whilst in the possession of the Customer.
          
          **7) Customer Specific Stock**
          
          Where {{ site_name }} has agreed to procure and/or warehouse and/or distribute Products specifically for the Customer, the Customer must, within 30 days of request, purchase all stock then warehoused and held at the then prevailing supply price. Where the Customer does not do so, {{ site_name }} may dispose of the affected Products without liability for any loss or damage suffered by the Customer as a result. The Customer indemnifies {{ site_name }} against all claims, demands, loss, costs and expenses incurred by or made against {{ site_name }}, arising out of any actual or alleged infringement of patents, copyright, trademarks, design rights or other intellectual property rights, by any logo, design, copyright or other material that {{ site_name }} may use, print or reproduce at the Customer's request.
          
          **8) Liability**
          
          8.1) Except for those required or implied by legislation, {{ site_name }} gives no express warranty in relation to products and services supplied to the Customer, and the Customer acknowledges that it has not relied on any representation or warranty made by or on behalf of {{ site_name }}. Certain legislation may imply conditions and warranties into these terms and conditions. To the extent that such conditions and warranties may lawfully be excluded, all such conditions and warranties are expressly excluded. The liability of {{ site_name }} under or arising out of the supply of goods and services for breach of any term, condition or warranty implied in or imposed upon the supply of goods and/or services by legislation, shall be limited, at the option or {{ site_name }} to:
          
          (a) the replacement of the goods or the supply of equivalent goods;
          (b) the repair of the goods;
          (c) the payment of the cost of replacing the goods or of acquiring equivalent goods; or
          (d) the payment of the cost of having the goods repaired;
          
          8.2) Except to the extent the law provides that liability is not able to be excluded, {{ site_name }} shall not be under any liability to the Customer in respect of any loss or damage (including consequential or indirect loss or damage or loss of profits, loss of use or loss of data) however caused, which may be suffered or incurred or which may arise directly or indirectly in respect of the Products, any services supplied by {{ site_name }} or the failure of {{ site_name }} to comply with these terms and conditions.
          
          **9) Conflicts**
          
          These terms and conditions will apply to the exclusion of all other terms and conditions contained in the Customer's order. In the event of any inconsistency, {{ site_name }} will be deemed, by delivering the Products to the Customer or supplying services to the Customer, to have made an offer to the Customer to sell the Products or supply the services pursuant to these terms and conditions, which offer will be deemed to have been accepted if the Customer retains the Products or accepts the services. {{ site_name }} reserves the right to change these Terms and Conditions at any time.
          
          **10) GST**
          
          All prices listed on the {{ site_name }} website are inclusive of GST if indicated on the product and in the checkout. Some products listed on our website may be GST free. Any consideration and all other amounts to be paid from a purchase through the {{ site_name }} online shopping store will be liable for GST. Upon checkout {{ site_name }} will issue a tax invoice showing the amount of GST applicable for the particular purchase.
          
          **11) Jurisdiction**
          
          These terms and conditions are governed by and will be construed in accordance with the laws of Victoria and the parties agree to submit to the jurisdiction of the courts of that state.
          
          **12) No Waiver**
          
          The failure by {{ site_name }} to exercise, or any delay in exercising, any right, power or privilege available to it under these terms and conditions will not operate as a waiver thereof or preclude any other or further exercise thereof or the exercise of any other right or power.
          
          **13) Export Agreements**
          
          Not all products listed on this website are available for export outside the Australian market due to distribution agreements in certain territories.
          
          **14) Unauthorized Resale of Products**
          
          {{ site_name }} reserves the right to deny service and/or take legal action against any individual or individuals who procure products from {{ site_name }} with the intent to resale those products on external websites.
          
          **15) In Store Pickup**
          
          In Store Pick up orders will be available for collection in store for a period of 1 month; if the order isn't collected by this point the order will be refunded back to the original account unless a prior arrangement is made with {{ site_name }}.
          
          **16) Backorder Items**
          
          If the customer is unreachable for a period of 14 days or more {{ site_name }} will ship the remaining items on the order while refunding the item in question to the customer to avoid any additional delays. A notification email of this will be sent to the customer. If they wish to look at an alternative product after this has occurred they can contact the customer service team prior to selecting an alternative for a reduced shipping fee on their next order.
          
          **17) Uncontactable Customer, Refunding item.**
          
          If the customer is unreachable for a period of 14 days or more {{ site_name }} will refund the item returned back to the account from which the item/s were originally purchased. A confirmation email will then be sent through to the customer. If the customer wishes to purchase an alternative to the missing item after this has occurred they can contact our customer service team for a reduced shipping charge on the remaining item.
          
          **18) In-store pickup policy**
          
          When placing an in-store pickup the goods can only be collected by the person who placed the order. {{ site_name }} may request proof of identity upon collection.
          
          **19) Hostile and Abusive Customer Behavior**
          
          Abusive customer behavior will not be tolerated. {{ site_name }} staff reserves the right to deny service to any customer or business who exhibits hostile or abusive behavior and are subject to the conditions below.
          
          Examples of this behaviour include, but are not limited to:
          
          - Threats of physical violence;  swearing;  inappropriate cultural, racial or religious references; and rudeness, including derogatory remarks.
          - Demanding responses within an unreasonable requests.
          - Repeatedly contacting or insisting on speaking to a particular member of staff who is not directly dealing with the matter.
          - Excessive telephone calls, emails or LiveChats (this also includes unsolicited marketing calls, letters and emails that are considered to be 'spam' or 'phishing').
          - Sending duplicate correspondence requiring a response to more than one member of staff;  persistent refusal to accept Ban or Block.
          
          Available restrictions:
          
          - Customer can be Blocked or Banned from purchasing products or contacting our staff via email, social media, phone and livechat channels.
          - Take legal action, such as applying for an injunction or court order to prohibit contact/poor behaviour.
          
          **20) Price Match Policy.**
          
          To price match an item with a competitor, the price match must be made on the day of purchase and is valid for that day only.
          
          - Price match must be agreed to prior to the purchase with an {{ site_name }} team member either via telephone, email or in store.
          - Price match must be done prior to purchase and can only be done via the telephone or in person.
          - Price matching cannot be activated retrospectively.
          - The price agreed upon is valid for the day of purchase only and is not valid in conjunction with any other offer.
          - The price match policy is only offered if the same size and model of the product is available and in stock with the competitor.
          - The competitor that we price match with must be a physical store located in Australia.
          
          **21) Software and Technology Issues.**
          
          In the event of communications or system errors occurring in connection with the settlement of accounts or other features or components of the software, neither {{ site_name }} nor the Software Provider will have any liability to you or to any third party in respect of such errors. {{ site_name }} reserves the right in the event of such errors to remove all relevant products from the Website and take any other action to correct such errors.
          
          **22) Promotion Codes and Free Gifts**
          
          Promotion codes, also referred to as coupon codes, discount codes or vouchers, and free gift offers:
          
          - are only valid online
          - cannot be used in conjunction with any other offer or price matching
          - cannot be used/claimed when purchasing {{ site_name }} Gift Vouchers
          cannot be enacted retrospectively
          - free shipping codes only apply to deliveries within Australia
          - any {{ site_name }} promotion codes used online will apply the discount value evenly across all products in your purchase. In the event of a discounted item being refunded, only the amount paid for the item can be refunded.
          
          **23) Taxes and Duties (International Orders)**
          
          Within Australia, prices are inclusive of Australian GST,  however if you live outside of Australia, there may be additional taxes or import duties imposed at the receivers end. {{ site_name }} is not liable for these charges and it is up to the customer to liaise with Customs regarding their delivery.
          
          In Australia there will be no additional costs.
          
          **24) Pricing**
          
          {{ site_name }} endeavours to ensure all prices are correct at time of publication; however if a pricing error does occur, {{ site_name }} reserves the right to alter prices for any reason. If this should happen after you have ordered a product, we will contact you prior to processing your order.
          
          We reserve the right to withdraw any product from sale without notice, for any reason. However for various reasons, that product might still appear on our website for a period of time after that withdrawal. We make no undertaking that a product shown on this website will be available for purchase online at all stores or at a specific store.
          
          **25) Gift Vouchers**
          
          Gift vouchers are not redeemable for cash
          
          For vouchers purchased after November 1st 2019, all gift vouchers are valid for 12 months from date of purchase
          
          The 1 year expiry period for gift vouchers/promo codes does not apply to:
          
          - promotional gift vouchers (donated vouchers or promotional one off vouchers)
          - vouchers for specific events
          - customer loyalty programs
          - part of a temporary marketing promotion
        text_default_margin: false
        text_default_margin_bottom: true
      - 
        type: content_set
        text: |
          ## General Website Terms and Conditions
          
          The term '{{ site_name }}' or 'us' or 'we' refers to the owner of the website. The term 'you' refers to the user or viewer of our website.
          
          **The use of this website is subject to the following terms of use:**
          
          The content of the pages of this website is for your general information and use only. It is subject to change without notice.
          
          This website uses cookies to monitor browsing preferences.
          
          Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.
          
          Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.
          
          This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited without prior permission.
          
          All trademarks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.
          
          Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offense.
          
          From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).
        text_default_margin: false
        text_default_margin_bottom: true
      - 
        type: break_set
        break_border: false
        break_above: 80
        break_full_width: false
    block_set_full_width: true
    block_set_hide: false
no_gv_bg: false
item_heading: 'Terms & Conditions'
header_margin: none
header_slider_speed: 5000
header_slider_show: 1
template: default
fieldset: default
page_menu_kind: rabbit-holes
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: false
toggle_page_menu: false
toggle_hidden: true
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
id: bd400b64-1cbf-4319-b961-8311fb5a3be5
