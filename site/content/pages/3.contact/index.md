form_pages:
  - 
    type: form_page_set
    form_page_fieldsets:
      - 
        type: fieldset_set
        form_blocks:
          - 
            type: text_set
            field_label: Name
            field_name: name
            is_required: true
            is_readonly: false
            required_option: none
            is_hidden: false
          - 
            type: text_set
            field_label: Your Email
            field_name: email
            is_required: true
            is_readonly: false
            required_option: email
            is_hidden: false
          - 
            type: text_set
            field_label: Contact Phone Number
            field_name: phone
            is_required: false
            is_readonly: false
            required_option: number
            is_hidden: false
          - 
            type: textarea_set
            field_label: Message / Enquiry
            field_name: message
            is_required: true
form_name: contact_form
form_heading: Contact Form
form_alignment: left
cf_in_col: true
cf_email_show_details: false
hide_recaptcha: false
forms2sheets: false
cf_save_form_data: false
cf_send_admin_email: true
cf_send_user_email: false
show_file_uploads: false
cf_mailchimp_optin: false
cf_admin_subject: '{{ form_name | deslugify | upper }} - {{ name }} - {{ date format="jS F Y" }}'
cf_success_subject: '{{ form_name | deslugify | upper }} Submission'
cf_user_success_heading: Thank you for your submission.
cf_user_success_text: Your enquiry is very important to us and we endeavour to reply as soon as possible.
cf_user_message_heading: Thank you for your submission.
cf_user_message_text: Your enquiry is very important to us and we endeavour to reply as soon as possible.
has_cf_payment: false
show_privacy_check: false
has_last_page: false
form_bg_opacity: 7
form_bg_hue: dark
form_bg_color: white
form_bg_type: none
title: Contact
content_blocks:
  - 
    type: content_block_set
    block_sets:
      - 
        type: break_set
        break_above: 10
        break_below: 20
        break_border: false
        break_full_width: false
      - 
        type: content_set
        align: left
        kind: normal
        text: |
          ### Contact Us
          
          <i class="fa fa-phone"></i> &nbsp;&nbsp; <a class="text-lg no-td" href="tel:{{ gvcontact:gv_phone regex_replace=' |' }}" onclick="gtag('event', 'clicks', { event_category: 'Phone Number', event_action: 'Clicked'});"> {{ gvcontact:gv_phone }}</a>
          <div class="markdown-br"></div>
          <i class="fa fa-envelope-o"></i>&nbsp;&nbsp; <a class="no-td" href="mailto:{{ gvcontact:gv_email }}">  {{ gvcontact:gv_email }}</a>
          <div class="markdown-br"></div>
        text_default_margin: false
        text_default_margin_bottom: true
      - 
        type: break_set
        break_above: "10"
        break_bg_color: none
        break_full_width: false
    block_set_full_width: true
    block_set_hide: false
no_gv_bg: false
item_heading: Contact Us
header_margin: none
header_slider_speed: 5000
header_slider_show: 1
template: page/contact
fieldset: pages/contact
page_class_override: contact-page
page_menu_kind: default
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: true
toggle_page_menu: false
toggle_hidden: false
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
form_padding_bottom: md
item_heading_color: white
header_size: md
slider_speed: 8000
cf_email_user: true
cf_no_admin_email: false
cf_no_user_email: false
id: aef1a89f-cc42-4814-b35e-55a963dc3789
