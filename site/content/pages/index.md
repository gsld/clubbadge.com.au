title: Home
show_page_auto_content: false
content_blocks:
  - 
    type: content_block_set
    block_set_full_width: true
    block_set_hide: false
    block_bg_settings:
      - 
        bg_margin: xs
        bg_color: dark
        bg_opacity: "10"
        bg_gradient: false
    block_sets:
      - 
        type: content_set
        align: center
        kind: normal
        text_default_margin: false
        text_default_margin_bottom: true
        text: '# Welcome to Club Badge Australia'
  - 
    type: content_block_set
    block_set_full_width: true
    block_set_hide: false
    block_sets:
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
      - 
        type: col_set
        col_equal_height: true
        col_vertical_align: false
        col_full_width: false
        col_widths: threeseven
        add_col_margin: false
        add_col_margin_bottom: false
        col_sets:
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_image_set
                align: left
                img_size: md
                orient: ls
                action: max
                toggle_lightbox: false
                toggle_stacked: true
                show_captions: false
                show_border: false
                run_lazyload: true
                show_full_width: false
                image_mask: none
                file:
                  - /assets/2022/images/site-logo.png
          - 
            type: column_set
            column_content_sets:
              - 
                type: col_text_set
                align: left
                kind: lead
                text_default_margin: false
                text_default_margin_bottom: true
                text: | 
                  Club Badge Australia proudly manufacture and supply custom & commemorative medallions, name badges, lapel pins, key rings, club membership / ID cards (custom lanyards also available) and calisthenics medals. We service a wide range of sporting clubs and corporations including calisthenics, lawn bowls, tennis, cricket, rugby, darts, horse racing, car/motorcycle and RSL Branches. **[Contact us](/contact) for more information or to order.**
      - 
        type: break_set
        break_above: 3sw
        break_bg_color: none
        break_full_width: false
no_gv_bg: true
header_sets:
  - 
    type: slide_set
    slide_bg_color: black
    slide_vertical_align: false
    slide_content_full_width: false
    slide_content_sets:
      - 
        type: slider_break_set
        break_above: 22sw
        break_bg_color: none
        break_full_width: false
    file: /assets/2022/images/headers/club-badge-australia-header1.jpg
    image_height: "500"
  - 
    type: slide_set
    file: /assets/2022/images/headers/club-badge-australia-header2.jpg
    slide_bg_color: black
    image_height: "500"
    slide_vertical_align: false
    slide_content_full_width: false
  - 
    type: slide_set
    file: /assets/2022/images/headers/club-badge-australia-header3.jpg
    slide_bg_color: black
    image_height: "500"
    slide_vertical_align: false
    slide_content_full_width: false
  - 
    type: slide_set
    file: /assets/2022/images/headers/club-badge-australia-header4.jpg
    slide_bg_color: black
    image_height: "500"
    slide_vertical_align: false
    slide_content_full_width: false
  - 
    type: slide_set
    file: /assets/2022/images/headers/club-badge-australia-header5.jpg
    slide_bg_color: black
    image_height: "500"
    slide_vertical_align: false
    slide_content_full_width: false
header_margin: none
header_slider_speed: 5000
header_slider_show: 1
template: page/home
fieldset: default
page_menu_kind: default
page_img_as_header: true
page_bottom_margin_color: none
cache_exclude: false
page_noindex: false
toggle_page_menu: false
toggle_hidden: false
toggle_nav_first: false
toggle_nav_secondary: false
toggle_nav_tertiary: false
toggle_page_nav: false
redirect_do: false
toggle_back: false
toggle_js_back: false
id: 1b1f4836-4686-4fc1-b007-f4b8163987d7
