<?php

return [

    'cheatsheet' => '
<h2 style="margin-top:0;">How to use this Text Editor</h2>

<h3>Headings</h3>
<pre class="language-markdown"><code># This will be an H1 (heading 1)
## This will be an H2 (heading 2)
### This will be an H3 (heading 3)
... up to H6
</code></pre>

<h3>Bold &amp; Italic</h3>
<pre class="language-markdown"><code>You can make things *emphasized*, **bold**, or _**both**_.</code></pre>

<h3>Links</h3>
<pre class="language-markdown"><code>This is an [example link](http://example.com).</code></pre>

<h3>Quoting</h3>

<p>Create a blockquote by starting your text with <code>> </code>.</p>

<pre class="language-markdown"><code>> This is going to be a blockquote.</code></pre>

<h3>Images</h3>
<pre class="language-markdown"><code>![alt text](http://example.com/image.jpg)</code></pre>

<h3>Unordered List</h3>
<pre class="language-markdown"><code>- Bacon
- Steak
- Beer</code></pre>

<h3>Ordered List</h3>
<pre class="language-markdown"><code>1. Eat
2. Drink
3. Be merry</code></pre>

'

];
