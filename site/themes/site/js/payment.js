// Some scripts to handle our payment processing
$().ready(function() {
  // Validate our payment form
  payment_validator = $("#payment-form").validate({
    rules: {
      full_name: "required",
      email: {
        required: true,
        email: true
      },
      desc: "required",
      amount: {
        required: true,
        number: true,
        min: 5
      },
      quantity: {
        required: true,
        number: true,
        min: 1
      },
      gateway: "required"
    },
    messages: {
      full_name: "Please enter your Name",
      email: "Please enter a valid email address",
      amount: "Please enter a valid amount greater than $5",
      desc: "Please enter a description for this payment",
      quantity: "The quantity value is invalid",
      gateway: "Please select payment method",
    },
    submitHandler: handle_payment_submission
  });
  // Validate our quantity fields
  $(".add-cart-buttons form").validate({
    rules: {
      quantity: {
        required: true,
        number: true,
        min: 1
      },
      options: {
        required: true
      }
    },
    messages: {
      quantity: "Please enter a valid quantity",
      options: "Please select a product option",
    }
  });
});

$('.btn-plus').unbind('click').bind('click', function () {
  var value = $(this).siblings('.product-quantity').val();
  value++;
  $(this).siblings('.product-quantity').val(value);
  console.log('Test');
  calculatePrices();
  console.log('Test2');
});
$('.btn-minus').unbind('click').bind('click', function () {
    var value = $(this).siblings('.product-quantity').val();
    value--;
    $(this).siblings('.product-quantity').val(value);
  console.log('Test');
    calculatePrices();
  console.log('Test2');
});

$('#pre-payment-form input.has-price.product-quantity').change(function(e) {
  calculatePrices();
});

// Trigger our payment recalculation when changing form elements or gateways
$('.pre-payment-form .has-price').on('keyup change', function() {
  console.log('Ohai');
  calcPayment();
});
$('.pre-payment-form input[name=payment_type]').on('change', function() {
  calcPayment();
});

function calcPayment() {
  // Calc our selected fields
  var total = 0;
  var val = 0;
  console.log('Calculating total');
  $('.pre-payment-form .has-price:checked').each(function() {
    val = parseFloat($(this).data('price'));
    total += (isNaN(val) ? 0 : val);
  });
  $('.pre-payment-form .has-price[type=text]').each(function() {
    val = parseFloat($(this).val());
    total += (isNaN(val) ? 0 : val);
  });
  console.log('Val: ' + val + ', Total: ' + total);
  // Set the total and levy amount
  var levy = $('.pre-payment-form input[name=payment_type]:checked').data('levy');
  levy = (isNaN(levy) ? 0 : levy / 100 * total);
  $('.levy-amount').html(levy.toFixed(2));
  $('#pre_payment_form_total').val((total + levy).toFixed(2));
  $('#pre_payment_form_subtotal').val(total.toFixed(2));
}

function calculatePrices() {
  console.log('Ohai');
  var levy = $('#pre-payment-form input[name=gateway]:checked').data('levy');
  var gateway = $('#pre-payment-form input[name=gateway]:checked').val();
  var quantity = 0, subtotal = 0, total = 0;

  console.log('Calculating total');
  
  // Check if we have a match for the gateway
  if (gateway != 'undefined' && gateway != '') {
    // Must be a hidden field so grab it
    levy = $('#pre-payment-form input[name=gateway]').data('levy');
    gateway = $('#pre-payment-form input[name=gateway]').val();
  }
  
  // Get our amounts and quantities
  // Loop over quantity fields
    // If the data-price attrib is text, get the amount from that field
    // Otherwise add that price to our total
  // Loop over other elements with data-price's set (how to ensure the quantity-linked fields aren't included??)
  $('.pre-payment-form .has-price:checked').each(function() {
    val = parseFloat($(this).data('price'));
    total += (isNaN(val) ? 0 : val);
  });
  $('.pre-payment-form .has-price[type=text]').each(function() {
    val = parseFloat($(this).val());
    total += (isNaN(val) ? 0 : val);
  });
    // If the data-price attrib is text, get the amount from that field
    // Otherwise add that price to our total
  
  // Handle our quantity (if there are more than 1 item then set the quantity to 1)
  if (num_items > 1) {
    quantity = 1;
  }
  
  // Handle delivery amount
  delivery = $('#pre-payment-form input[name=shipping_option]:checked').val();
  console.log('Delivery: ', delivery);
  if (delivery != 'undefined') {
    if (delivery == 'custom') {
      delivery = $('#pre-payment-form input[name=custom_delivery]').val();
      if (delivery == '') {
        delivery = '0';
      }
      subtotal = (amount * quantity) + parseFloat(delivery);
      $('#pre-payment-form .delivery-price').text(delivery);
    } else if (delivery == 'free') {
      subtotal = amount * quantity;
      $('#pre-payment-form .delivery-price').text('FREE');
    } else {
      subtotal = (amount * quantity) + parseFloat(delivery);
      $('#pre-payment-form .delivery-price').text(delivery);
    }
  }
  
  // Add our current levy
  if (typeof levy != 'undefined' && levy != '') {
    total = subtotal + (subtotal * (parseFloat(levy)) / 100);
    // Show our levy block
    $('#pre-payment-form .' + gateway + '.levy-fee').show();
  } else {
    total = subtotal;
    // Hide our levy block
    $('#pre-payment-form .' + gateway + '.levy-fee').hide();
  }
  
  // Update our payment form with the calulated prices
  $('#pre-payment-form input[name=amount]').val(subtotal.toFixed(2));
  $('#pre-payment-form input[name=amount]').data('price', subtotal.toFixed(2));
  $('#pre-payment-form input[name=total]').val(total.toFixed(2));
  $('#pre-payment-form input[name=quantity]').val(quantity);
  $('#pre-payment-form .levy-amount').each(function() {
    $(this).text((subtotal * (parseFloat($(this).data('levy'))) / 100).toFixed(2));
  });
}
