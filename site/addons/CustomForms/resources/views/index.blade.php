@extends('layout')

@section('content')
<div class="flexy mb-3">
    <h1 class="fill"><a href="{{ cp_route('customforms.index') }}">Form Pages</a></h1>
</div>
<div class="card flush">
    <div class="dossier-table-wrapper">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Page Title</th>
                    <th>URI</th>
                    <th>Form Name</th>
                </tr>
            </thead>
            <tbody>
        @if (empty($forms))
                <tr>
                    <td colspan="7">There are no forms to display</td>
                </tr>
        @else
            @foreach ($forms as $id => $form)
                <tr>
                    <td><a href="{{ $form['editurl'] }}">{{ $form['title'] }}</a></td>
                    <td>{{ $form['uri'] }}</td>
                    <td>{{ $form['formname'] }}</td>
                </tr>
            @endforeach
        @endif
            </tbody>
        </table>
    </div>
</div>
@endsection
