<div class="card">
    <div class="head">
        <h1><a href="{{ cp_route('customforms.index') }}">Recent Form Pages</a></h1>
    </div>
    <div class="card-body">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Page Title</th>
                    <th>Slug</th>
                    <th>Form Name</th>
                </tr>
            </thead>
            <tbody>
        @if (empty($forms))
                <tr>
                    <td colspan="7">There are no forms to display</td>
                </tr>
        @else
            @foreach ($forms as $id => $form)
                <tr>
                    <td><a href="{{ $form['editurl'] }}">{{ $form['title'] }}</a></td>
                    <td>{{ $form['slug'] }}</td>
                    <td>{{ $form['formname'] }}</td>
                </tr>
            @endforeach
        @endif
            </tbody>
        </table>
    </div>
</div>
