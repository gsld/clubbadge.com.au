<?php

namespace Statamic\Addons\CustomForms;

use Statamic\Extend\Controller;

class CustomFormsController extends Controller
{
    private $cf;
    
    public function __construct(CustomForms $cf)
    {
        $this->cf = $cf;
    }
    
    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */
    public function index()
    {
        // Get our list of forms
        $forms = array_reverse($this->cf->getForms());
        // Build our view
        return $this->view('index', ['title' => 'Form Pages', 'forms' => $forms]);
    }
}
