<?php

namespace Statamic\Addons\CustomForms;

use Log;

use Statamic\Extend\Extensible;


class CustomForms
{
    use Extensible;

    public function getForms()
    {
        // Find our Custom Forms pages
        $forms = [];
        $pages = \Statamic\API\Page::all();
        foreach ($pages as $page) {
            $pageData = $page->data();
            // Check if this is a custom form page (and it isn't empty)
            if (isset($pageData['form_pages']) && !empty($pageData['form_pages'])) {
                $forms[$pageData['title']] = [
                    'title' => $pageData['title'],
                    'slug' => $page->slug(),
                    'uri' => $page->uri(),
                    'formname' => $pageData['form_name'],
                    'editurl' => $page->in(default_locale())->editUrl(),
                    'editdate' => $page->lastModified()
                ];
            }
        }
        ksort($forms);
        return $forms;
    }
}
