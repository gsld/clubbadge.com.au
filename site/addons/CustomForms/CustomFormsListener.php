<?php

namespace Statamic\Addons\CustomForms;

use Statamic\Extend\Listener;

use Log;

use Statamic\Contracts\Forms\Submission;
use Statamic\Data\Pages\Page;
use Statamic\API\Config;
use Statamic\API\File;
use Statamic\API\Form;
use Statamic\API\GlobalSet;
use Statamic\API\Nav;
use Statamic\API\YAML;
use Statamic\Forms\Formset;
use Statamic\View\Antlers\Template;

class CustomFormsListener extends Listener
{
    private $fields = [];
    private $columns = [];
    private $adminFiles = [];
    private $name = '';
    private $lastName = '';
    private $email = '';
    private $shipping = [];
    private $billing = [];

    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'cp.nav.created' => 'addNavItems',
        'cp.page.published' => 'checkForm',
        'cp.globals.published' => 'rebuildForms'
    ];

    /**
     * The method for adding our Nav Item
     *
     * @var array
     */
    public function addNavItems($nav)
    {
        // Create the navigation item
        $item = Nav::item('Form Pages')->icon('news')->route('customforms.index');

        // Finally, add our navigation item to the navigation under the 'tools' section.
        $nav->addTo('content', $item);
    }

    /**
     * The method for checking if our entry contains a form and whether we need to create/update a formset
     *
     * @var array
     */
    public function checkForm(Page $page) {
        $pageData = $page->data();
        // Check if we have a form(s) to process
        if (isset($pageData['form_pages']) && !empty($pageData['form_pages'])) {
            $this->parseForm($pageData);
        }
    }

    /**
     * The method for rebuilding all our CF formsets
     *
     */
    public function rebuildForms() {
        $pages = \Statamic\API\Page::all();
        foreach ($pages as $page) {
            $pageData = $page->data();
            // Check if we have a form to process
            if (isset($pageData['form_pages']) && !empty($pageData['form_pages'])) {
                $this->parseForm($pageData);
            }
        }
    }

    private function parseForm($pageData) {
        $this->fields = [];
        $this->columns = [];
        $this->adminFiles = [];
        $this->name = '';
        $this->lastName = '';
        $this->email = '';
        $this->shipping = [];
        $this->billing = [];
        // Try to load any existing formset
        $formset = Form::get($pageData['form_name']);
        $formsetFields = [];
        if (isset($formset)) {
            $formsetFields = $formset->formset()->fields();
        }
        // Loop over the data array looking for form elements
        Log::info('Parsing form: '.$pageData['form_name']);
        foreach ($pageData['form_pages'] as $formPage) {
            if (isset($formPage['cf_page_duplicate'][0]['amount']) && $formPage['cf_page_duplicate'][0]['amount'] > 1) {
                $numPages = $formPage['cf_page_duplicate'][0]['amount'];
            } else {
                $numPages = 1;
            }
            // Loop over the page duplicates (or 1 for no dups)
            for ($j = 0; $j < $numPages; $j++) {
                $prefix = ($numPages > 1 ? $formPage['cf_page_duplicate'][0]['name'].$j.'_' : '');
                $prefixDisplay = ($numPages > 1 ? $formPage['cf_page_duplicate'][0]['label'].' '.($j + 1).' ' : '');
                if ($numPages > 1) {
                    $this->fields[$prefix.'penabled'] = array(
                        'type' => 'text',
                        'display' => $this->getLabel($prefixDisplay.' Enabled')
                    );
                }
                // Loop over the fieldsets looking for fieldsets
                foreach ($formPage['form_page_fieldsets'] as $fieldset) {
                    if ($fieldset['type'] == 'fieldset_set') {
                        // This is a fieldset so lets parse it
                        // Check for a hidden fieldset
                        if (isset($fieldset['fieldset_hidden']) && $fieldset['fieldset_hidden'][0]['set_hidden'] == 1) {
                            $this->fields[$prefix.$fieldset['fieldset_hidden'][0]['set_hidden_name']] = array(
                                'type' => 'text',
                                'display' => $this->getLabel($prefixDisplay.$fieldset['fieldset_hidden'][0]['set_hidden_name'])
                            );
                        }
                        if (isset($fieldset['fieldset_duplicate'][0]['amount']) && $fieldset['fieldset_duplicate'][0]['amount'] > 1) {
                            for ($i = 0; $i < $fieldset['fieldset_duplicate'][0]['amount']; $i++) {
                                $thisPrefix = $prefix.$fieldset['fieldset_duplicate'][0]['name'].'_';
                                $thisPrefixDisplay = $prefixDisplay.$fieldset['fieldset_duplicate'][0]['label'].' ';
                                $this->fields[$thisPrefix.'fenabled_'.$i] = array(
                                    'type' => 'text',
                                    'display' => $this->getLabel($thisPrefixDisplay.$i.' Enabled')
                                );
                                $this->processFormBlocks($pageData, $formPage, $formsetFields, $fieldset, $thisPrefix, '_'.$i, $thisPrefixDisplay);
                            }
                        } else {
                            $this->processFormBlocks($pageData, $formPage, $formsetFields, $fieldset, $prefix, '', $prefixDisplay);
                        }
                    }
                }
            }
        }
        // Check we have a non-empty array of form elements
        if (!empty($this->fields)) {
            // Add our default fields
            $this->fields['form_name'] = array('display' => 'Form Name');
            $this->fields['submission_id'] = array('display' => 'Submission ID');
            $this->fields['submission_date'] = array('display' => 'Submission Date');
            $this->fields['site_name'] = array('display' => 'Site Name');

            // If we have a payment form, add our payment fields
            if ($pageData['has_cf_payment']) {
                $this->fields['cf_payment_total'] = array('display' => 'Payment Total');
                $this->fields['cf_payment_gateway'] = array('display' => 'Payment Method');
                $this->fields['cf_trans_id'] = array('display' => 'Payment Transaction ID');
                $this->fields['cf_payment_summary'] = array('display' => 'Order Summary');
                $this->fields['cf_payment_shipping'] = array('display' => 'Shipping Option');
                if ($pageData['cf_payment_type'] == 'subs') {
                    $this->fields['cf_subscription_timestamp'] = array('display' => 'Payment Timestamp');
                    $this->fields['cf_subscription_id'] = array('display' => 'Subscription Plan ID');
                    $this->fields['cf_subscription_stripe_cust_id'] = array('display' => 'Subscription Customer ID');
                    $this->fields['cf_subscription_result'] = array('display' => 'Subscription ID');
                    $this->fields['cf_subscription_amount'] = array('display' => 'Subscription Price');
                    $this->fields['cf_subscription_quantity'] = array('display' => 'Subscription Quantity');
                    $this->fields['cf_subscription_product_id'] = array('display' => 'Subscription Product ID');
                    $this->fields['cf_subscription_interval'] = array('display' => 'Subscription Interval (eg month)');
                    $this->fields['cf_subscription_interval_count'] = array('display' => 'Subscription Interval Count (eg 3 = quarterly)');
                } else {
                    $this->fields['cf_trans_count'] = array('display' => 'Payment Simple Transaction ID');
                    $this->fields['cf_payment_amount'] = array('display' => 'Payment Amount (without levy)');
                    $this->fields['cf_payment_tax'] = array('display' => 'Payment Tax');
                    $this->fields['cf_payment_desc'] = array('display' => 'Payment Description');
                    $this->fields['cf_payment_item_id'] = array('display' => 'Payment Item (the form name)');
                    $this->fields['cf_payment_gw_levy'] = array('display' => 'Payment Gateway Levy');
                    $this->fields['cf_payment_timestamp'] = array('display' => 'Payment Timestamp');
                    $this->fields['cf_payment_currency'] = array('display' => 'Payment Currency');
                    $this->fields['cf_card_type'] = array('display' => 'Card Type');
                    $this->fields['cf_card_account'] = array('display' => 'Card Account Type');
                    $this->fields['cf_card_ending'] = array('display' => 'Card Ending');
                }
            }

            // If we have a Mailchimp config, add our mailchimp field
            if (isset($pageData['cf_mailchimp_id']) && $pageData['cf_mailchimp_id'] != '') {
                $this->fields['cf_mailchimp_signup'] = array('display' => 'Mailchimp Signup');
            }

            // If we have a Privacy Check question, add the field
            if (isset($pageData['show_privacy_check']) && $pageData['show_privacy_check'] == 'true') {
                $this->fields['privacy_check_agree_i_agree'] = array('display' => 'Privacy Check');
            }

            // Check if we have a column to show, if not show the first field
            if (empty($this->columns)) {
                $this->columns[] = array_keys($this->fields)[0];
            }
            array_unshift($this->columns, 'form_name');
            array_unshift($this->columns, 'submission_id');
            array_unshift($this->columns, 'submission_date');

            // Check if we are using Forms2Sheets for this form
            if (isset($pageData['forms2sheets']) && $pageData['forms2sheets']) {
                $sheetID = '';
                try {
                    // Check the Forms2Sheets addon is installed
                    $api = $this->api('FormsToSheets');
                    // Check if need to create our spreadsheet
                    if (!isset($pageData['google_sheet_id']) || $pageData['google_sheet_id'] == '') {
                        // Create this sheet
                        $sheetID = $this->api('FormsToSheets')->create($pageData['form_heading'], $this->fields);
                        $page->set('google_sheet_id', $sheetID);
                        $page->save();
                        Log::info('Sheet created: '.$sheetID);
                    } else {
                        // Edit this sheet
                        $this->api('FormsToSheets')->edit($pageData['google_sheet_id'], $this->fields);
                        $sheetID = $pageData['google_sheet_id'];
                        Log::info('Sheet edited: '.$sheetID);
                    }
                    $this->fields['google_sheet_id'] = array('display' => 'Google Sheet ID', 'value' => $sheetID);
                } catch (ApiNotFoundException $e) {
                    // Addon isn't installed so log an error and continue
                    Log::error('Forms2Sheets addon requested, but not installed');
                }
            }

            // Build our email body partials
            $this->buildEmailTemplate($pageData);

            // Build the formset
            $formset = new Formset();
            $formset->name($pageData['form_name']);
            $addr = (!isset($pageData['cf_send_admin_email']) || $pageData['cf_send_admin_email'] ? $this->getConfig('admin_email') : '').(!empty($pageData['cf_admin_emails']) ? ', '.$pageData['cf_admin_emails'] : '');
            $email_admin = [
                'to' => trim($addr, ", \t\n\r\0\x0B"),
                'bcc' => $this->getConfig('admin_bcc_email'),
                'from_email' => $this->getConfig('admin_from_email'),
                'subject' => (!empty($pageData['cf_admin_subject']) ? $pageData['cf_admin_subject'] : $this->getConfig('admin_subject')),
                'template' => 'output/cf-workflow'
            ];
            if ($this->email != '') {
                $email_admin['reply_to'] = '{{ email }}';
                if (!isset($pageData['cf_send_user_email']) || $pageData['cf_send_user_email']) {
                    $email_user = [
                        'to' => '{{ email }}',
                        'bcc' => $this->getConfig('success_bcc_email'),
                        'from_email' => $this->getConfig('success_from_email'),
                        'reply_to' => $this->getConfig('success_reply_to_email'),
                        'subject' => (!empty($pageData['cf_success_subject']) ? $pageData['cf_success_subject'] : $this->getConfig('success_subject')),
                        'template' => 'output/cf-success'
                    ];
                }
            }
            $formset->data(array(
                'title' => $pageData['form_heading'],
                'honeypot' => 'website1',
                'columns' => $this->columns,
                'fields' => $this->fields,
                'email' => isset($email_user) ? [$email_admin, $email_user] : $email_admin
            ));
            $formset->save();

            // Ensure this form is in the reCaptcha config
            $recaptcha = YAML::parse(File::get(settings_path("addons/recaptcha.yaml")));
            if (isset($pageData['hide_recaptcha']) && !$pageData['hide_recaptcha']) {
                if (!in_array($pageData['form_name'], $recaptcha['forms'])) {
                    Log::info('Adding form to recaptcha configuration');
                    $recaptcha['forms'][] = $pageData['form_name'];
                    File::put(settings_path("addons/recaptcha.yaml"), YAML::dump($recaptcha));
                } else {
                    Log::info('Form already in recaptcha configuration');
                }
            } else {
                Log::info('Removing form from recaptcha configuration');
                $recaptcha['forms'] = array_values(array_diff($recaptcha['forms'], array($pageData['form_name'])));
                File::put(settings_path("addons/recaptcha.yaml"), YAML::dump($recaptcha));
            }

            // Check Mailchimp for this form
            if (isset($pageData['cf_mailchimp_id']) && $pageData['cf_mailchimp_id'] != '') {
                $mailchimp = YAML::parse(File::get(settings_path("addons/mailchimp.yaml")));
                // Check if we need to add this form to the Mailchimp config
                $done = FALSE;
                if (isset($mailchimp['forms'])) {
                    foreach ($mailchimp['forms'] as &$form) {
                        if ($form['form'] == $pageData['form_name']) {
                            $done = TRUE;
                            Log::info('Form already in Mailchimp configuration, updating settings');
                            $form = [
                                'mailchimp_list_id' => $pageData['cf_mailchimp_id'],
                                'disable_opt_in' => $pageData['cf_mailchimp_optin'] ?? FALSE,
                                'form' => $pageData['form_name'],
                                'check_permission' => TRUE,
                                'permission_field' => 'cf_mailchimp_signup',
                            ];
                            File::put(settings_path("addons/mailchimp.yaml"), YAML::dump($mailchimp));
                        }
                    }
                }
                if (!$done) {
                    // Add this form to the Mailchimp config
                    Log::info('Adding form to Mailchimp configuration');
                    $mailchimp['forms'][] = [
                        'mailchimp_list_id' => $pageData['cf_mailchimp_id'],
                        'disable_opt_in' => $pageData['cf_mailchimp_optin'] ?? FALSE,
                        'form' => $pageData['form_name'],
                        'check_permission' => FALSE,
                    ];
                    File::put(settings_path("addons/mailchimp.yaml"), YAML::dump($mailchimp));
                }
            }
        }
    }

    private function processFormBlocks($pageData, $formPage, $formsetFields, $fieldset, $prefix, $suffix, $prefixDisplay) {
        // Loop over the form block grabbing our fields
        foreach ($fieldset['form_blocks'] as $formBlock) {
            // Parse all the types of fields
            if ($formBlock['type'] == 'address_set') {
                $parts = ['building','street','town','state','postcode','country'];
                foreach ($parts as $part) {
                    $this->fields[$prefix.$formBlock['field_name'].'_'.$part.$suffix] = array('display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].'_'.$part.$suffix));
                }
                if (substr($formBlock['field_name'], 0, 8) == 'shipping' && $prefix == '' && $suffix == '') {
                    foreach ($parts as $part) {
                        $this->shipping[$formBlock['field_name'].'_'.$part] = $this->fields[$formBlock['field_name'].'_'.$part]['display'];
                    }
                } elseif (substr($formBlock['field_name'], 0, 7) == 'billing' && $prefix == '' && $suffix == '') {
                    foreach ($parts as $part) {
                        $this->billing[$formBlock['field_name'].'_'.$part] = $this->fields[$formBlock['field_name'].'_'.$part]['display'];
                    }
                }
            } elseif ($formBlock['type'] == 'upload_set') {
                // Add our field
                $dirPrefix = '';
                // Check if we need an obscured directory and whether one already exists
                if (isset($formsetFields) && isset($formsetFields[$formBlock['field_name']]) && isset($formsetFields[$formBlock['field_name']]['folder'])) {
                    $folder = $formsetFields[$formBlock['field_name']]['folder'];
                    // Grab the existing obscured directory string, if there is one
                    $start = stripos($folder, $pageData['form_name']) + strlen($pageData['form_name']) + 1;
                    $end = strripos($folder, $formBlock['field_name']);
                    $dirPrefix = substr($folder, $start, $end - $start);
                    if ($dirPrefix == '') {
                        $dirPrefix = bin2hex(random_bytes(12)).'/';
                    }
                } else {
                    $dirPrefix = bin2hex(random_bytes(12)).'/';
                }
                $this->fields[$prefix.$formBlock['field_name'].$suffix] = array(
                    'type' => 'asset',
                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix),
                    'container' => 'main',
                    'folder' => 'form_uploads/'.$pageData['form_name'].'/'.$dirPrefix.$formBlock['field_name']
                );
            } elseif ($formBlock['type'] == 'textarea_set') {
                $this->fields[$prefix.$formBlock['field_name'].$suffix] = array(
                    'type' => 'textarea',
                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix)
                );
            } elseif ($formBlock['type'] == 'hidden_textarea_set') {
                $this->fields[$prefix.$formBlock['field_name'].'_question'.$suffix] = array(
                    'type' => 'textarea',
                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.' Question')
                );
                $this->fields[$prefix.$formBlock['field_name'].$suffix] = array(
                    'type' => 'textarea',
                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix)
                );
            } elseif ($formBlock['type'] == 'selectable_set' || $formBlock['type'] == 'payment_single_set') {
                // Check if we have checkboxes because we need to create fields for each option
                if ($formBlock['options_type'] == 'checkbox') {
                    foreach ($formBlock['field_options'] as $opts) {
                        $this->fields[$prefix.$formBlock['field_name'].'_'.($opts['type'] == 'options_other_set' ? 'other' : $this->clean($opts['option_value'])).$suffix] = array(
                            'type' => 'text',
                            'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.'-'.$opts['option_text'])
                        );
                        // Check if we have an "other" option and create the text field for it
                        if ($opts['type'] == 'options_other_set') {
                            $this->fields[$prefix.$formBlock['field_name'].'_other_value'.$suffix] = array(
                                'type' => 'text',
                                'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.'-'.$opts['option_text'])
                            );
                            if ($formBlock['type'] == 'payment_single_set') {
                                $this->fields[$prefix.$formBlock['field_name'].'_other_amount'.$suffix] = array(
                                    'type' => 'text',
                                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.'-'.$opts['option_text'].' Amount')
                                );
                            }
                        }
                    }
                } else {
                    $this->fields[$prefix.$formBlock['field_name']] = array(
                        'type' => 'text',
                        'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'])
                    );
                    // Check if we have an "other" option and create the text field for it
                    if (isset($formBlock['field_options'])) {
                        foreach ($formBlock['field_options'] as $opts) {
                            if ($opts['type'] == 'options_other_set') {
                                $this->fields[$prefix.$formBlock['field_name'].'_other_value'.$suffix] = array(
                                    'type' => 'text',
                                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.'-'.$opts['option_text'])
                                );
                                if ($formBlock['type'] == 'payment_single_set') {
                                    $this->fields[$prefix.$formBlock['field_name'].'_other_amount'.$suffix] = array(
                                        'type' => 'text',
                                        'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.'-'.$opts['option_text'].' Amount')
                                    );
                                }
                            }
                        }
                    }
                }
            } elseif ($formBlock['type'] == 'payment_multiple_set') {
                $this->fields[$prefix.$formBlock['field_name'].$suffix] = array(
                    'type' => 'text',
                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix)
                );
                $this->fields[$prefix.$formBlock['field_name'].'_qty'.$suffix] = array(
                    'type' => 'text',
                    'display' => $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix.' Quantity')
                );
            } elseif ($formBlock['type'] == 'provision_set') {
                $this->adminFiles = array_merge($this->adminFiles, $formBlock['admin_files']);
            } elseif (isset($formBlock['field_name'])) {
                $field = [];
                $field['display'] = $this->getLabel($prefixDisplay.$formBlock['field_name'].$suffix);
                // Check for special name and email fields
                if ((!empty($pageData['cf_email_first_name_field']) && $formBlock['field_name'] == $pageData['cf_email_first_name_field']) || $formBlock['field_name'] == 'name' || $formBlock['field_name'] == 'firstname' || $formBlock['field_name'] == 'first_name') {
                    if ($prefix == '') {
                        $this->columns[] = $formBlock['field_name'];
                        $this->name = $formBlock['field_name'];
                    }
                } elseif ((!empty($pageData['cf_email_last_name_field']) && $formBlock['field_name'] == $pageData['cf_email_last_name_field']) || $formBlock['field_name'] == 'surname' || $formBlock['field_name'] == 'lastname' || $formBlock['field_name'] == 'last_name') {
                    if ($prefix == '') {
                        $this->columns[] = $formBlock['field_name'];
                        $this->lastName = $formBlock['field_name'];
                    }
                } elseif ($formBlock['field_name'] == 'email') {
                    $field['validate'] = 'email';
                    if ($prefix == '') {
                        $this->columns[] = $formBlock['field_name'];
                        $this->email = $formBlock['field_name'];
                    }
                }
                $this->fields[$prefix.$formBlock['field_name'].$suffix] = $field;
            }
            // Look for shipping or billing fields that aren't the address cos fml
            if (isset($formBlock['field_name']) && $formBlock['type'] != 'address_set') {
                if (substr($formBlock['field_name'], 0, 8) == 'shipping' && $prefix == '') {
                    $this->shipping[$formBlock['field_name']] = $this->fields[$formBlock['field_name']]['display'];
                } elseif (substr($formBlock['field_name'], 0, 7) == 'billing' && $prefix == '') {
                    $this->billing[$formBlock['field_name']] = $this->fields[$formBlock['field_name']]['display'];
                }
            }
        }
    }

    private function buildEmailTemplate($pageData) {
            $ignore = ['cf_trans_id', 'cf_payment_desc', 'cf_payment_gateway', 'cf_payment_total', 'cf_card_ending', 'cf_payment_amount',
                'submission_id', 'site_name', 'cf_subscription_timestamp', 'cf_subscription_id', 'cf_subscription_stripe_cust_id',
                'cf_subscription_result', 'cf_subscription_amount', 'cf_subscription_quantity', 'cf_subscription_product_id',
                'cf_subscription_interval', 'cf_subscription_interval_count', 'cf_payment_amount', 'cf_payment_item_id', 'cf_payment_gw_levy',
                'cf_payment_timestamp', 'cf_card_type', 'cf_card_account', 'google_sheet_id'
            ];
            $header_ignore = [$this->name, 'email', 'cf_trans_id', 'cf_trans_count', 'cf_payment_summary', 'cf_payment_desc', 'cf_payment_gateway',
                'cf_payment_total'
            ];
            // If we have a lastname field, add it to our header ignores
            if ($this->lastName != '') {
                $header_ignore[] = $this->lastName;
            }

            $fields_csv = '';
            $fields_text = '';
            $fields_html = '';

            // Check if we have a billing address and assume that we need mf's damn shipping structure thing fml // soz mf
            if (!empty($this->billing)) {
                $fields_html .= "<div style='font-size: 15px;line-height: 1.4'>";
                $fields_text .= "{{ if cf_payment_shipping ~ '/pickup/i' }}\n";
                $fields_html .= "{{ if cf_payment_shipping ~ '/pickup/i' }}\n";
                $fields_text .= GlobalSet::whereHandle("gvcustomshop")->get('shipping_email_pickup')."\n\n";
                $fields_html .= GlobalSet::whereHandle("gvcustomshop")->get('shipping_email_pickup')."</div>";
                $fields_text .= "{{ else }}\n";
                $fields_html .= "{{ else }}\n";
                $fields_text .= GlobalSet::whereHandle("gvcustomshop")->get('shipping_email_delivery')."\n\n";
                $fields_html .= GlobalSet::whereHandle("gvcustomshop")->get('shipping_email_delivery')."\n\n";
                if (!empty($this->shipping)) {
                    $fields_text .= "{{ if same_shipping_address == 'No' }}\n";
                    $fields_html .= "{{ if same_shipping_address == 'No' }}\n";
                    $fields_text .= "{{ shipping_first_name }} {{ shipping_last_name }} - ";
                    $fields_html .= "{{ shipping_first_name }} {{ shipping_last_name }} - ";
                    $fields_text .= "{{ if shipping_organisation }}{{ shipping_organisation }}{{ /if }} ";
                    $fields_html .= "{{ if shipping_organisation }}{{ shipping_organisation }}{{ /if }} ";
                    $fields_text .= "{{ if shipping_address_building }}{{ shipping_address_building }}{{ /if }} ";
                    $fields_html .= "{{ if shipping_address_building }}{{ shipping_address_building }}{{ /if }} ";
                    $fields_text .= "{{ shipping_address_street }} {{ shipping_address_town }} {{ shipping_address_state }} {{ shipping_address_postcode }} {{ shipping_address_country }}\n";
                    $fields_html .= "{{ shipping_address_street }} {{ shipping_address_town }} {{ shipping_address_state }} {{ shipping_address_postcode }} {{ shipping_address_country }}\n";
                    $fields_text .= "{{ else }}\n";
                    $fields_html .= "{{ else }}\n";
                }
                $fields_text .= "{{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}, ";
                $fields_html .= "{{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}, ";
                $fields_text .= "{{ if billing_organisation }}{{ billing_organisation }}{{ /if }} ";
                $fields_html .= "{{ if billing_organisation }}{{ billing_organisation }}{{ /if }} ";
                $fields_text .= "{{ if billing_building }}{{ billing_building }}{{ /if }} ";
                $fields_html .= "{{ if billing_building }}{{ billing_building }}{{ /if }} ";
                $fields_text .= "{{ billing_street }} {{ billing_town }} {{ billing_state }} {{ billing_postcode }} {{ billing_country }}";
                $fields_html .= "{{ billing_street }} {{ billing_town }} {{ billing_state }} {{ billing_postcode }} {{ billing_country }}";
                if (!empty($this->shipping)) {
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                }
                $fields_text .= " - if this is incorrect, please contact us immediately on ".GlobalSet::whereHandle("gvcontact")->get('gv_phone')."\n\n";
                $fields_html .= " - if this is incorrect, please contact us immediately on ".GlobalSet::whereHandle("gvcontact")->get('gv_phone')."</div>";
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
                
                // Add our order details
                $fields_text .= "\n==== Order Summary\n{{ cf_payment_summary }}\n";
                $fields_html .= "<br />\n<h3>Order Summary</h3><hr />\n<div>\n{{ cf_payment_summary | payamic }}\n</div>\n";

                // Add these fields to our header ignore array
                $header_ignore = array_merge($header_ignore, array_keys($this->shipping), array_keys($this->billing));
                // Build our Shopify style address details shiznix-mehness
                $fields_text .= "\n==== Customer & Shipping Information\n";
                $fields_html .= "<br />\n<h3>Customer & Shipping Information</h3><hr />\n";
                $fields_text .= "Shipping Address:\n";
                $fields_html .= "<table border='0' cellpadding='0' cellspacing='0' width='560' id='templateColumns'>\n<tr><td align='center' valign='top' width='50%' class='templateColumnContainer'>\n";
                $fields_html .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td valign='top' class='leftColumnContent'>\n";
                $fields_html .= "<strong>Shipping Address:</strong><br />\n";
                // Handle possibly having separate a shipping address but maybe not
                if (!empty($this->shipping)) {
                    $fields_text .= "{{ if same_shipping_address == 'No' }}\n";
                    $fields_html .= "{{ if same_shipping_address == 'No' }}\n";
                    $fields_text .= "{{ shipping_first_name }} {{ shipping_last_name }}\n";
                    $fields_html .= "{{ shipping_first_name }} {{ shipping_last_name }}<br />\n";
                    $fields_text .= "{{ if shipping_organisation }}\n";
                    $fields_html .= "{{ if shipping_organisation }}\n";
                    $fields_text .= "{{ shipping_organisation }}\n";
                    $fields_html .= "{{ shipping_organisation }}<br />\n";
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                    $fields_text .= "{{ if shipping_address_building }}\n";
                    $fields_html .= "{{ if shipping_address_building }}\n";
                    $fields_text .= "{{ shipping_address_building }}\n";
                    $fields_html .= "{{ shipping_address_building }}<br />\n";
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                    $fields_text .= "{{ shipping_address_street }}\n";
                    $fields_html .= "{{ shipping_address_street }}<br />\n";
                    $fields_text .= "{{ shipping_address_town }} {{ shipping_address_state }} {{ shipping_address_postcode }}\n";
                    $fields_html .= "{{ shipping_address_town }} {{ shipping_address_state }} {{ shipping_address_postcode }}<br />\n";
                    $fields_text .= "{{ shipping_address_country }}\n";
                    $fields_html .= "{{ shipping_address_country }}<br />\n";
                    $shipping = array_diff_key($this->shipping, ['shipping_first_name' => '', 'shipping_last_name' => '', 'shipping_organisation' => '', 'shipping_address_building' => '', 'shipping_address_street' => '', 'shipping_address_town' => '', 'shipping_address_state' => '', 'shipping_address_postcode' => '', 'shipping_address_country' => '']);
                    foreach ($shipping as $name => $display) {
                        $fields_html .= "{{ if ".$name." }}{{ ".$name." }}{{ /if }}<br />\n";
                        $fields_text .= "{{ if ".$name." }}\n".$display.": {{ ".$name." }}\n{{ /if }}\n";
                    }
                    $fields_text .= "{{ else }}\n";
                    $fields_html .= "{{ else }}\n";
                }
                $fields_text .= "{{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}\n";
                $fields_html .= "{{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}<br />\n";
                $fields_text .= "{{ if billing_organisation }}\n";
                $fields_html .= "{{ if billing_organisation }}\n";
                $fields_text .= "{{ billing_organisation }}\n";
                $fields_html .= "{{ billing_organisation }}<br />\n";
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
                $fields_text .= "{{ if billing_building }}\n";
                $fields_html .= "{{ if billing_building }}\n";
                $fields_text .= "{{ billing_building }}\n";
                $fields_html .= "{{ billing_building }}<br />\n";
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
                $fields_text .= "{{ billing_street }}\n";
                $fields_html .= "{{ billing_street }}<br />\n";
                $fields_text .= "{{ billing_town }} {{ billing_state }} {{ billing_postcode }}\n";
                $fields_html .= "{{ billing_town }} {{ billing_state }} {{ billing_postcode }}<br />\n";
                $fields_text .= "{{ billing_country }}\n";
                $fields_html .= "{{ billing_country }}<br />\n";
                $billing = array_diff_key($this->billing, ['billing_first_name' => '', 'billing_last_name' => '', 'billing_organisation' => '', 'billing_building' => '', 'billing_street' => '', 'billing_town' => '', 'billing_state' => '', 'billing_postcode' => '', 'billing_country' => '']);
                foreach ($billing as $name => $display) {
                    $fields_html .= "{{ if ".$name." }}".$display.": {{ ".$name." }}{{ /if }}<br />\n";
                    $fields_text .= "{{ if ".$name." }}\n".$display.": {{ ".$name." }}\n{{ /if }}\n";
                }
                if (!empty($this->shipping)) {
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                }
                $fields_text .= "\nBilling Address:\n";
                $fields_html .= "</td></tr></table>\n";
                $fields_html .= "</td><td align='center' valign='top' width='50%' class='templateColumnContainer'>";
                $fields_html .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td valign='top' class='rightColumnContent'>\n";
                $fields_html .= "<strong>Billing Address:</strong><br />\n";
                $fields_text .= "{{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}\n";
                $fields_html .= "{{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}<br />\n";
                $fields_text .= "{{ ".$this->email." }}\n";
                $fields_html .= "{{ ".$this->email." }}<br />\n";
                $fields_text .= "{{ if billing_organisation }}\n";
                $fields_html .= "{{ if billing_organisation }}\n";
                $fields_text .= "{{ billing_organisation }}\n";
                $fields_html .= "{{ billing_organisation }}<br />\n";
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
                $fields_text .= "{{ if billing_building }}\n";
                $fields_html .= "{{ if billing_building }}\n";
                $fields_text .= "{{ billing_building }}\n";
                $fields_html .= "{{ billing_building }}<br />\n";
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
                $fields_text .= "{{ billing_street }}\n";
                $fields_html .= "{{ billing_street }}<br />\n";
                $fields_text .= "{{ billing_town }} {{ billing_state }} {{ billing_postcode }}\n";
                $fields_html .= "{{ billing_town }} {{ billing_state }} {{ billing_postcode }}<br />\n";
                $fields_text .= "{{ billing_country }}\n";
                $fields_html .= "{{ billing_country }}<br />\n";
                foreach ($billing as $name => $display) {
                    $fields_html .= "{{ if ".$name." }}".$display.": {{ ".$name." }}{{ /if }}<br />\n";
                    $fields_text .= "{{ if ".$name." }}\n".$display.": {{ ".$name." }}\n{{ /if }}\n";
                }
                $fields_html .= "</td></tr></table>\n";
                $fields_html .= "</td></tr></table>\n";
                $fields_html .= "<table border='0' cellpadding='0' cellspacing='0' width='560' id='templateColumns'>\n<tr><td align='center' valign='top' width='50%' class='templateColumnContainer'>\n";
                $fields_html .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td valign='top' class='leftColumnContent'>\n";
                // Shipping option details because fml
                $fields_text .= "\nShipping Method:\n";
                $fields_html .= "<br /><strong>Shipping Method:</strong><br />\n";
                foreach (GlobalSet::whereHandle("gvcustomshop")->get('shipping') as $shipping) {
                    $fields_text .= "{{ if cf_payment_shipping == '".$shipping['shipping_id']."' }}\n".$shipping['shipping_name']."\n{{ /if }}\n";
                    $fields_html .= "{{ if cf_payment_shipping == '".$shipping['shipping_id']."' }}".$shipping['shipping_name']."{{ /if }}\n";
                }
                $fields_html .= "</td></tr></table>\n";
                $fields_html .= "</td><td align='center' valign='top' width='50%' class='templateColumnContainer'>";
                $fields_html .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td valign='top' class='rightColumnContent'>\n";
                // Payment details
                $fields_text .= "\nPayment Method:\n";
                $fields_html .= "<br /><strong>Payment Method:</strong><br />\n";
                foreach (GlobalSet::whereHandle("gvcustomform")->get('cf_payment_gateways') as $gateway) {
                    if (isset($pageData['show_gateways']) && in_array($gateway['gateway_value'], $pageData['show_gateways'])) {
                        $fields_text .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n".$gateway['gateway_name']."\n{{ /if }}\n";
                        $fields_html .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}".$gateway['gateway_name']."{{ /if }}\n";
                    }
                }
                $fields_text .= "{{ if cf_payment_gateway == 'stripe' }}\n";
                $fields_html .= "{{ if cf_payment_gateway == 'stripe' }}\n";
                $fields_text .= "\nPaid {{ partial:custom-form/currency-symbol }}{{ cf_payment_total }} with card ending in {{ cf_card_ending }}\n";
                $fields_html .= "<br />\nPaid {{ partial:custom-form/currency-symbol }}{{ cf_payment_total }} with card ending in {{ cf_card_ending }}<br />\n";
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
                $fields_html .= "</td></tr></table>\n";
                $fields_html .= "</td></tr></table>\n";
                $fields_text .= "\n";
                $fields_html .= "<br />\n";
                // Show our gateway details if set because omg
                foreach (GlobalSet::whereHandle("gvcustomform")->get('cf_payment_gateways') as $gateway) {
                    $fields_text .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n";
                    $fields_html .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n";
                    if (isset($pageData['cf_gateway_email_overrides']) && $pageData['cf_gateway_email_overrides']) {
                        if ($gateway['gateway_value'] == 'stripe' && isset($pageData['cf_stripe_email_instructions']) && $pageData['cf_stripe_email_instructions'] != '') {
                            $fields_text .= $pageData['cf_stripe_email_instructions']."\n";
                            $fields_html .= $pageData['cf_stripe_email_instructions']."\n";
                        } elseif ($gateway['gateway_value'] == 'offline' && isset($pageData['cf_offline_email_instructions']) && $pageData['cf_offline_email_instructions'] != '') {
                            $fields_text .= $pageData['cf_offline_email_instructions']."\n";
                            $fields_html .= $pageData['cf_offline_email_instructions']."\n";
                        } elseif ($gateway['gateway_value'] == 'other' && isset($pageData['cf_other_email_instructions']) && $pageData['cf_other_email_instructions'] != '') {
                            $fields_text .= $pageData['cf_other_email_instructions']."\n";
                            $fields_html .= $pageData['cf_other_email_instructions']."\n";
                        } else {
                            if (isset($gateway['gateway_email_text']) && $gateway['gateway_email_text'] != '') {
                                $fields_text .= $this->parseGlobals($gateway['gateway_email_text'])."\n";
                            }
                            if (isset($gateway['gateway_email_html']) && $gateway['gateway_email_html'] != '') {
                                $fields_html .= $this->parseGlobals($gateway['gateway_email_html'])."\n";
                            }
                        }
                    } else {
                        if (isset($gateway['gateway_email_text']) && $gateway['gateway_email_text'] != '') {
                            $fields_text .= $this->parseGlobals($gateway['gateway_email_text'])."\n";
                        }
                        if (isset($gateway['gateway_email_html']) && $gateway['gateway_email_html'] != '') {
                            $fields_html .= $this->parseGlobals($gateway['gateway_email_html'])."\n";
                        }
                    }
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                }
            } else {
                $fields_text .= "==== Contact Details\n";
                $fields_html .= "<h3>Contact Details</h3><hr />\n";
                if ($this->name != '') {
                    $fields_text .= "Name: {{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}\n";
                    $fields_html .= "<strong>Name:</strong> {{ ".$this->name.($this->lastName != '' ? ' }} {{ '.$this->lastName : '')." }}<br />\n";
                }
                if ($this->email != '') {
                    $fields_text .= "Email: {{ ".$this->email." }}\n";
                    $fields_html .= "<strong>Email:</strong> {{ ".$this->email." }}<br />\n";
                }
                // Check if we have an address field block to show in the header section
                if (!empty($pageData['cf_email_address_field']) && isset($this->fields[$pageData['cf_email_address_field'].'_street'])) {
                    // Add these fields to our header ignore array
                    $header_ignore = array_merge($header_ignore, [
                        $pageData['cf_email_address_field'].'_building',
                        $pageData['cf_email_address_field'].'_street',
                        $pageData['cf_email_address_field'].'_town',
                        $pageData['cf_email_address_field'].'_state',
                        $pageData['cf_email_address_field'].'_postcode',
                        $pageData['cf_email_address_field'].'_country'
                    ]);
                    // Build the template bits
                    $fields_text .= "Address:\n";
                    $fields_text .= "{{ if ".$pageData['cf_email_address_field'].'_building'." }}{{ ".$pageData['cf_email_address_field'].'_building'." }}, {{ /if }}{{ ".$pageData['cf_email_address_field'].'_street'." }}\n";
                    $fields_text .= "{{ ".$pageData['cf_email_address_field'].'_town'." }} {{ ".$pageData['cf_email_address_field'].'_state'." }} {{ ".$pageData['cf_email_address_field'].'_postcode'." }}\n";
                    $fields_text .= "{{ ".$pageData['cf_email_address_field'].'_country'." }}\n";
                    $fields_html .= "<strong>Address:</strong><br />\n";
                    $fields_html .= "{{ if ".$pageData['cf_email_address_field'].'_building'." }}{{ ".$pageData['cf_email_address_field'].'_building'." }}, {{ /if }}{{ ".$pageData['cf_email_address_field'].'_street'." }}<br />\n";
                    $fields_html .= "{{ ".$pageData['cf_email_address_field'].'_town'." }} {{ ".$pageData['cf_email_address_field'].'_state'." }} {{ ".$pageData['cf_email_address_field'].'_postcode'." }}<br />\n";
                    $fields_html .= "{{ ".$pageData['cf_email_address_field'].'_country'." }}<br />\n";
                }
                if (!empty($pageData['cf_email_custom_fields'])) {
                    // Add these fields to our header ignore array
                    $header_ignore = array_merge($header_ignore, $pageData['cf_email_custom_fields']);
                    // Build the template bits
                    foreach ($pageData['cf_email_custom_fields'] as $field) {
                        if (isset($this->fields[$field])) {
                            $this->parseField($fields_text, $fields_html, $field, $this->fields[$field], $pageData);
                        }
                    }
                }
                if ($pageData['has_cf_payment']) {
                    $fields_text .= "\n==== Order Summary\n{{ cf_payment_summary }}\n";
                    $fields_html .= "<br />\n<h3>Order Summary</h3><hr />\n<div>\n{{ cf_payment_summary | payamic }}\n</div>\n";
                    $fields_text .= "\n==== Transaction Details\n";
                    $fields_html .= "<br />\n<h3>Transaction Details</h3><hr />\n";
                    if ($pageData['cf_payment_type'] == 'subs') {
                        $fields_text .= "Submission / Order ID: {{ cf_trans_id }}\n";
                        $fields_html .= "<strong>Submission / Order ID:</strong> {{ cf_trans_id }}<br />\n";
                        foreach (GlobalSet::whereHandle("gvcustomform")->get('cf_payment_gateways') as $gateway) {
                            if (isset($pageData['show_gateways']) && in_array($gateway['gateway_value'], $pageData['show_gateways'])) {
                                $fields_text .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n";
                                $fields_html .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n";
                                $fields_text .= "Payment Method: ".$gateway['gateway_name']."\n";
                                $fields_html .= "<strong>Payment Method:</strong> ".$gateway['gateway_name']."<br /><br />\n";
                                $fields_text .= "{{ /if }}\n";
                                $fields_html .= "{{ /if }}\n";
                            }
                        }
                        $fields_text .= "Amount Paid: {{ cf_payment_total }}\n";
                        $fields_html .= "<strong>Amount Paid:</strong> {{ cf_payment_total }}<br />\n";
                        $fields_text .= "Subscription Frequency: {{ cf_subscription_interval }}\n";
                        $fields_html .= "<strong>Subscription Frequency</strong>: {{ cf_subscription_interval }}<br />\n";
                        $fields_text .= "Plan ID: {{ cf_subscription_id }}\n";
                        $fields_html .= "<strong>Plan ID</strong>: {{ cf_subscription_id }}<br />\n";
                    } else {
                        $fields_text .= "Submission / Order ID: {{ cf_trans_count }}\n";
                        $fields_html .= "<strong>Submission / Order ID:</strong> {{ cf_trans_count }}<br />\n";
                        $fields_text .= "Payment Description: {{ cf_payment_desc }}\n";
                        $fields_html .= "<strong>Payment Description:</strong> {{ cf_payment_desc }}<br />\n";
                        foreach (GlobalSet::whereHandle("gvcustomform")->get('cf_payment_gateways') as $gateway) {
                            if (isset($pageData['show_gateways']) && in_array($gateway['gateway_value'], $pageData['show_gateways'])) {
                                $fields_text .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n";
                                $fields_html .= "{{ if cf_payment_gateway == '".$gateway['gateway_value']."' }}\n";
                                $fields_text .= "Payment Method: ".$gateway['gateway_name']."\n";
                                $fields_html .= "<strong>Payment Method:</strong> ".$gateway['gateway_name']."<br /><br />\n";
                                if (isset($pageData['cf_gateway_email_overrides']) && $pageData['cf_gateway_email_overrides']) {
                                    if ($gateway['gateway_value'] == 'stripe' && isset($pageData['cf_stripe_email_instructions']) && $pageData['cf_stripe_email_instructions'] != '') {
                                        $fields_text .= $pageData['cf_stripe_email_instructions']."\n";
                                        $fields_html .= $pageData['cf_stripe_email_instructions']."\n";
                                    } elseif ($gateway['gateway_value'] == 'offline' && isset($pageData['cf_offline_email_instructions']) && $pageData['cf_offline_email_instructions'] != '') {
                                        $fields_text .= $pageData['cf_offline_email_instructions']."\n";
                                        $fields_html .= $pageData['cf_offline_email_instructions']."\n";
                                    } elseif ($gateway['gateway_value'] == 'other' && isset($pageData['cf_other_email_instructions']) && $pageData['cf_other_email_instructions'] != '') {
                                        $fields_text .= $pageData['cf_other_email_instructions']."\n";
                                        $fields_html .= $pageData['cf_other_email_instructions']."\n";
                                    } else {
                                        $fields_text .= $this->parseGlobals($gateway['gateway_email_text'])."\n";
                                        $fields_html .= $this->parseGlobals($gateway['gateway_email_html'])."\n";
                                    }
                                } else {
                                    $fields_text .= $this->parseGlobals($gateway['gateway_email_text'])."\n";
                                    $fields_html .= $this->parseGlobals($gateway['gateway_email_html'])."\n";
                                }
                                $fields_text .= "{{ /if }}\n";
                                $fields_html .= "{{ /if }}\n";
                            }
                        }
                    }
                }
            }

            if (!empty($this->adminFiles)) {
                $fields_text .= "\n==== Files\n";
                $fields_html .= "<br />\n<h3>Files</h3>\n<hr />\n<ul>\n";
                $base = GlobalSet::whereHandle("global")->get('site_url');
                foreach ($this->adminFiles as $file) {
                    $url = $base.$file;
                    $fields_text .= $url."\n";
                    $fields_html .= "<li><a href='".$url."'>".$url."</a></li>\n";
                }
                $fields_html .= "</ul>\n";
            }
            if (isset($pageData['cf_email_show_details']) && !$pageData['cf_email_show_details']) {
                $fields_text .= "{{ if var_dest == 'workflow' }}";
                $fields_html .= "{{ if var_dest == 'workflow' }}";
            }
            $fields_text .= "\n==== Submission Details\n";
            $fields_html .= "<br /><br />\n<h3>Submission Details</h3><hr />\n";
            $p_rep = '';
            $f_rep_num = -1;
            foreach ($this->fields as $field_name => $field) {
                // Add this field to our CSV
                $fields_csv .= '"{{ '.$field_name.' }}",';
                // Check if we need to close our fieldset repetition block
                if ($f_rep_num >= 0 && substr($field_name, strrpos($field_name, '_')) != '_'.$f_rep_num) {
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                    $f_rep_num = -1;
                }
                // Check if we need to close our page repetition block
                if ($p_rep != '' && substr($field_name, 0, strpos($field_name, '_')) != $p_rep) {
                    $fields_text .= "{{ /if }}\n";
                    $fields_html .= "{{ /if }}\n";
                    $p_rep = '';
                }
                // Check if we need to open our page repetition block
                if (stripos($field_name, '_penabled') !== FALSE) {
                    $fields_text .= "{{ if ".$field_name." }}\n";
                    $fields_html .= "{{ if ".$field_name." }}\n";
                    $p_rep = substr($field_name, 0, stripos($field_name, '_penabled'));
                // Check if we need to open our fieldset repetition block
                } elseif (stripos($field_name, '_fenabled_') !== FALSE) {
                    $fields_text .= "{{ if ".$field_name." }}\n";
                    $fields_html .= "{{ if ".$field_name." }}\n";
                    $f_rep_num = substr($field_name, strrpos($field_name, '_') + 1);
                // Handle this field
                } else {
                    // Check if we are adding this field to our text and html (not in the headers)
                    if (!in_array($field_name, $ignore) && !in_array($field_name, $header_ignore)) {
                        $this->parseField($fields_text, $fields_html, $field_name, $field, $pageData);
                    }
                }
            }
            // Check if we need to close our fieldset repetition block
            if ($f_rep_num > -1) {
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
            }
            // Check if we need to close our page repetition block
            if ($p_rep != '') {
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
            }
            if (isset($pageData['cf_email_show_details']) && !$pageData['cf_email_show_details']) {
                $fields_text .= "{{ /if }}\n";
                $fields_html .= "{{ /if }}\n";
            }
            // Trim the trailling comma from the CSV string
            $fields_csv = trim($fields_csv, ',');
            // Output our template files
            $this->createTemplate(site_path('/themes/site/partials/custom-form/forms/'.$pageData['form_name'].'-csv.html'), $fields_csv);
            $this->createTemplate(site_path('/themes/site/partials/custom-form/forms/'.$pageData['form_name'].'-text.html'), $fields_text);
            $this->createTemplate(site_path('/themes/site/partials/custom-form/forms/'.$pageData['form_name'].'-html.html'), $fields_html);
            $user_text = '';
            $user_html = '';
            if (isset($pageData['cf_user_message_heading']) && isset($pageData['cf_user_message_text'])) {
                $user_text = "==== ".$pageData['cf_user_message_heading']."\n".strip_tags($pageData['cf_user_message_text'])."\n";
                $user_html = "<h2>".$pageData['cf_user_message_heading']."</h2>\n<div>".nl2br($pageData['cf_user_message_text'])."</div>\n";
            }
            $this->createTemplate(site_path('/themes/site/partials/custom-form/forms/'.$pageData['form_name'].'-subheader-text.html'), $user_text);
            $this->createTemplate(site_path('/themes/site/partials/custom-form/forms/'.$pageData['form_name'].'-subheader-html.html'), $user_html);
    }

    private function parseField(&$fields_text, &$fields_html, $field_name, $field, $pageData) {
        if (isset($field['type']) && $field['type'] == 'textarea') {
            $fields_text .= $field['display'].":\n{{ ".$field_name." }}\n";
            $fields_html .= '<strong>'.$field['display'].":</strong><br />\n{{ ".$field_name." | nl2br }}<br /><br />\n";
        } elseif (isset($field['type']) && $field['type'] == 'asset') {
            $fields_text .= "{{ if ".$field_name." }}\n";
            $fields_html .= "{{ if ".$field_name." }}\n";
            $fields_text .= $field['display'].": ";
            $fields_html .= '<strong>'.$field['display'].":</strong> ";
            if ($pageData['show_file_uploads']) {
                $fields_text .= "{{ if var_dest == 'workflow' }}";
                $fields_html .= "{{ if var_dest == 'workflow' }}";
                $fields_text .= GlobalSet::whereHandle("global")->get('site_url')."{{ ".$field_name." }}";
                $fields_html .= "<a href='".GlobalSet::whereHandle("global")->get('site_url')."{{ ".$field_name." }}'>";
                $fields_text .= "{{ else }}";
                $fields_html .= "{{ /if }}";
            }
            $fields_text .= "{{ assets:".$field_name." }}{{ basename }}{{ /assets:".$field_name." }}";
            $fields_html .= "{{ assets:".$field_name." }}{{ basename }}{{ /assets:".$field_name." }}";
            if ($pageData['show_file_uploads']) {
                $fields_html .= "{{ if var_dest == 'workflow' }}";
                $fields_html .= "</a>";
                $fields_text .= "{{ /if }}";
                $fields_html .= "{{ /if }}";
            }
            $fields_text .= "\n\n{{ /if }}\n";
            $fields_html .= "<br />\n{{ /if }}\n";
//        } elseif (isset($field['type']) && $field['type'] == 'radio') {
//            $fields_text .= $field['display'].': {{ '.$field_name." }}\n";
//            $fields_html .= '<strong>'.$field['display'].':</strong> {{ '.$field_name." }}<br />\n";
        } elseif ($field_name == 'cf_mailchimp_signup') {
            $fields_text .= $field['display'].': {{ '.$field_name." | yes_no }}\n";
            $fields_html .= '<strong>'.$field['display'].':</strong> {{ '.$field_name." | yes_no }}<br />\n";
        } else {
            $fields_text .= $field['display'].': {{ '.$field_name." }}\n";
            $fields_html .= '<strong>'.$field['display'].':</strong> {{ '.$field_name." }}<br />\n";
        }
    }

    private function parseGlobals($var) {
        if (preg_match_all('/{{ (\w+:\w+) }}/i', $var, $matches) > 0) {
            foreach ($matches[1] as $match) {
                list($global, $val) = explode(':', $match);
                $var = str_replace('{{ '.$match.' }}', GlobalSet::whereHandle($global)->get($val), $var);
            }
        }
        return $var;
    }

    private function getLabel($name) {
        // Strip the underscores and titlise the name
        return title_case(str_replace(array('_', '-'), ' ', $name));
    }

    private function clean($name) {
        // Strip invalid chrs and lowercase the name
        return strtolower(str_replace(array('/', ',', '.', '\\', '(', ')', '&', ' '), '_', $name));
    }

    private function createTemplate($file, $contents) {
        $dir = dirname($file);
        if (!is_dir($dir)) {
            Log::info('Creating directory:');
            Log::info($dir);
            mkdir($dir, 0777, TRUE);
        }
        Log::info('Creating file:');
        Log::info($file);
        file_put_contents($file, $contents);
    }
}
