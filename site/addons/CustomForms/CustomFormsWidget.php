<?php

namespace Statamic\Addons\CustomForms;

use Log;

use Statamic\Extend\Widget;

class CustomFormsWidget extends Widget
{
    private $cf;

    public function __construct(CustomForms $cf)
    {
        $this->cf = $cf;
    }

    /**
     * The HTML that should be shown in the widget
     *
     * @return string
     */
    public function html()
    {
        // Get our forms
        $forms = $this->cf->getForms();
        // Sort the array by edit date
        uasort($forms, [$this, 'cmp']);
        // Build our view
        return $this->view('widget', [
            'fields' => $this->get('fields', []),
            'forms' => array_slice($forms, 0, 5)
        ]);
    }

    private function cmp($a, $b) {
        if ($a['editdate'] == $b['editdate']) {
            return 0;
        }
        return ($a['editdate'] > $b['editdate'] ? -1 : 1);
    }
}
