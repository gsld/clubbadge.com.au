<?php

namespace Statamic\Addons\CacheEx;

use Statamic\Extend\Listener;

use Log;

use Statamic\Data\Pages\Page;
use Statamic\API\Config;

class CacheExListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'cp.page.published' => 'checkCache',
    ];
    
    /**
     * The method for checking if our entry should be added to the cache excludes
     *
     * @var array
     */
    public function checkCache($page) {
        // Get our list of excludes
        $excludes = Config::get('caching.static_caching_exclude');
        // Check if our var is set
        Log::debug('Page saved... checking if we need to add it to the cache excludes');
        if ($page->get('cache_exclude') == true) {
            Log::debug('Page is to be excluded');
            // Check if this entry is already in the cache excludes and if not, then add it
            if (!in_array($page->uri(), $excludes)) {
                // This page isn't in the excludes so lets add it
                Log::debug('Adding page to Static cache');
                $excludes[] = $page->uri();
                Config::set('caching.static_caching_exclude', $excludes);
                Config::save();
            } else {
                Log::debug('Page is already excluded');
            }
        } else {
            Log::debug('Page is not to be excluded');
            // Check if this entry is already in the cache excludes and if so, then remove it
            if (in_array($page->uri(), $excludes)) {
                $result = array_diff($excludes, array($page->uri()));
                Config::set('caching.static_caching_exclude', $result);
                Config::save();
                Log::debug('Page has been removed from the excludes list');
            } else {
                Log::debug('Page is not excluded');
            }
        }
    }
}
