<?php

namespace Statamic\Addons\FormsFilter;

use Log;

use Statamic\API\GlobalSet;
use Statamic\Contracts\Forms\Submission;
use Statamic\Extend\Listener;

class FormsFilterListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = ['Form.submission.creating' => 'handle'];

    /**
     * The method for checking if our form submission passes the filter or not
     *
     * @var array
     */
    public function handle(Submission $submission)
    {
        $found = false;
        $match = "";
        $strings = json_decode(file_get_contents("/var/www/formsfilter/terms.json"));
        // Convert the form details to a JSON string so we can do a simple "stripos" for each bad string
        $form = json_encode($submission->data());
        Log::debug($form);
        // Check for strings to warn about in the form
        $errors = [];
        foreach ($strings->warn as $str) {
            Log::debug('Testing warning: '.$str);
            if (mb_stripos($form, $str) !== false) {
                Log::debug('FormsFilter - Warning Word found - '.$str);
                $found = true;
                $errors[] = "Error: You put this word '".$str."' in the submission. Please try again without using '".$str."'.<br>";
                break;
            }
        }
        if ($found) {
            // Return the errors for this
            return [
                'submission' => $submission,
                'errors' => $errors,
            ];
        }
        $found = false;

        // Check for the whole word matches in the form
        foreach ($strings->whole as $str) {
            Log::debug('Testing whole word: '.$str);
            if (preg_match("/\b".$str."\b/i", $form) == 1) {
                Log::debug('FormsFilter - Bad Word found - '.$str);
                $found = true;
                $match = $str;
                break;
            }
        }
        // Check for multi word matches in the form
        if (!$found) {
            foreach ($strings->multi as $arr) {
                Log::debug('Testing multi-word: '.join(' + ', $arr));
                $matches = 0;
                // Test each of the words
                foreach ($arr as $str) {
                    if (preg_match("/\b".$str."\b/i", $form) == 1) {
                        // Increment our count of matches
                        $matches++;
                    }
                }
                // Check if all words matched
                if ($matches == count($arr)) {
                    $found = true;
                    $match = join(' + ', $arr);
                    Log::debug('FormsFilter - Bad Multi-Word found - '.$match);
                    break;
                }
            }
        }
        // Check for the partial matches in the form
        if (!$found) {
            foreach ($strings->partials as $str) {
                Log::debug('Testing partial word: '.$str);
                if (mb_stripos($form, $str) !== false) {
                    Log::debug('FormsFilter - Bad Partial Word found - '.$str);
                    $found = true;
                    $match = $str;
                    break;
                }
            }
        }

        if ($found) {
            // Get our directory, check it exists and if not, create it
            $dir = '/var/www/formsfilter/storage/';
            /* OLD
            $dir = '/var/www/formsfilter/storage/'.GlobalSet::whereHandle("global")->get('site_nice_url');
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            */
            // Save the file
            //file_put_contents($dir.'/'.time().'.json', $match."\n".$form);
            file_put_contents($dir.'/'.$match.'-'.time().'.json', $match."\n".$form);
            sleep(2);
            // Bail out
            Log::debug('Bailing');
            throw new \Statamic\Exceptions\SilentFormFailureException('Invalid words found in form submission');
        }

        return [
            'submission' => $submission,
        ];
    }
}
