<?php

namespace Statamic\Addons\EmailGlobals;

use Statamic\Extend\Listener;
use Statamic\API\GlobalSet;
use Log;

class EmailGlobalsListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'cp.page.published' => 'parseEmailPartials',
        'cp.globals.published' => 'parseEmailPartials'
    ];
    
    /**
     * The method for parsing our Email partials for globals
     *
     */
    public function parseEmailPartials() {
        // Parse our extra partials for globals
        $files = glob(site_path('/themes/site/templates/email/').'*-template.html');
        $files = array_merge($files, glob(site_path('/themes/site/templates/email/*/').'*-template.html'));
        $files = array_merge($files, glob(site_path('/themes/site/partials/email/').'*-template.html'));
        $files = array_merge($files, glob(site_path('/themes/site/partials/email/*/').'*-template.html'));
        foreach ($files as $file) {
            $str = file_get_contents($file);
            $start = strripos($file, '/') + 1;
            $outFile = substr($file, 0, stripos($file, '/email/') + 7).'output/'.substr($file, $start, strripos($file, '-template.html') - $start).'.html';
            file_put_contents($outFile, $this->parseGlobals($str));
        }
    }

    private function parseGlobals($var) {
//        Log::info($var);
        if (preg_match_all('/{{?\s*(\w+:\w+)\s*}?}/i', $var, $matches) > 0) {
//            Log::info(print_r($matches, TRUE));
            foreach ($matches[1] as $num => $match) {
                list($global, $val) = explode(':', $match);
                $globalSet = GlobalSet::whereHandle($global);
                if (isset($globalSet)) {
                    $var = str_replace($matches[0][$num], $globalSet->get($val), $var);
                }
            }
        }
        return $var;
    }
}
