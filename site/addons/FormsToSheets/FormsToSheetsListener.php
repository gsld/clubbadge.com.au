<?php

namespace Statamic\Addons\FormsToSheets;

use Statamic\Extend\Listener;
use Statamic\Contracts\Forms\Submission;

use Log;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;

class FormsToSheetsListener extends Listener
{
    private $common;

    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = ['Form.submission.created' => 'handleSubmission'];

    protected function init()
    {
        $this->common = new FormsToSheets();
    }

    /**
     * Pushes our submission to a new row(s) in a Google Sheet
     *
     * @param The form submission
     */
    public function handleSubmission(Submission $submission)
    {
        // Get the ID of the spreadsheet to update, if it exists
        $fields = $submission->formset()->get('fields');
        $sheetId = isset($fields['google_sheet_id']) && isset($fields['google_sheet_id']['value']) ? $fields['google_sheet_id']['value'] : null;
        if (is_null($sheetId)) {
            // There is no sheet ID to update so bail
            return;
        }

        // Build our row(s)
        $values = [
            array_values($submission->data())
        ];

        $response = $this->common->add($sheetId, $values);
    }
}
