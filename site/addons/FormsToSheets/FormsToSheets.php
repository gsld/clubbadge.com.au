<?php

namespace Statamic\Addons\FormsToSheets;

use Log;

use Statamic\Extend\Extensible;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Sheets_ValueRange;

class FormsToSheets
{
    use Extensible;

    /**
     * Returns an authorized API client.
     *
     * @return Google_Client the authorized client object
     */
    function getClient()
    {
        // TODO: This needs to be updated to support proper the Web App OAuth process (this is based on the command line process)
        
        
        $client = new Google_Client();
        $client->setApplicationName('FormsToSheets Statamic Addon');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $client->setAuthConfig($this->getDirectory().'/credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = $this->getDirectory().'/token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Log an error that we aren't configured correctly and bail
                Log::error('FormsToSheet: Invalid Google Sheets authorisation! Please reauthorise this addon in the Control Panel');
                return;
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    /**
     * Create a new spreadsheet and return the ID
     *
     * @param Form Name
     * @param Field list
     * @return SheetID
     */
    public function create($form_name)
    {
        // Check our form name is valid
        if (is_null($form_name) || $form_name == '') {
        	return;
        }

        // Get the API client and construct the service object.
        $client = $this->getClient();
        if (is_null($client)) {
            // We don't have authorisation or there was some error so bail
            return;
        }
        $service = new Google_Service_Sheets($client);
		$spreadsheet = new Google_Service_Sheets_Spreadsheet([
		    'properties' => [
		        'title' => $form_name
		    ]
		]);
		$spreadsheet = $service->spreadsheets->create($spreadsheet, []);
		return $spreadsheet->spreadsheetId;
    }

    /**
     * Add/remove fields from our existing spreadsheet
     *
     * @param SheetID
     * @param Field list
     */
    public function edit($sheet_id, $fields)
    {
    	// Hard stuff
    }

    /**
     * Pushes new row(s) in a Google Sheet
     *
     * @param SheetID
     * @param The rows to add
     */
    public function add($sheetId, $rows)
    {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        if (is_null($client)) {
            // We don't have authorisation or there was some error so bail
            return;
        }
        $service = new Google_Service_Sheets($client);
        
        // The A1 notation of a range to search for a logical table of data.
        // Values will be appended after the last row of the table.
        $range = 'Sheet1!A1';

        // Get our body object for the row(s) to be inserted
        $body = new Google_Service_Sheets_ValueRange([
            'majorDimension' => 'ROWS',
            'values' => $rows
        ]);
        
        // Ensure we are inserting data in case someone has screwed up our table
        $params = [
            'valueInputOption' => 'USER_ENTERED',
            'insertDataOption' => 'INSERT_ROWS'
        ];
        
        // Do our thiang    TODO: Handle errors
        return $service->spreadsheets_values->append($sheetId, $range, $body, $params);
    }
}
