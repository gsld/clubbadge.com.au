<?php

namespace Statamic\Addons\FormsToSheets;

use Statamic\Extend\API;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_Spreadsheet;

class FormsToSheetsAPI extends API
{
	private $common;

	protected function init()
	{
		$this->common = new FormsToSheets();
	}

    /**
     * Create a new Sheets spreadsheet
     *
     * Accessed by $this->api('FormsToSheets')->create() from other addons
     *
     * @param Form Name
     * @param Field list
     * @return SheetID
     */
    public function create($form_name, $fields)
    {
    	// Create our spreadsheet
        $sheet_id = $this->common->create($form_name);
        // Build and add a header row
        foreach ($fields as $field) {
        	$out[] = $field['display'];
        }
        $this->common->add($sheet_id, [ $out ]);
        // Return our sheet ID
        return $sheet_id;
    }

    /**
     * Add/remove rows in an existing Sheets spreadsheet
     *
     * Accessed by $this->api('FormsToSheets')->edit() from other addons
     *
     * @param SheetID
     * @param Field list
     */
    public function edit($sheet_id, $fields)
    {
        $this->common->edit($sheet_id, $fields);
    }
}
