<?php

namespace Statamic\Addons\AntiCache;

use Statamic\Extend\Listener;

use Log;
use Statamic\Data\Pages\Page;
use Statamic\API\Cache;

class AntiCacheListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'PagesMoved' => 'clearCache',
    ];

    /**
     * The method for nuking our cache
     *
     * @var array
     */
    public function clearCache(PagesMoved $event)
    {
        Cache::clear();
    }
}
