<?php

namespace Statamic\Addons\AssetResize;

use Statamic\Extend\Listener;

use Statamic\API\File;
use Statamic\API\Cache;
use Statamic\Events\Data\AssetUploaded;

class AssetResizeListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        AssetUploaded::class => 'assetResize'
    ];

    /**
     * The method for resizing our asset and deleting the original
     *
     * @var array
     */
    public function assetResize($assetIn)
    {
        $asset = $assetIn->asset;
        $width = $this->getConfig('max_width', 1600);

        // Replace any brackets in the asset filename with dashes because they break stuff apparently
        $new = str_replace(['(', ')'], ['-', '-'], $asset->filename());
        if ($new != $asset->filename()) {
            $asset->rename($new);
        }

        if ($asset->isImage() && $asset->width() > $width) {
            // Resize our image to max 1600px wide and replace the original asset file
            $filename = $asset->manipulate(['w' => $width, 'fit' => 'contain']);
            $asset->replace(File::get($filename));
            // Be hackish and hose our dimensions cache so they get regenerated correctly
            $cacheKey = 'assets.dimensions.' . $asset->containerId() . '.' . $asset->path();
            Cache::forget($cacheKey);
        }
    }
}
