<?php
return [
    'php_path' => 'Path to PHP',
    'php_path_instruct' => "The path to PHP on your system, used to create asset presets in the background.",
    'shell_path' => 'Your Shell Path',
    'shell_path_instruct' => "The path to the shell on your system system, used to create asset presets in the background."
  ];