<?php

namespace Statamic\Addons\CMSTraining;

use Statamic\API\Nav;
use Statamic\Extend\Listener;

class CMSTrainingListener extends Listener
{
    public $events = [
        'cp.nav.created' => 'addNavItems'
    ];

    public function addNavItems($nav)
    {
        // Create the first level navigation item
        // Note: by using route('store'), it assumes you've set up a route named 'store'.
        $CMStraining = Nav::item('CMS Training')->url('https://www.gsld.com.au/cms/training/v2/1/')->icon('youtube');
        // Finally, add our first level navigation item
        // to the navigation under the 'tools' section.
        $nav->addTo('tools', $CMStraining);
    }
}
