<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Tasks;
use Illuminate\Console\Scheduling\Schedule;

class PayamicTasks extends Tasks
{
    /**
     * Define the task schedule
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    public function schedule(Schedule $schedule)
    {
        if ($this->getConfig('subscriptions')) {
            $schedule->command('payamic:checksubs')->daily();
        }
    }
}
