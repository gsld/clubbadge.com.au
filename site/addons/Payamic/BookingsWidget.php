<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Widget;

class BookingsWidget extends Widget
{
    private $common;

    public function __construct(Common $common)
    {
        $this->common = $common;
    }

    /**
     * The HTML that should be shown in the widget
     *
     * @return string
     */
    public function html()
    {
        // Get our cottage names
        $cottages = collect(\Statamic\API\Entry::whereCollection('cottages')->toArrayWith(['id', 'title']))->keyBy('id')->all();
        // Get our pending bookings
        $bookings = \Statamic\API\Entry::whereCollection('bookings')
            ->conditions(['booking_status:is' => 'pending'])
            ->supplement('cottage_name', function($item) use ($cottages) {
                return $cottages[$item->get('cottage')]['title'];
            })->toArray();
        // Build our view
        return $this->view('bookings-widget', [
            'bookings' => $bookings
        ]);
    }
}
