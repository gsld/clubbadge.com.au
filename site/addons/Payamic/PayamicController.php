<?php

namespace Statamic\Addons\Payamic;

use Statamic\API\User;
use Statamic\API\Helper;
use Statamic\API\Fieldset;

use Statamic\Extend\Controller;
use Statamic\CP\Publish\ProcessesFields;

use Illuminate\Http\Request;

use Session;
use Log;

class PayamicController extends Controller
{
    use ProcessesFields;

    private $common;
    private $page_limit = 30;

    public function __construct(Common $common)
    {
        $this->common = $common;
    }

    /**
     * Show our list of transactions
     *
     * @return view
     */
    public function index(Request $request)
    {
        // Get our transactions
        $transactions = collect($this->common->getTransactions());
        $pages = $this->paginate($transactions, $request->input('page', 1));
        $statuses = $this->common->getOrderStatuses();
        // Prettify our amount field and get the correct currency symbol
        $transactions = $transactions->transform(function($item, $key) {
            $currency = $this->common->getCurSymbol($item['currency'] ?? 'aud');
            $item['amount'] = $currency.number_format($item['amount'], 2, '.', '');
            return $item;
        })
            ->reverse()
            ->forPage($request->input('page', 1), $this->page_limit)
            ->all();
        // Build our dataset
        $data = [
            'title' => 'Payamic | Transactions',
            'transactions' => $transactions,
            'statuses' => $statuses,
            'pagination' => $pages
        ];
        return $request->ajax() ? response()->json($data) : $this->view('index', $data);
    }

    /**
     * Show the details of the selected transaction
     *
     * @return view
     */
    public function detail(Request $request)
    {
        // Get our ID
        $id = $request->input('id');
        // Get our transactions
        $transactions = $this->common->getTransactions();
        // Build our view
        return $this->view('detail', [
            'title' => 'Payamic | Transaction',
            'id' => $id,
            'transaction' => isset($transactions[$id]) ? $transactions[$id] : null
        ]);
    }

    /**
     * Return our list of transactions as either a CSV or JSON file
     *
     * @return response
     */
    public function export(Request $request)
    {
        // Get our transactions
        $transactions = $this->common->getTransactions();
        $tmpfname = tempnam(sys_get_temp_dir(), 'sta');
        // Build the output - TODO
        $type = $request->input('type');
        if ($type == 'json') {
            file_put_contents($tmpfname, json_encode($transactions));
        } else {
            // Build our CSV file
            $type = 'csv';
            $fp = fopen($tmpfname, 'w');
            if (isset($transactions[0])) {
                fputcsv($fp, array_keys($transactions[0]));
            }
            foreach ($transactions as $fields) {
                fputcsv($fp, $fields);
            }
            fclose($fp);
        }
        // Serve and delete our temp file
        return response()->download($tmpfname, 'transactions.'.$type)->deleteFileAfterSend(true);
    }

    /**
     * Show our list of orders
     *
     * @return view
     */
    public function orders(Request $request)
    {
        // Get our orders and statuses
        $orders = $this->common->getOrders()->filter(function($value) use ($request) {
            $status = strtolower($request->input('status', 'all statuses'));
            return $status == 'all statuses' || $status == strtolower($value['order_status']);
        });
        $pages = $this->paginate($orders, $request->input('page', 1));
        $statuses = $this->common->getOrderStatuses();
        // Prettify our amount field and get the collection ready
        $orders = $orders->transform(function($item, $key) {
            $currency = $this->common->getCurSymbol($item['currency'] ?? 'aud');
            $item['amount'] = $currency.number_format($item['amount'], 2, '.', '');
            return $item;
        })
            ->reverse()
            ->forPage($request->input('page', 1), $this->page_limit)
            ->all();
        // Build our dataset
        $data = [
            'title' => 'Payamic | Orders',
            'orders' => $orders,
            'statuses' => $statuses,
            'pagination' => $pages
        ];
        return $request->ajax() ? response()->json($data) : $this->view('orders', $data);
    }

    /**
     * Show the details of the selected order
     *
     * @return view
     */
    public function order(Request $request)
    {
        // Get our ID
        $id = $request->input('id');
        // Get our order and statuses
        $order = $this->common->getOrders()->get($id);
        $statuses = $this->common->getOrderStatuses();
        // Build our view
        return $this->view('order', [
            'title' => 'Payamic | Order',
            'id' => $id,
            'order' => $order,
            'statuses' => $statuses,
            'headers' => ['name', 'amount', 'desc', 'summary', 'order_status', 'delivery']
        ]);
    }

    /**
     * Update the status of the order
     *
     * @return json
     */
    public function updateOrder(Request $request)
    {
        // Get our ID and new status
        $id = $request->input('id');
        $status = $request->input('status');
        // Update our order
        $result = $this->common->updateOrder($id, $status);
        // Build our view
        return response()->json(['result' => $result, 'error' => $result ? '' : $this->common->error]);
    }

    public function checkOrders(Request $request) {
        // Check if we have any new orders since refresh_time and return the count
        $orders = $this->common->getOrders()->filter(function($item) use ($request) {
            return isset($item['order_status']) && intval($item['timestamp']) * 1000 > $request->input('last_update');
        });
        return response()->json(['count' => count($orders), 'orders' => $orders]);
    }

    public function vouchers() {
        $vouchers = $this->common->getVouchers()->all();
        foreach ($vouchers as $key => &$voucher) {
            $voucher['is_active'] = ($voucher['expiry'] > time() && ($voucher['max'] <= 0 || $voucher['max'] > $voucher['counter']));
        }
        return $this->view('vouchers', ['title' => 'Payamic', 'vouchers' => $vouchers]);
    }

    public function createVoucher()
    {
        return $this->view('voucher', [
            'id' => null,
            'data' => $this->prepareData([], 'voucher'),
            'submitUrl' => route('payamic.store_voucher')
        ]);
    }

    public function editVoucher(Request $request)
    {
        $id = $request->id;

        $data = $this->prepareData($this->common->getVoucher($id), 'voucher');

        return $this->view('voucher', [
            'id' => $id,
            'data' => $data,
            'submitUrl' => route('payamic.update_voucher')
        ]);
    }

    public function updateVoucher(Request $request)
    {
        $id = $request->uuid;

        $this->saveVoucher($id, $request);

        return $this->voucherSuccessResponse($id);
    }

    public function storeVoucher(Request $request)
    {
        $id = Helper::makeUuid();

        $this->saveVoucher($id, $request);

        return $this->voucherSuccessResponse($id);
    }

    /**
     * Our custom (variable price) front-end function - /!/Payamic/custom
     *
     * @return mixed
     */
    public function getCustom()
    {
        // Set our referrer and then redirect to the payment page with a custom payment
        $this->common->setReferrer();
        return $this->common->getCustomPaymentURL(TRUE);
    }

    /**
     * Our payment URL front-end function (for Ajax submissions requiring payment redirect) - /!/Payamic/paymenturl
     *
     * @return mixed
     */
    public function getPaymentUrl(Request $request)
    {
        // Return the link to the payment page with a custom payment
        $amount = $request->input('amount');
        $desc = $request->input('desc');
        $item_id = $request->input('item_id');

        return response()->json(array('url' => $this->common->getPaymentURL($amount, $desc, $item_id, FALSE)));
    }

    /**
     * Our front-end function for returning the voucher validity and details - /!/Payamic/voucher
     *
     * @return mixed
     */
    public function getVoucher(Request $request)
    {
        // Check the supplied voucher is valid and return its details
        return response()->json($this->common->checkVoucher($request->input('voucher')));
    }

    /**
     * Our front-end function for obtaining shipping option details - /!/Payamic/shipping
     *
     * @return mixed
     */
    public function getShipping(Request $request)
    {
        // Return the price of each shipping option
        return response()->json($this->common->getShippingOptions($request->input('obj')));
    }

    /**
     * Our checkout front-end function - /!/Payamic/checkout
     *
     * @return mixed
     */
    public function postCheckout(Request $request)
    {
        // Load our vars
        if (!$this->common->loadVars()) {
            // There was an error loading our vars so crash
            return $this->handleError('ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
        }

        // Check we have the necessary fields (gateway and payment)
        $payment = $request->input('payment');
        $gateway = $request->input('payment_gateway');
        $quantity = $request->input('quantity', 1);
        $shipping = $request->input('shipping_option');
        $name = $this->common->getName($request->input());
        $email = $request->input('email');
        $summary = $request->input('payment_summary');
        $form = $request->input('email_template', $this->common->email_template_payment_default);

        if (is_null($payment) || $payment == '' || is_null($gateway) || $gateway == '') {
            // We are missing data so error
            return $this->handleError('ERROR: Payment details missing');
        }
        // Check we have a valid payment
        $payment = $this->common->checkPayment($payment);
        if ($payment === FALSE) {
            // Handle the error
            return $this->handleError('ERROR: Payment details missing or invalid');
        }

        if ($payment['amount'] == 'custom') {
            $payment['item_id'] = 'custom';
            $payment['amount'] = $request->input('amount');
            if (!is_numeric($payment['amount']) || floatval($payment['amount']) <= 0) {
                // This isn't a valid amount so error
                return $this->handleError('ERROR: Invalid payment amount entered');
            }
            $payment['amount'] = number_format($payment['amount'], 2, '.', '');
            $payment['desc'] = $request->input('desc');
            if ($payment['desc'] == '') {
                // We need a description so error
                return $this->handleError('ERROR: Please enter a description');
            }
        } elseif ($payment['item_id'] != 'cart') {
            // Check if this payment is for a single entry based item
            $entry = $this->common->getItemEntry($payment['item_id']);
            if ($entry) {
                // Ensure our quantity is available
                if (is_numeric($entry->get('quantity')) && $entry->get('quantity') > -1 && $entry->get('quantity') < $quantity) {
                    // Eek, invalid qty
                    return $this->handleError('ERROR: Quantity unavailable');
                }
                // Override the input price with the price from the entry
                $payment['amount'] = $entry->get('price');
            }
        }

        // Adjust our amount based on the quantity
        $price = $payment['amount'];
        $payment['amount'] = $payment['amount'] * $quantity;

        // Handle a voucher if supplied and valid
        if ($request->input('voucher') != '') {
            $voucher = $this->common->checkVoucher($request->input('voucher'));
            if ($voucher['result'] == 'success') {
                if ($voucher['type'] == 'amount') {
                    $payment['amount'] = $payment['amount'] - floatval($voucher['amount']);
                } else {
                    $payment['amount'] = $payment['amount'] - ($payment['amount'] * floatval($voucher['amount']) / 100);
                }
            } else {
                return $this->handleError($voucher['error']);
            }
        }

        // Add our delivery fee if necessary
        $shipping_fee = $this->common->getShipping($shipping, $payment['item_id'] == 'cart' ? 'cart' : $payment['item_id'].'|'.$quantity);
        if ($shipping_fee < 0) {
            // The chosen delivery option is invalid
            return $this->handleError($this->common->error);
        }
        $payment['amount'] += $shipping_fee;

        // Check our payment is above the gateway minimum
        if ($gateway == 'stripe' && $payment['amount'] < $this->getConfig('stripe_minimum')) {
            // The amount of this payment is below our minimum
            return $this->handleError('ERROR: Payment amount is below the minimum for this gateway');
        }

        // Add any gateway levy
        if ($this->getConfig($gateway.'_levy') != '' && floatval($this->getConfig($gateway.'_levy')) > 0) {
            $payment['amount'] = $payment['amount'] + ($payment['amount'] * floatval($this->getConfig($gateway.'_levy')) / 100);
        }

        // Trim our payment to cents
        $payment['amount'] = round($payment['amount'], 2);

        // Find our user
        if (stripos($payment['desc'], 'Membership Renewal') !== FALSE) {
            $user = User::find($request->input('user_id'));
            if (is_null($user)) {
                $user = User::getCurrent();
            }
        } else {
            $user = User::getCurrent();
        }

        // Record our fields so far
        $this->common->setPaymentDetail('user', ($user ? $user->getAuthIdentifier() : 'guest'));
        $this->common->setPaymentDetail('name', $name);
        $this->common->setPaymentDetail('email', $email);
        $this->common->setPaymentDetail('gateway', $gateway);
        $this->common->setPaymentDetail('amount', $payment['amount']);
        $this->common->setPaymentDetail('desc', $payment['desc']);
        $this->common->setPaymentDetail('summary', $this->common->parseTextBlob($summary));
        $this->common->setPaymentDetail('item_id', $payment['item_id']);
        $this->common->setPaymentDetail('quantity', $quantity);
        $this->common->setPaymentDetail('price', $price);
        $this->common->setPaymentDetail('shipping', ['type' => $shipping, 'price' => $shipping_fee]);
        $this->common->setPaymentDetail('gw_levy', $this->getConfig($gateway.'_levy', 0));
        $this->common->setPaymentDetail('voucher', isset($voucher) ? $voucher['id'] : '(none)');
        if ($request->input('order_status') != '') {
            $this->common->setPaymentDetail('order_status', $request->input('order_status'));
        }
        $this->common->setCustomFields();

        // Process the payment
        $result = FALSE;
        if ($gateway == 'offline') {
            // Just store the details and redirect to the success page
            $this->common->setPaymentDetail('timestamp', time());
            $this->common->setPaymentDetail('trans_id', 'off-'.time());
            $this->common->setTransCount();
            $result = TRUE;
        } elseif ($gateway == 'stripe') {
            // Check that we have our Stripe token
            $token = $request->input('stripe_token');
            if (is_null($token) || $token == '') {
                // We are missing data so error
                return $this->handleError('ERROR: Payment details missing');
            }
            // Process our payment
            $result = $this->common->processStripe($payment, $token);
        } else {
            // wtf is this?
            return $this->handleError('ERROR: Invalid payment details');
        }

        Session::flash('payamic', json_encode($this->common->payment_details));
        if ($result) {
            // Record this transaction in the log
            $this->common->recordTrans();
            // Send some notification emails
            $this->common->sendEmails($form);
            // Handle any item quantity adjustments
            $this->common->updateItemQuantities();
            // If this is a cart payment then clear it
            if ($payment['item_id'] == 'cart') {
                $request->session()->forget('cart');
            }
            // Check if this is a subscription payment and if so, update the user profile
            if (stripos($payment['desc'], 'Membership Renewal') !== FALSE) {
                // Update the user profile
                $this->common->updateUser($payment['item_id'], $user, $gateway);
            }
            // Check if this is a booking payment and if so, create the booking
            if (stripos($payment['desc'], 'Booking') !== FALSE) {
                $this->common->createBooking($payment);
            }
            // If there was a voucher used, update its counter
            if (isset($voucher) && $voucher['result'] == 'success') {
                $this->common->incVoucher($voucher['id']);
            }
            // Redirect to our success page
            if ($request->ajax()) {
                return response()->json(array_merge($this->common->payment_details, ['result' => 'success']));
            } else {
                return redirect($request->input('payment_success', '/'.$this->common->page_success));
            }
        } else {
            // Redirect to our error page
            if ($request->ajax()) {
                return response()->json(array_merge($this->common->payment_details, ['result' => 'error', 'errors' => Session::get('payment_errors')]));
            } else {
                return redirect($request->server('HTTP_REFERER'));
            }
        }
    }

    /**
     * Our add-to-cart front-end function - /!/Payamic/add-to-cart
     *
     * @return mixed
     */
    public function postAddToCart()
    {
        $entry = null;
        // Grab our item's details from the request
        $raw_item = request()->input();
        // Filter out known excess fields
        unset($raw_item['_token']);
        // Replace any spaces in array keys lost when transmitting each variable
        $item = [];
        foreach ($raw_item as $key => $value) {
            $item[str_replace('_', ' ', $key)] = $value;
        }
        // Check if the item is an entry
        if (isset($item['itemid'])) {
            $entry = $this->common->getItemEntry($item['itemid']);
        }
        // If we have an entry, grab the name and price from that
        if (!is_null($entry)) {
            $item['name'] = $entry->get('title');
            $item['price'] = $entry->get('price');
            // Check if we have options and if they have prices and calculate our total
            if ($entry->get('options')) {
                // Loop over our options to check them
                foreach ($entry->get('options') as $option) {
                    // Check if this option has been specified
                    if (isset($item[$option['option_name']])) {
                        // Loop through this option's choices to find the selected one
                        foreach ($option['option_items'] as $val) {
                            if ($val['option_value'] == $item[$option['option_name']] && isset($val['option_price'])) {
                                // We have a price so adjust our total
                                $item['price'] += floatval($val['option_price']);
                            }
                        }
                    }
                }
            }
        }
        // Check we have an item name field
        if (!isset($item['name'])) {
            return response()->json(['result' => 'error', 'error' => 'Invalid item - missing name']);
        }
        // Check we have a price field
        if (!isset($item['price'])) {
            return response()->json(['result' => 'error', 'error' => 'Invalid item - missing price']);
        }
        // Filter out known excess fields
        unset($item['_token']);
        // Ensure we have a quantity field
        if (!isset($item['quantity'])) {
            $item['quantity'] = 1;
        }
        // Ensure our quantity is available
        if ($entry && is_numeric($entry->get('quantity')) && $entry->get('quantity') > -1 && $entry->get('quantity') < $item['quantity']) {
            // Eek, invalid qty
            return response()->json(['result' => 'error', 'error' => 'Quantity unavailable']);
        }
        // Grab our cart
        $cart = collect(request()->session()->get('cart'));
        // If it exists, check for an existing matching item
        if (!$cart->isEmpty()) {
            // Try to find a matching item
            $id = $cart->search(function($cur, $key) use (&$item) {
                // Diff our items
                $diff = array_diff_assoc($cur, $item);
                // Store and remove the quantity field if it exists
                if (isset($diff['quantity'])) {
                    unset($diff['quantity']);
                }
                // Check if we have any other differences
                if (empty($diff)) {
                    // This is a match so update the quantity and return this item
                    $item['quantity'] += $cur['quantity'];
                    return TRUE;
                }
                return FALSE;
            });
            if ($id !== FALSE) {
                // Ensure our updated quantity is available
                if ($entry && is_numeric($entry->get('quantity')) && $entry->get('quantity') > -1 && $entry->get('quantity') < $item['quantity']) {
                    // Eek, invalid qty
                    return response()->json(['result' => 'error', 'error' => 'Quantity unavailable']);
                }
                // We have a match so replace that item with the updated quantity
                $cart = $cart->merge([$id => $item]);
            } else {
                // Generate a new key and add this item to the cart
                $id = bin2hex(random_bytes(5));
                $cart = $cart->put($id, $item);
            }
        } else {
            // Generate a random key for this item and create a new cart in our session
            $id = bin2hex(random_bytes(5));
            $cart = collect([$id => $item]);
        }
        // Update our cart in the session
        request()->session()->put('cart', $cart->all());
        $num_items = $cart->reduce(function ($carry, $item) {
            return $carry + $item['quantity'];
        });
        // Return success
        return response()->json(['result' => 'success', 'cid' => $id, 'total' => $this->getCartTotal($cart), 'num_items' => $num_items]);
    }

    /**
     * Our cart updating front-end function - /!/Payamic/update-cart-item
     *
     * @return mixed
     */
    public function postUpdateCartItem()
    {
        $id = request()->input('id');
        $qty = request()->input('quantity');
        // Check we have an id
        if (is_null($id) || $id == '') {
            return response()->json(['result' => 'error', 'error' => 'Invalid item']);
        }
        // Check we have a valid quantity (is numeric)
        if (!is_numeric($qty) || $qty < 0) {
            return response()->json(['result' => 'error', 'error' => 'Invalid quantity']);
        }
        // If our quantity is 0 then pass to our delete function
        if ($qty == 0) {
            return $this->postDeleteCartItem();
        }
        // Get our cart
        $cart = collect(request()->session()->get('cart'));
        // Grab the item
        $item = $cart->pull($id);
        if (is_null($item)) {
            return response()->json(['result' => 'error', 'error' => 'Invalid item']);
        }
        // Check if the item is an entry
        if (isset($item['itemid'])) {
            $entry = $this->common->getItemEntry($item['itemid']);
            // Ensure our quantity is available
            if ($entry && is_numeric($entry->get('quantity')) && $entry->get('quantity') > -1 && $entry->get('quantity') < $qty) {
                // Eek, invalid qty
                return response()->json(['result' => 'error', 'error' => 'Quantity unavailable']);
            }
        }
        // Update our item's quantity, and replace it
        $item['quantity'] = $qty;
        $subtotal = $item['price'] * $qty;
        $cart->put($id, $item);
        // Update our cart
        request()->session()->put('cart', $cart->all());
        $num_items = $cart->reduce(function ($carry, $item) {
            return $carry + $item['quantity'];
        });
        // Return success
        return response()->json(['result' => 'success', 'quantity' => $qty, 'subtotal' => $subtotal, 'total' => $this->getCartTotal($cart), 'num_items' => ($num_items ?? 0)]);
    }

    /**
     * Our cart item deleting front-end function - /!/Payamic/delete-cart-item
     *
     * @return mixed
     */
    public function postDeleteCartItem()
    {
        $id = request()->input('id');
        // Get our cart
        $cart = collect(request()->session()->get('cart'));
        // Try to remove the item
        if (is_null($id) || $id == '' || empty($cart) || is_null($cart->pull($id))) {
            return response()->json(['result' => 'error', 'error' => 'Invalid item']);
        }
        // Update our cart
        request()->session()->put('cart', $cart->all());
        $num_items = $cart->reduce(function ($carry, $item) {
            return $carry + $item['quantity'];
        });
        // Return success
        return response()->json(['result' => 'success', 'total' => $this->getCartTotal($cart), 'num_items' => ($num_items ?? 0)]);
    }

    /**
     * Our cart clearing front-end function - /!/Payamic/empty-cart
     *
     * @return mixed
     */
    public function getEmptyCart()
    {
        request()->session()->forget('cart');
        return redirect(request()->server('HTTP_REFERER'));
    }

    private function getCartTotal($cart) {
        return $cart->reduce(function($carry, $item) {
            return $carry + ($item['quantity'] * $item['price']);
        });
    }

    private function paginate($data, $page) {
        $count = ceil(count($data) / $this->page_limit);
        return [
            'current' => intval($page),
            'total' => $count
        ];
    }

    private function handleError($error) {
        Log::error($error);
        if (request()->ajax()) {
            return $error;
        } else {
            Session::flash('payment_errors', $error);
            Session::flash('old', request()->input());
            return redirect(request()->server('HTTP_REFERER'));
        }
    }

    /**
     * Prepare the data for the view.
     *
     * Vue needs to have at least null values available from the start in order
     * to properly set up the reactivity. The data in the contentstore is not
     * guaranteed to have every single field. This method will add the
     * appropriate null values based on the provided fieldset.
     *
     * @return array
     */
    private function prepareData($data, $fieldset)
    {
        return $this->preProcessWithBlankFields(Fieldset::get($fieldset), $data);
    }

    private function saveVoucher($id, $request)
    {
        // Vue submits data slightly differently to how it should be saved.
        // Each field's fieldtype will process the data appropriately.
        $data = $this->processFields(Fieldset::get($request->fieldset), $request->fields);

        // Ensure we have a counter field
        if (!isset($data['counter'])) {
            $data['counter'] = 0;
        }

        $this->common->saveVoucher($id, $data);
    }

    private function voucherSuccessResponse($id)
    {
        $message = 'Entry saved';

        if (! request()->continue || request()->new) {
            $this->success($message);
        }

        return [
            'success'  => true,
            'redirect' => route('payamic.edit_voucher') . '?id=' . $id,
            'message'  => $message
        ];
    }
}
