<?php

namespace Statamic\Addons\Payamic\Commands;

use DateTime;

use Statamic\Extend\Command;
use Statamic\Addons\Payamic\Common;

class CheckBookingsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payamic:checkbookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Script to check if pending bookings are coming upon expiry, and if so send notifications or disable the booking';

    /**
     * Our addon's common codebase
     *
     * @var string
     */
    private $common;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        $this->common = new Common;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get today's date
        $date = new DateTime();
        // Check subscriptions
        $this->common->checkBookings($date);
    }
}
