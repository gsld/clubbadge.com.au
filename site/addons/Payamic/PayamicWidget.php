<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Widget;

use Statamic\API\Folder;

class PayamicWidget extends Widget
{
    private $common;
    
    public function __construct(Common $common)
    {
        $this->common = $common;
    }
    
    /**
     * The HTML that should be shown in the widget
     *
     * @return string
     */
    public function html()
    {
        $type = strtolower($this->get('display', 'transactions'));
        // Get our transactions, prettify our amount field and get the correct currency symbol
        $transactions = $type == 'transactions' ? collect($this->common->getTransactions()) : $this->common->getOrders();
        $transactions = $transactions->transform(function($item, $key) {
            $currency = $this->common->getCurSymbol($item['currency'] ?? 'aud');
            $item['amount'] = $currency.number_format($item['amount'], 2, '.', '');
            return $item;
        })
            ->reverse()
            ->slice(0, $this->get('limit', 5), TRUE)
            ->all();
        // Build our view
        return $this->view('widget', ['type' => $type, 'fields' => $this->get('fields', []), 'transactions' => $transactions]);
    }
}
