var payamic_orders_tr = {

    props: ['id', 'order'],

    data: function() {
        return {
            editing: false,
            order_route: this.$parent.detail_route,
            save_route: this.$parent.save_route,
            statuses: this.$parent.statuses,
            status: this.order.order_status,
            timestamp: moment.unix(this.order.timestamp).format('YYYY/MM/DD hh:mm A')
        };
    },

    computed: {
        isNew: function() {
            return this.order.timestamp * 1000 > this.$parent.refresh_time ? '#E75650' : 'inherit';
        }
    },

    methods: {
        edit: function() {
            this.editing = true;
        },
        cancel: function() {
            this.editing = false;
        },
        save: function() {
            this.editing = false;
            this.saving = true;
            this.$http.post(this.save_route, {
                id: this.id,
                status: this.status
            }, function(data) {
                this.order.order_status = this.status;
                this.$dispatch('setFlashSuccess', 'Status updated successfully.');
            }, function() {
                this.$dispatch('setFlashError', 'There was an error trying to save this status. Please try again or contact support.');
            }).always(function() {
                this.saving = false;
            });
        },
    },

    template: `
                <tr v-bind:style="{ backgroundColor: isNew }">
                    <td><a href="{{ order_route }}?id={{ id }}">{{ timestamp }}</a></td>
                    <td>{{ order.name }}</td>
                    <td>{{ order.amount }}</td>
                    <td class="field">
                        <span>{{ order.order_status }}</span>
                        <select-fieldtype v-if="editing" :data.sync="status" :options="statuses"></select-fieldtype>
                    </td>
                    <td class="button">
                        <button class="btn edit" v-if="!editing" @click="edit()">Edit</button>
                        <button class="btn save" v-if="editing" @click="save()">Save</button>
                        <button class="btn cancel" v-if="editing" @click="cancel()">X</button>
                    </td>
                </tr>
    `
};

Vue.component('payamic-trans', {

    props: ['list_route', 'detail_route', 'save_route', 'new_route', 'status', 'inStatuses', 'inPagination'],

    data: function () {
        return {
            loading: true,
            transactions: payamicOrderData,
            statuses: {},
            pagination: JSON.parse(this.inPagination),
            new_orders: 0,
            refresh_time: moment.now(),
            playing: true
        };
    },

    computed: {
        hasTransactions: function() {
            return !this.loading && !jQuery.isEmptyObject(this.transactions);
        },
        statuses: function() {
            var out = [];
            JSON.parse(this.inStatuses).forEach(function(val) {
                out.push({value: val, text: val});
            });
            return out;
        },
        filterStatuses: function() {
            var out = [{value: 'All Statuses', text: 'All Statuses'}];
            JSON.parse(this.inStatuses).forEach(function(val) {
                out.push({value: val, text: val});
            });
            return out;
        }
    },

    ready: function() {
        this.loading = false;
    },

    methods: {
        formatTime: function(time) {
            return moment.unix(time).format('YYYY/MM/DD hh:mm A');
        },
        doStatus: function() {
            var params = {status: this.status};
            if (this.pagination.current > 1) {
                params.page = this.pagination.current;
            }
            this.getTrans(params);
        },
        paginationPageSelected: function(page) {
            var params = {page: page};
            if (this.status != '') {
                params.status = this.status;
            }
            this.getTrans(params);
        },
        getTrans: function(params) {
            this.loading = true;
            this.$http.get(this.list_route, params, function(data) {
                this.transactions = (typeof data.transactions != 'undefined' ? data.transactions : data.orders);
                this.pagination = data.pagination;
                // Check if this is a new page and we should add it to the history
                var href = this.list_route + '?' + jQuery.param(params);
                if (href != window.location) {
                    window.history.pushState({href: href}, '', href);
                }
            }, function() {
                this.$dispatch('setFlashError', 'There was an error trying to retrieve these results.');
            }).always(function() {
                this.loading = false;
            });
        },
        checkOrders: function() {
            this.$http.get(this.new_route, { last_update: this.refresh_time }, function(data) {
                // Update our count
                this.new_orders = data.count;
                if (this.new_orders > 0) {
                    // Play our alert sound with a minor delay to allow for the element to be loaded into the DOM
                    if (this.playing) {
                        setTimeout(function() {
                            $('#orders_alert')[0].play();
                        }, 1000);
                    }
                    // Refresh our order/transaction list
                    var params = {};
                    if (this.pagination.current > 1) {
                        params.page = this.pagination.current;
                    }
                    if (this.status != '' || this.status == 'All Statuses') {
                        params.status = this.status;
                    }
                    this.getTrans(params);
                }
            }, function() {
                // Ignore any errors
            });
        },
        toggleAlert: function() {
            this.playing = !this.playing;
        },
        cancelAutoUpdate: function() {
            clearInterval(this.timer);
        }
    },

    created: function() {
        this.timer = setInterval(this.checkOrders, 60000);
    },

    beforeDestroy: function() {
      clearInterval(this.timer);
    },

    components: {
        'payamic-orders-tr': payamic_orders_tr,
    }

});

Vue.component('payamic-order-status', {
    props: ['id', 'status', 'save_route', 'inStatuses'],

    data: function() {
        return {
            new_status: this.status
        };
    },

    computed: {
        statuses: function() {
            var out = [];
            JSON.parse(this.inStatuses).forEach(function(val) {
                out.push({value: val, text: val});
            });
            return out;
        }
    },

    methods: {
        save: function() {
            this.saving = true;
            this.$http.post(this.save_route, {
                id: this.id,
                status: this.new_status
            }, function(data) {
                this.status = this.new_status;
                this.$dispatch('setFlashSuccess', 'Status updated successfully.');
            }, function() {
                this.$dispatch('setFlashError', 'There was an error trying to save this status. Please try again or contact support.');
            }).always(function() {
                this.saving = false;
            });
        }
    },

    template: `
                        <span id="order_status">{{ status }}</span>
                        <span class="ml-8" style="display: inline-block; width: 300px;"><select-fieldtype :data.sync="new_status" :options="statuses"></select-fieldtype></span>
                        <button class="btn btn-default btn-save" @click="save()">Save</button>
    `
});
