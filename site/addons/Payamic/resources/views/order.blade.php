@extends('layout')

@section('content')
<div class="flexy mb-3">
    <a href="{{ route('payamic.orders') }}" class="btn icon round mr-2">&larr;</a>
    <h1><a href="{{ cp_route('payamic.orders') }}">Order Detail</a></h1>
</div>
<div class="card flush">
    @if ($id == '')
        <div class="no-results">
            Unable to load order - missing ID
        </div>
    @else
        @if (empty($order))
            <div class="no-results">
                Unable to load order - Order ID ({{ $id }}) not found
            </div>
        @else
        <div class="dossier-table-wrapper">
            <table class="dossier">
                <tbody>
                @foreach ([TRUE, FALSE] as $head)
                    @foreach ($order as $key => $val)
                        @if ($head == in_array($key, $headers))
                            <tr>
                                <th>{{ $key }}</th>
                                <td>
                                    @if (!is_array($val))
                                        @if ($key != 'order_status')
                                            {!! nl2br($val) !!}
                                        @else
                                            <payamic-order-status
                                                id="{{ $id }}"
                                                status="{{ $val }}"
                                                save_route="{{ route('payamic.update_order') }}"
                                                in-statuses='{!! json_encode($statuses) !!}'
                                            ></payamic-order-status>
                                        @endif
                                    @else
                                        @foreach ($val as $key2 => $val2)
                                            {{ ucfirst($key2) }}:
                                            @if (!is_array($val2))
                                                {!! nl2br($val2) !!}<br>
                                            @else
                                                <br>
                                                @foreach ($val2 as $key3 => $val3)
                                                    <span class="ml-24" >{{ ucfirst($key3) }}: {!! nl2br($val3) !!}</span><br>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
    @endif
</div>
@endsection
