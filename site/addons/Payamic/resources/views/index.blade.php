@extends('layout')

@section('content')
<script>
    var payamicOrderData = {!! json_encode($transactions) !!};
</script>
<payamic-trans inline-template v-cloak
    list_route="{{ route('payamic.index') }}"
    detail_route="{{ route('payamic.detail') }}"
    save_route="{{ route('payamic.update_order') }}"
    new_route="{{ route('payamic.check_orders') }}"
    status=""
    in-statuses='{!! json_encode($statuses) !!}'
    in-pagination='{!! json_encode($pagination) !!}'
>
<div class="flexy mb-3">
    <h1 class="fill"><a href="{{ cp_route('payamic.index') }}">Recent Transactions</a></h1>
@if (!empty($transactions))
    <div class="btn-group">
        <a href="{{ route('payamic.export', ['type' => 'csv', 'download' => 'true']) }}"
           type="button" class="btn btn-default">{{ t('export') }}</a>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">{{ translate('cp.toggle_dropdown') }}</span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="{{ route('payamic.export', ['type' => 'csv', 'download' => 'true']) }}">{{ t('export_csv') }}</a></li>
            <li><a href="{{ route('payamic.export', ['type' => 'json', 'download' => 'true']) }}">{{ t('export_json') }}</a></li>
        </ul>
      </div>
@endif
    <div class="btn-group ml-8">
      <a href="{{ route('payamic.orders') }}" type="button" class="btn btn-default">View Orders</a>
      <a href="{{ route('payamic.vouchers') }}" type="button" class="btn btn-default">View Vouchers</a>
    </div>
</div>
<div class="card bg-red" style="width: 500px; margin-left: auto; margin-right: auto;" v-if="new_orders > 0">
    <div class="icon icon-warning icon-large icon-left" style="float: left;"></div>
    There are @{{ new_orders }} new orders awaiting your attention!!<br>
    <small>Note: They might be hidden by the 'status' filter</small><br>
    <div style="text-align: right;">
        <button class="btn btn-secondary btn-small" @click="toggleAlert()"><span v-if="playing">Mute</span><span v-else>Unmute</span> Alert</button>
    </div>
    <audio id="orders_alert"><source src="/site/addons/Payamic/resources/assets/audio/chime.mp3" type="audio/mpeg"></audio>
</div>
<div class="card flush">
    <div class="dossier-table-wrapper">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Item ID</th>
                    <th>Full Name</th>
                    <th>Amount</th>
                    <th>Email</th>
                    <th>Gateway</th>
                </tr>
            </thead>
            <tbody v-if="!hasTransactions">
                <tr><td colspan="6">There are no transactions to display</td></tr>
            </tbody>
            <tbody v-else>
                <tr v-for="(id, transaction) in transactions">
                    <td><a href="@{{ detail_route + '?id=' + id }}">@{{ formatTime(transaction.timestamp) }}</a></td>
                    <td>@{{ transaction.item_id }}</td>
                    <td>@{{ transaction.name }}</td>
                    <td>@{{ transaction.amount }}</td>
                    <td>@{{ transaction.email }}</td>
                    <td>@{{ transaction.gateway }}</td>
                </tr>
            </tbody>
        </table>
        <pagination
            v-if="pagination.total > 1"
            :total="pagination.total"
            :current="pagination.current"
            @selected="paginationPageSelected">
        </pagination>
    </div>
</div>
</payamic-trans>
@endsection
