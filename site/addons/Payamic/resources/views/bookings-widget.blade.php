<div class="card">
    <div class="head">
        <h1><a href="{{ cp_route('entries.show', 'bookings') }}">Pending Bookings</a></h1>
    </div>
    <div class="card-body">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Cottage</th>
                    <th>Check-in</th>
                    <th>Check-out</th>
                </tr>
            </thead>
            <tbody>
        @if (empty($bookings))
                <tr>
                    <td colspan="4">There are no bookings to display</td>
                </tr>
        @else
            @foreach ($bookings as $booking)
                <tr>
                    <td><a href="{{ cp_route('entry.edit', 'bookings/'.array_get($booking, 'slug')) }}">{{ array_get($booking, 'name') }}</a></td>
                    <td>{{ array_get($booking, 'cottage_name') }}</td>
                    <td>{{ array_get($booking, 'start') }}</td>
                    <td>{{ array_get($booking, 'end') }}</td>
                </tr>
            @endforeach
        @endif
            </tbody>
        </table>
    </div>
</div>
