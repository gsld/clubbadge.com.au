@extends('layout')

@section('content')
<script>
    var payamicOrderData = {!! json_encode($orders) !!};
</script>
<payamic-trans inline-template v-cloak
    list_route="{{ route('payamic.orders') }}"
    detail_route="{{ route('payamic.order') }}"
    save_route="{{ route('payamic.update_order') }}"
    new_route="{{ route('payamic.check_orders') }}"
    status="{{ request()->input('status', 'All Statuses') }}"
    in-statuses='{!! json_encode($statuses) !!}'
    in-pagination='{!! json_encode($pagination) !!}'
>
<div class="flexy mb-3">
    <h1 class="fill"><a href="{{ cp_route('payamic.orders') }}">Recent Orders</a></h1>
    <div class="btn-group ml-1">
      <select-fieldtype :data.sync="status" :options="filterStatuses" @change="doStatus()"></select-fieldtype>
    </div>
    <div class="btn-group ml-1">
      <a href="{{ cp_route('payamic.index') }}" type="button" class="btn btn-default">View All Transactions</a>
    </div>
    <div class="btn-group ml-1">
      <a href="{{ cp_route('payamic.vouchers') }}" type="button" class="btn btn-default">View Vouchers</a>
    </div>
</div>
<div class="card bg-red" style="width: 500px; margin-left: auto; margin-right: auto;" v-if="new_orders > 0">
    <div class="icon icon-warning icon-large icon-left" style="float: left;"></div>
    There are @{{ new_orders }} new orders awaiting your attention!!<br>
    <small>Note: They might be hidden by the 'status' filter</small><br>
    <div style="text-align: right;">
        <button class="btn btn-secondary btn-small" @click="toggleAlert()"><span v-if="playing">Mute</span><span v-else>Unmute</span> Alert</button>
    </div>
    <audio id="orders_alert"><source src="/site/addons/Payamic/resources/assets/audio/chime.mp3" type="audio/mpeg"></audio>
</div>
<div class="card flush">
    <div class="loading" v-if="loading">
        <span class="icon icon-circular-graph animation-spin"></span> Loading
    </div>
    <div class="dossier-table-wrapper">
        <table class="dossier orders" data-csrf="{{ csrf_token() }}">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Full Name</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody v-if="hasTransactions">
                <tr is="payamic-orders-tr" v-for="(id, order) in transactions" :id="id" v-bind:key="id" :order="order"></tr>
            </tbody>
            <tbody v-else>
                <tr><td colspan="5">There are no orders to display</td></tr>
            </tbody>
        </table>
        <pagination
            v-if="pagination.total > 1"
            :total="pagination.total"
            :current="pagination.current"
            @selected="paginationPageSelected">
        </pagination>
    </div>
</div>
</payamic-trans>
@endsection
