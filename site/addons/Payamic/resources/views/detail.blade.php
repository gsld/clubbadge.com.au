@extends('layout')

@section('content')
<div class="flexy mb-3">
    <a href="{{ route('payamic.index') }}" class="btn icon round mr-2">&larr;</a>
    <h1><a href="{{ cp_route('payamic.index') }}">Transaction Detail</a></h1>
</div>
<div class="card flush">
    @if ($id == '')
        <div class="no-results">
            Unable to load transaction - missing ID
        </div>
    @else
        @if (empty($transaction))
            <div class="no-results">
                Unable to load transaction - Transaction ID ({{ $id }}) not found
            </div>
        @else
        <div class="dossier-table-wrapper">
            <table class="dossier">
                <tbody>
                @foreach ($transaction as $key => $val)
                    <tr>
                        <th>{{ $key }}</th>
                        <td>
                            @if (!is_array($val))
                                {!! nl2br($val) !!}
                            @else
                                @foreach ($val as $key2 => $val2)
                                    {{ ucfirst($key2) }}:
                                    @if (!is_array($val2))
                                        {!! nl2br($val2) !!}<br>
                                    @else
                                        <br>
                                        @foreach ($val2 as $key3 => $val3)
                                            <span class="ml-24">{{ ucfirst($key3) }}: {!! nl2br($val3) !!}</span><br>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
    @endif
</div>
@endsection
