<div class="card">
    <div class="head">
        <h1><a href="{{ cp_route('payamic.index') }}">Recent {{ $type == 'transactions' ? 'Transactions' : 'Orders' }}</a></h1>
    </div>
    <div class="card-body">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Item ID</th>
                    <th>Full Name</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
        @if (empty($transactions))
                <tr>
                    <td colspan="{{ count($fields) }}">There are no {{ $type == 'transactions' ? 'transactions' : 'orders' }} to display</td>
                </tr>
        @else
            @foreach ($transactions as $id => $transaction)
                <tr>
                    <td><a href="{{ route('payamic.detail', ['id' => $id]) }}">{{ date('Y-m-d H:s', array_get($transaction, 'timestamp')) }}</a></td>
                    <td>{{ array_get($transaction, 'item_id') }}</td>
                    <td>{{ array_get($transaction, 'name') }}</td>
                    <td>{{ array_get($transaction, 'amount') }}</td>
                </tr>
            @endforeach
        @endif
            </tbody>
        </table>
    </div>
</div>
