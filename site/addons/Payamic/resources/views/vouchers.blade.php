@extends('layout')

@section('content')
<div class="flexy mb-3">
    <h1 class="fill"><a href="{{ cp_route('payamic.vouchers') }}">Vouchers</a></h1>
    <div class="btn-group ml-8">
        <a href="{{ route('payamic.create_voucher') }}" type="button" class="btn btn-primary">New Voucher</a></td>
    </div>
    <div class="btn-group ml-8">
      <a href="{{ route('payamic.index') }}" type="button" class="btn btn-default">View Transactions</a>
      <a href="{{ route('payamic.orders') }}" type="button" class="btn btn-default">View Orders</a>
    </div>
</div>
<div class="card flush">
    <div class="dossier-table-wrapper">
        <table class="dossier">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Expiry</th>
                    <th>Max</th>
                    <th>Used</th>
                    <th>Active</th>
                </tr>
            </thead>
            <tbody>
        @if (empty($vouchers))
                <tr>
                    <td colspan="5">There are no vouchers to display</td>
                </tr>
        @else
            @foreach ($vouchers as $id => $voucher)
                <tr>
                    <td><a href="{{ route('payamic.edit_voucher', ['id' => $id]) }}">{{ $voucher['name'] }}</a></td>
                    <td>{{ $voucher['type'] }}</td>
                    <td>{{ $voucher['type'] == 'amount' ? '$'.number_format($voucher['amount'], 2, '.', '') : $voucher['amount'].'%' }}</td>
                    <td>{{ date("Y/m/d H:i", $voucher['expiry']) }}</td>
                    <td>{{ $voucher['max'] == 0 ? 'Unlimited' : $voucher['max'] }}</td>
                    <td>{{ $voucher['counter'] }}</td>
                    <td>{{ $voucher['is_active'] ? 'Yes' : 'No' }}</td>
                </tr>
            @endforeach
        @endif
            </tbody>
        </table>
    </div>
</div>
@endsection
