@extends('layout')

@section('content')

    <script>
        Statamic.Publish = {
            contentData: {!! json_encode($data) !!},
        };
    </script>

    <publish title="{{ $id ? $data['name'] : 'New Voucher' }}"
             :is-new="{{ bool_str($id === null) }}"
             fieldset-name="voucher"
             content-type="addon"
             submit-url="{{ $submitUrl }}"
             id="{{ $id }}"
    ></publish>

@endsection
