<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Fieldtype;

use Statamic\API\User;

class PayamicFieldtype extends Fieldtype
{
    private $common;

    public function __construct(Common $common)
    {
        $this->common = $common;
    }

    /**
     * The blank/default value
     *
     * @return int
     */
    public function blank()
    {
        // Find our next user number as our default value
        return $this->common->findNextUserNum();
    }

    /**
     * Pre-process the data before it gets sent to the publish page
     *
     * @param int $data
     * @return int
     */
    public function preProcess($data)
    {
        // Check if we have a value already, and if not, then find our next user number
        if (is_null($data) || $data == '') {
            $data = $this->common->findNextUserNum();
        }
        return $data;
    }

    /**
     * Process the data before it gets saved
     *
     * @param int $data
     * @return int
     */
    public function process($data)
    {
        // Check if we have a value already, and if not, then find our next user number
        if (is_null($data) || $data == '') {
            $data = $this->common->findNextUserNum();
        }
        return $data;
    }
}
