<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Tags;

use Statamic\API\GlobalSet;
use Statamic\API\User;

use Session;
use Request;
use Log;

use DateTime;
use DateInterval;

class PayamicTags extends Tags
{
    private $common;
    private $error_return = '';             // TODO: Work out where to redirect on a config error

    public function __construct(Common $common)
    {
        $this->common = $common;
    }

    /**
     * The {{ payamic }} tag
     *
     * @return string|array
     */
    public function index()
    {
        return "";
    }

    /**
     * The {{ payamic:get_result }} tag
     *
     * @return array
     */
    public function getResult()
    {
        $result = json_decode(Session::get('payamic'), TRUE);
        if (!is_null($result)) {
            return $this->parse($result);
        } else {
            return $this->parse(array('payment' => 'empty'));
        }
    }

    /**
     * The {{ payamic:getUserTransactions }} tag
     *
     * @return array
     */
    public function getUserTransactions()
    {
        // Get our transactions
        $trans = $this->common->getTransactions();
        // Filter this array by our user
        $out = array_reverse(array_filter($trans, [$this, 'userFilter']));
        if (empty($out)) {
            return $this->parseNoResults();
        }
        return $this->parseLoop($out);
    }

    /**
     * The {{ payamic:getStripePubKey }} tag
     *
     * @return string
     */
    public function getStripePubKey()
    {
        // Load our global vars
        if (!$this->common->loadVars()) {
            // There was an error loading our vars so crash
            Session::flash('payment_errors', 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
            return 'Error!';
        }
        return $this->common->stripe_pub_key;
    }

    /**
     * The {{ payamic:getLevy }} tag
     *
     * @return string
     */
    public function getLevy()
    {
        $val = $this->getConfig($this->getParam('gateway').'_levy');
        return (isset($val) && $val != '' && floatval($val) > 1 ? floatval($val) : 0);
    }

    /**
     * The {{ payamic:getMinimum }} tag
     *
     * @return string
     */
    public function getMinimum()
    {
        return $this->getConfig($this->getParam('gateway').'_minimum');
    }

    /**
     * The {{ payamic:getCountry }} tag
     *
     * @return string
     */
    public function getCountry()
    {
        $countries = $this->getConfig('countries');
        return isset($countries[strtoupper($this->getParam('code'))]) ? $countries[strtoupper($this->getParam('code'))] : '(unknown)';
    }

    /**
     * The {{ payamic:getCountryOptions }} tag
     *
     * @return string
     */
    public function getCountryOptions()
    {
        $countries = $this->getConfig('countries');
        $out = '';
        foreach ($countries as $key => $val) {
            $out .= '<option value="'.strtolower($key).'"'.($key == 'AU' ? ' selected' : '').'>'.$val.'</option>'."\n";
        }
        return $out;
    }

    /**
     * The {{ payamic:action }} tag
     *
     * @return string
     */
    public function action()
    {
        return $this->actionUrl('checkout');
    }

    /**
     * The {{ payamic:getPaymentStr }} tag
     * @param  amount   The amount for this payment
     * @param  desc     The description for this payment
     * @param  item_id  The Item ID for this payment
     *
     * @return string
     */
    public function getPaymentStr()
    {
        $amount = $this->getParam('amount');
        $desc = $this->getParam('desc');
        $item_id = $this->getParam('item_id');

        // TODO: Need to validate the values above
        $str = $this->common->createPaymentStr($amount, $item_id, $desc);
        return $str;
    }

    /**
     * The {{ payamic:getMemberRenewStr }} tag
     * @param  user_id  The user ID for this payment
     *
     * @return string
     */
    public function getMemberRenewalStr()
    {
        $user = User::find($this->getParam('user_id'));
        if (is_null($user)) {
            return "ERROR: User not found";
        }
        $type = $user->get('membership_type');
        $prices = $this->common->getMembershipPrices();
        $amount = $prices[$type]['price'];
        $desc = ucfirst($type).' Membership Renewal';
        $item_id = $type;

        $str = $this->common->createPaymentStr($amount, $item_id, $desc);
        return $str;
    }

    /**
     * The {{ payamic:getPaymentURL }} tag
     *
     * @return string
     */
    public function getPaymentURL()
    {
        $amount = $this->getParam('amount', request()->input('amount'));
        $desc = $this->getParam('desc', request()->input('desc'));
        $item_id = $this->getParam('item_id', request()->input('item_id'));

        return $this->common->getPaymentURL($amount, $desc, $item_id, ($this->getParam('redirect') == 'true'));
    }

    /**
     * The {{ payamic:getCustomPaymentURL }} tag
     *
     * @return string
     */
    public function getCustomPaymentURL()
    {
        return $this->common->getCustomPaymentURL($this->getParam('redirect') == 'true');
    }

    /**
     * The {{ payamic:getPaymentDetails }} tag
     *
     * @return string|array
     */
    public function getPaymentDetails()
    {
        // Get our hidden payment field(s) - currently just "payment"
        $segments = request()->segments();
        $payment = array_pop($segments);
        $vars = $this->common->checkPayment($payment);
        if ($vars === FALSE) {
            $vars = array(
                'amount' => 'Unknown',
                'desc' => 'Unknown',
                'item_id' => 'Unknown',
                'items' => [],
                'payment_summary' => '',
                'payment_tax' => 0,
                'error' => $this->common->error ?: "ERROR: The payment details are missing or invalid."
            );
        } else {
            $vars['error'] = '';
            $vars['payment_tax'] = 0;
            // Build our summary if necessary
            if ($vars['desc'] == 'Cart Payment') {
                $cart = $this->cart();
                $vars['items'] = $cart['items'];
                if (empty($cart['items'])) {
                    $vars['payment_summary'] = 'You have no items in your shopping cart';
                } else {
                    $summary = '';
                    $tax = 0;
                    foreach ($cart['items'] as $cid => $item) {
                        $summary .= ucfirst($item['name']);
                        if (isset($item['fields']) && !empty($item['fields'])) {
                            $summary .= ' - ';
                            foreach ($item['fields'] as $field) {
                                $summary .= ucfirst($field['field_name']).': '.$field['field_value'].', ';
                            }
                            $summary = substr($summary, 0, -2);
                        }
                        $summary .= ' - '.$this->common->getCurSymbol($item['currency'] ?? 'aud').number_format(floatval($item['price']), 2, '.', '').' x '.$item['quantity'].' = '.number_format($item['subtotal'], 2, '.', '').'<br>'.PHP_EOL;
                        // Check for tax on each item because we didn't pass that through our cart fml
                        $entry = $this->common->getItemEntry($item['itemid']);
                        Log::info('Item: '.$item['itemid'].', Entry: '.(!is_null($entry) ? 'Found': '(none)'));
                        if ($entry) {
                            $item_tax = $entry->get('tax');
                            Log::info('Tax: '.$item_tax);
                            if ($item_tax > 0) {
                                $tax += $item['subtotal'] * $item_tax / 100;
                                Log::info('Tax total: '.$tax);
                            }
                        }
                    }
                    $vars['payment_summary'] = $summary;
                    $vars['payment_tax'] = $tax;
                }
            } else {
                $summary = $vars['desc'];
                $in = request()->input();
                if (!empty($in)) {
                    $got_fields = FALSE;
                    foreach ($in as $key => $val) {
                        if ($key != 'quantity') {
                            if (!$got_fields) {
                                $summary .= ' - ';
                                $got_fields = TRUE;
                            }
                            $summary .= ucfirst($key).': '.$val.', ';
                        }
                    }
                    if ($got_fields) {
                        $summary = substr($summary, 0, -2);
                    }
                }
                $qty = 1;
                if (isset($in['quantity']) && intval($in['quantity']) > 0) {
                    $summary .= ' x '.$in['quantity'];
                    $qty = intval($in['quantity']);
                }
                $summary .= ' = '.$this->common->getCurSymbol($vars['currency'] ?? 'aud').number_format(floatval($vars['amount']) * $qty, 2, '.', '');
                $vars['amount'] = $vars['amount'] * $qty;
                $vars['payment_summary'] = $summary;
                // Try to grab our item if it exists
                $item = $this->common->getItemEntry($vars['item_id']);
                if ($item) {
                    // Grab the fields from our item
                    $item = $item->data();
                    $vars['items'][] = [
                        'itemid' => $vars['item_id'],
                        'name' => $item['title'],
                        'fields' => array_diff($in, ['quantity' => 1]),
                        'price' => $item['price'],
                        'quantity' => $qty,
                        'subtotal' => $item['price'] * $qty,
                    ];
                    $vars['payment_tax'] = isset($item['tax']) && $item['tax'] > 0 ? ($item['price'] * $qty * $item['tax'] / 100) : 0;
                } else {
                    $vars['items'] = [];
                }
            }
        }
        $vars['payment'] = $payment;
        Log::info('Tax total: '.$vars['payment_tax']);
        return $vars;
    }

    /**
     * The {{ payamic:getPaymentErrors }} tag
     *
     * @return string|array
     */
    public function getPaymentErrors()
    {
        return Session::get('payment_errors');
    }

    /**
     * The {{ payamic:calcBookingDetails }} tag
     *
     * @return string|array
     */
    public function calcBookingDetails()
    {
        return $this->common->calcBookingDetails();
    }

    /**
     * The {{ payamic:setReferrer }} tag
     *
     * @return string|array
     */
    public function setReferrer()
    {
        $this->common->setReferrer();
    }

    /**
     * The {{ payamic:getReferrer }} tag
     *
     * @return string|array
     */
    public function getReferrer()
    {
        return Session::get('payment_referrer');
    }

    /**
     * The {{ payamic:regoDue }} tag
     *
     * @return bool
     */
    public function regoDue()
    {
        // Check if the current user's renewal date is within a month of now
        $inUser = $this->getParam('user');
        $user = $inUser != '' ? User::find($inUser) : User::getCurrent();
        if (!is_null($user)) {
            $renewal = new DateTime($user->get('valid_until'));
            return (time() > $renewal->sub(new DateInterval('P30D'))->format('U'));
        } else {
            return FALSE;
        }
    }

    /**
     * The {{ payamic:cart }} tag
     *
     * @return string|array
     */
    public function cart()
    {
        // Get our cart
        $cart = collect(request()->session()->get('cart'));
        if ($cart->isEmpty()) {
            return ['no_items' => TRUE, 'items' => [], 'total' => 0];
        }
        // Parse our list of items building the necessary structure for our generic cart
        // (add all the unknown fields to a $fields array)
        $items = $cart->map(function($item, $key) {
            $fields = [];
            // Grab all the non-standard fields
            foreach ($item as $field => $value) {
                if ($field != 'itemid' && $field != 'name' && $field != 'price' && $field != 'quantity') {
                    $fields[] = ['field_name' => $field, 'field_value' => $value];
                }
            }
            return [
                'itemid' => $item['itemid'],
                'name' => $item['name'],
                'fields' => $fields,
                'price' => $item['price'],
                'quantity' => $item['quantity'],
                'subtotal' => $item['price'] * $item['quantity'],
                'cid' => $key
            ];
        });
        // Get our total
        $total = $cart->reduce(function($carry, $item) {
            return $carry + ($item['quantity'] * $item['price']);
        });
        // Return our cart details
        return ['items' => $items->values()->all(), 'total' => $total];
    }

    public function getCartCount()
    {
        $cart = collect(request()->session()->get('cart'));
        $count = $cart->reduce(function ($carry, $item) {
            return $carry + $item['quantity'];
        });
        return $count ?? 0;
    }

    public function getShipping() {
        // Get our shipping options
        $shipping = $this->common->getShippingOptions($this->getParam('item_id').'|'.request()->input('quantity', 1));
        // Check we have some options!
        if (isset($shipping['error'])) {
            return $shipping['error'];
        }
        return $this->parseLoop($shipping);
    }

    public function getShippingWeight() {
        return $this->common->getShippingWeight($this->getParam('item_id').'|'.request()->input('quantity', 1));
    }

    /**
     * Function for filtering transactions by the current user
     *
     * @param  $trans   Transaction to test
     * @return bool
     */
    private function userFilter($trans) {
        // Check if this transaction is for this user
        $user = User::getCurrent();
        return (isset($trans['user']) && $trans['user'] == $user->getAuthIdentifier());
    }
}
