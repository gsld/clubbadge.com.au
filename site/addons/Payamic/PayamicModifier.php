<?php

namespace Statamic\Addons\Payamic;

use Statamic\Extend\Modifier;

class PayamicModifier extends Modifier
{
    /**
     * Modify a value
     *
     * @param mixed  $value    The value to be modified
     * @param array  $params   Any parameters used in the modifier
     * @param array  $context  Contextual values
     * @return mixed
     */
    public function index($value, $params, $context)
    {
        return str_replace(["\n", "TOTAL:"], ["<br>\n", "<strong>TOTAL:</strong>"], $value);
    }
}
