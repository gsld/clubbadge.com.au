<?php

namespace Statamic\Addons\Payamic;

use DateTime;
use DateInterval;
use Session;
use Request;
use Log;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Carbon\CarbonInterval;

use Statamic\API\Email;
use Statamic\API\Folder;
use Statamic\API\GlobalSet;
use Statamic\API\User;
use Statamic\Data\Entries\EntryFactory;

use Statamic\Extend\Extensible;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error;
use Stripe\Subscription;

class Common
{
    use Extensible;

    public $globals = [];
    public $prices = [];
    public $admin_email = '';
    public $admin_name = '';
    public $from_email = '';
    public $from_name = '';
    public $bcc_email = '';
    public $site_name = '';
    public $email_template_payment_default = '';
    public $email_template_subscription_success = '';
    public $email_template_subscription_reminder = '';
    public $page_checkout = '';
    public $page_success = '';
    public $stripe_priv_key = '';
    public $stripe_pub_key = '';
    public $error = '';
    public $vouchers;
    public $shipping;

    // Set up our initial array of payment detail fields to ensure they exist
    public $payment_details = array(
        'name' => '',
        'email' => '',
        'amount' => '',
        'desc' => '',
        'item_id' => '',
        'gateway' => '',
        'timestamp' => '',
        'trans_id' => '',
        'gateway_vars' => '',
    );

    public function encode($str)
    {
        return base64_encode($str);
    }

    public function decode($str)
    {
        return base64_decode($str);
    }

    public function getCustomPaymentURL($redirect)
    {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            Session::flash('payment_errors', 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
            return 'Error!';
        }

        // Build our URL
        $url = '/'.$this->page_checkout.'/'.$this->createPaymentStr('custom', 'custom', 'Custom Payment');
        if ($redirect) {
            return redirect($url);
        }
        return $url;
    }

    public function getPaymentURL($amount, $desc, $item_id, $redirect) {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            Session::flash('payment_errors', 'ERROR: There was an error with the payment system configuration. We will attempt to fix this as soon as possible.');
            return 'Error!';
        }

        // TODO: Need to validate the values above
        $url = '/'.$this->page_checkout.'/'.$this->createPaymentStr($amount, $item_id, $desc);
        if ($redirect) {
            return redirect($url);
        }
        return $url;
    }

    public function setReferrer()
    {
        // Get our referrer and trim it back to just the domain
        $referrer = Request::server('HTTP_REFERER');
        $start = strpos($referrer, '://') + 3;
        $referrer = substr($referrer, $start > 3 ? $start : 0);
        $referrer = substr($referrer, 0, strpos($referrer, '/'));
        Session::flash('payment_referrer', $referrer);
    }

    public function checkPayment($str)
    {
        $payment = [];
        // Check if our input string is an actual payment and unpack the details
        if (!isset($str) || $str == '') {
            return FALSE;
        }
        $decoded = $this->decode($str);
        // Check this is an encoded payment string (contains the /// seperator)
        if (strpos($decoded, '///') === FALSE) {
            return FALSE;
        }
        list($price, $item_id, $desc) = explode('///', $decoded);
        if (!$this->validatePayment($price, $item_id, $desc)) {
            // This isn't a valid payment so error
            return FALSE;
        }
        // Check if this is our shopping cart payment
        if ($price == 'cart') {
            $cart = collect(request()->session()->get('cart'));
            // Calculate the correct total value
            $payment['amount'] = $cart->reduce(function($carry, $item) {
                return $carry + ($item['quantity'] * $item['price']);
            });
        } else {
            $payment['amount'] = ($price == 'custom' ? $price : number_format($price, 2, '.', ''));
        }
        $payment['desc'] = $desc;
        $payment['item_id'] = $item_id;
        return $payment;
    }

    public function createPaymentStr($price, $item_id, $desc = "")
    {
        // Check our inputs are reasonable (non-empty with positive float price)
        if (!$this->validatePayment($price, $item_id, $desc)) {
            // This isn't a valid payment so error
            return FALSE;
        }

        // Create our payment string from the input details
        return $this->encode($price.'///'.$item_id.'///'.$desc);
    }

    public function validatePayment(&$price, $item_id, $desc)
    {
        // Check our inputs are reasonable (non-empty with positive float price or special "custom" price)
        if ($price == '' || (floatval($price) <= 0 && $price != 'custom' && $price != 'cart') || $desc == '') {
            return FALSE;
        }
        // Check for any extra validation that needs to happen
        if (stripos($desc, 'Booking') && !$this->validateBooking()) {
            return FALSE;
        }
        // Check if this item is an entry
        $entry = \Statamic\API\Content::find($item_id);
        if (!$entry) {
            // Not an entry so ignore it
            return TRUE;
        }
        // Override the given price with our item's correct price
        $price = $entry->get('price', $price);
        // Check if we have a quantity field and if so, enough quantity for this payment
        $qty = $entry->get('quantity');
        if (!is_null($qty) && $qty > -1 && $qty < request()->input('quantity', 1)) {
            $this->error = 'ERROR: Requested quantity is unavailable';
            return FALSE;
        }
        return TRUE;
    }

    public function getItemEntry($id) {
        return \Statamic\API\Content::find($id);
    }

    public function getShippingWeight($obj) {
        // Check if we are processing our cart or not
        if (substr($obj, 0, 5) != 'cart|') {
            // Get our item ID and quantity
            if (strpos($obj, '|') === FALSE) {
                return 0;
            }
            list($item, $quantity) = explode('|', $obj);
            // If the item is numeric then assume it is a weight value
            if (is_numeric($item)) {
                return $item * $quantity;
            }
            // Attempt to get our item entry and return the weight for it
            $entry = \Statamic\API\Content::find($item);
            Log::info('Single item - Weight: '.(!is_null($entry) ? floatval($entry->get('weight')) * $quantity : 0));
            return !is_null($entry) ? floatval($entry->get('weight')) * $quantity : 0;
        } else {
            // Get the item ID's for each item in our cart and calculate their weights
            $cart = collect(request()->session()->get('cart'));
            return $cart->reduce(function($carry, $item) {
                // Attempt to get our item entry and add it's weight to the total
                $entry = isset($item['itemid']) ? \Statamic\API\Content::find($item['itemid']) : null;
                Log::info('Cart Items - Weight: '.(!is_null($entry) ? floatval($entry->get('weight')) * $item['quantity'] : 0));
                return $carry + (!is_null($entry) ? floatval($entry->get('weight')) * $item['quantity'] : 0);
            });
        }
    }

    public function getShippingOptions($obj) {
        $out = [];
        // Check we have loaded our config
        $this->loadVars();
        // Check if we have any shipping options
        if (is_null($this->shipping) || $this->shipping->isEmpty()) {
            $this->error = 'Attempted shipping option use without any shipping options loaded!!';
            Log::error($this->error);
            $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
            return ['error' => 'No shipping options found'];
        }
        Log::info('Getting weight: '.$obj);
        // Get the weight of our item(s)
        $weight = $this->getShippingWeight($obj);
        Log::info('Weight: '.$weight);
        // Loop over all the shipping options and find the relevant price for this weight if available
        $out = $this->shipping->map(function ($option, $id) use ($weight) {
            return [
                'id' => $option['shipping_id'],
                'name' => $option['shipping_name'],
                'price' => $this->getShippingPrice($id, $weight)
            ];
        })->filter(function($value) {
            return $value['price'] >= 0;
        })->keyBy('id')->all();
        return $out;
    }

    public function getShipping($id, $obj) {
        // Check if we have any shipping options
        if (is_null($this->shipping) || $this->shipping->isEmpty()) {
            $this->error = 'Attempted shipping option use without any shipping options loaded!!';
            Log::error($this->error);
            $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
            $this->error = 'No shipping options found';
            return -1;
        }
        $shipping = $this->shipping->get($id);
        // Check if this shipping ID exists
        if (is_null($shipping)) {
            $this->error = 'Shipping option invalid';
            return -1;
        }
        // Calculate the weight of our payment item(s)
        $weight = $this->getShippingWeight($obj);
        // Check if our shipping option is valid
        $price = $this->getShippingPrice($id, $weight);
        if ($price < 0) {
            $this->error = 'Weight too large for this shipping option';
            return -1;
        }
        return $price;
    }

    public function getShippingPrice($id, $weight) {
        // Find the smallest available price for our weight for this shipping option
        $shipping = collect($this->shipping->get($id)['shipping_levels'])
            ->sortBy('weight')
            ->first(function($key, $value) use ($weight) {
                return $value['weight'] >= $weight;
            });
        // Check we found value
        if (empty($shipping)) {
            return -1;
        }
        return $shipping['price'];
    }

    public function loadVars()
    {
        // Load our vars if necessary
        if (!isset($this->admin_email) || $this->admin_email == '') {
            // Check we have an admin email address
            $this->admin_email = $this->getConfig('admin_email');
            if (!isset($this->admin_email) || $this->admin_email == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the site email address from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
            // Load a pretty name for the from address and BCC email if they exist
            $this->admin_name = $this->getConfig('admin_name');
            $this->bcc_email = $this->getConfig('bcc_email');
            $this->from_email = $this->getConfig('from_email');
            $this->from_name = $this->getConfig('from_name');

            // Check we have a site name
            $this->site_name = $this->getConfig('site_name');
            if (!isset($this->site_name) || $this->site_name == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the site name from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }

            // Check we have our email templates
            $this->email_template_payment_default = $this->getConfig('default_payment_email_template_prefix');
            $this->email_template_subscription_success = $this->getConfig('subscription_success_email_template_prefix');
            $this->email_template_subscription_due = $this->getConfig('subscription_reminder_email_template_prefix');
            $this->email_template_bookings_reminder = $this->getConfig('bookings_reminder_email_template_prefix');
            if (!isset($this->email_template_payment_default) || $this->email_template_payment_default == ''
                    || !isset($this->email_template_subscription_success) || $this->email_template_subscription_success == ''
                    || !isset($this->email_template_subscription_due) || $this->email_template_subscription_due == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the notification email templates from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
            // Check we have our Stripe Keys
            $this->stripe_priv_key = $this->getConfig('stripe_priv_key', NULL, NULL, FALSE, FALSE);
            $this->stripe_pub_key = $this->getConfig('stripe_pub_key', NULL, NULL, FALSE, FALSE);
            if (!isset($this->stripe_priv_key) || $this->stripe_priv_key == ''
                    || !isset($this->stripe_pub_key) || $this->stripe_pub_key == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the Stripe keys from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
            // Check we have our checkout and success pages
            $this->page_checkout = $this->getConfig('page_checkout');
            $this->page_success = $this->getConfig('page_success');
            if (!isset($this->page_checkout) || $this->page_checkout == ''
                    || !isset($this->page_success) || $this->page_success == '') {
                // Error getting our settings
                $this->error = "There was an error retrieving the default checkout and success pages from the settings database";
                Log::error($this->error);
                $this->sendAdminEmail(array('Error', $this->error), 'PAYAMIC - Payment System error!!');
                return FALSE;
            }
        }
        $this->loadVouchers();
        $this->loadShipping();
        return TRUE;
    }

    private function loadVouchers() {
        // Load our shipping details if they haven't already been loaded
        if (!$this->vouchers) {
            // Initialise our var
            $this->vouchers = collect();
            $vouchers = [];
            // Get our list of transaction record files
            $files = collect(Folder::disk('storage')->getFiles('/addons/Payamic/'))
            ->filter(function ($file) {
                return substr($file, 15, 8) == 'voucher-';
            })->each(function ($file) use (&$vouchers) {
                $voucher = $this->storage->getYAML(substr($file, 15, -5));
                $voucher['uuid'] = substr($file, 23, -5);
                $vouchers[] = $voucher;
            });
            $this->vouchers = collect($vouchers, [])->keyBy('id');
        }
    }

    private function loadShipping() {
        // Load our shipping details if they haven't already been loaded
        Log::info('Loading shipping');
        if (!$this->shipping) {
            Log::info('Shipping not loaded yet');
            // Initalise our var
            $this->shipping = collect();
            // Attempt to get our globals location from the config
            Log::info('Global: '.$this->getConfig('shipping_global', '.'));
            list($global, $var) = explode('.', $this->getConfig('shipping_global', '.'));
            if (isset($global) && $global != '' && isset($var) && $var != '') {
                // Attempt to load any vouchers from the global
                $shipping = GlobalSet::whereHandle($global);
                if ($shipping) {
                    $this->shipping = collect($shipping->get($var, []))->keyBy('shipping_id');
                }
            }
        }
//        Log::info('Shipping loaded: '.print_r($this->shipping, true));
    }

    public function incVoucher($id) {
        $voucher = $this->getVoucher($id);
        $file = $voucher['uuid'];
        unset($voucher['uuid']);
        $voucher['counter']++;
        $this->saveVoucher($file, $voucher);
    }

    public function getVouchers() {
        // Ensure our vouchers are loaded
        $this->loadVouchers();
        return $this->vouchers;
    }

    public function getVoucher($id) {
        // Ensure our vouchers are loaded
        $this->loadVouchers();
        return $this->vouchers->get(strtoupper($id));
    }

    public function checkVoucher($id) {
        // Check the supplied voucher is valid and return its details
        $voucher = $this->getVoucher($id);
        // Check if the input ID is a valid voucher
        if (is_null($voucher) || $voucher['expiry'] < time() || ($voucher['max'] > 0 && $voucher['max'] >= $voucher['counter'])) {
            return ['result' => 'error', 'error' => 'Voucher not found or expired'];
        }
        if ($voucher['start'] > time()) {
            return ['result' => 'error', 'error' => 'Voucher not active yet'];
        }
        if (isset($voucher['start_time']) && isset($voucher['end_time']) && $voucher['start_time'] != '' && $voucher['end_time'] != ''
                && (Carbon::createFromTimeString($voucher['start_time']) > time() || Carbon::createFromTimeString($voucher['end_time']) < time())) {
            return ['result' => 'error', 'error' => 'Voucher only valid between '.$voucher['start_time'].' and '.$voucher['end_time']];
        }
        $voucher['result'] = 'success';
        return $voucher;
    }

    public function saveVoucher($id, $data) {
        $name = 'voucher-'.$id;
        $data['id'] = strtoupper($data['id']);
        array_walk_recursive($data, 'self::trimArray');
        $this->storage->putYAML($name, $data);
    }

    public function sendAdminEmail($data, $title, $addy = NULL)
    {
        if (!isset($addy) || $addy == '') {
            $addy = (isset($this->admin_email) && $this->admin_email != '' ? $this->admin_email : 'payamic@gsld.com.au');
        }
        $email = Email::create();
        $email->to($addy)
            ->subject($title)
            ->with($data)
            ->automagic(TRUE);
        if ($this->from_name != '') {
            $email->from($this->from_email, $this->from_name);
        } elseif ($this->admin_name != '') {
            $email->from($addy, $this->admin_name);
        } else {
            $email->from($addy);
        }
        if ($this->bcc_email != '') {
            $email->bcc($this->bcc_email);
        }
        $email->send();
    }

    public function setPaymentDetail($var, $val) {
        $this->payment_details[$var] = $val;
    }

    public function setTransCount() {
        // Get our transaction count
        $count = intval($this->storage->get('order_counter', 100)) + 1;
        // Set the detail
        $this->setPaymentDetail('trans_count', $count);
        // Save the new count
        $this->storage->put('order_counter', $count);
    }

    public function setCustomFields() {
        foreach ($this->getConfig('fields') as $field) {
            $val = Request::input($field['name']);
            $this->setPaymentDetail($field['name'], $val);
        }
    }

    public function recordTrans()
    {
        array_walk_recursive($this->payment_details, 'self::trimArray');
        // Store this transaction
        $this->storage->putYAML('trans-'.date('YmdHis', time()), $this->payment_details);
    }

    public function sendEmails($form = NULL)
    {
        $this->setPaymentDetail('time', date('Y M d H:i:s', $this->payment_details['timestamp']));
        if (is_null($form) || $form == '') {
            $form = $this->default_email_template;
        }
        // Send a success email to the customer if we have an email address for them
        $email_addy = $this->payment_details['email'];
        if ($email_addy != '') {
            $email = Email::create();
            $email->to($email_addy)
                ->subject(request()->input('success_subject', $this->site_name.' Payment Successful'))
                ->with(array_merge(
                    $this->payment_details,
                    request()->input()
                ))
                ->template($form.'-success');
            if ($this->from_name != '') {
                $email->from($this->from_email, $this->from_name);
            } elseif ($this->admin_name != '') {
                $email->from($this->admin_email, $this->admin_name);
            } else {
                $email->from($this->admin_email);
            }
            if ($this->bcc_email != '') {
                $email->bcc($this->bcc_email);
            }
            $email->send();
        }
        // Send a success email to the site admins
        $email = Email::create();
        $email->to($this->admin_email)
            ->subject(request()->input('admin_subject', 'Successful Payment by '.$this->payment_details['name']))
            ->with(array_merge(
                $this->payment_details,
                request()->input()
            ))
            ->template($form.'-admin');
        if ($this->from_name != '') {
            $email->from($this->from_email, $this->from_name);
        } elseif ($this->admin_name != '') {
            $email->from($this->admin_email, $this->admin_name);
        } else {
            $email->from($this->admin_email);
        }
        if ($this->bcc_email != '') {
            $email->bcc($this->bcc_email);
        }
        $email->send();
    }

    public function processStripe($payment, $token) {
        // Set our secret key
        Stripe::setApiKey($this->stripe_priv_key);

        // Create the charge on Stripe's servers - this will charge the user's card
        $success = TRUE;
        $fatal = FALSE;
        $error = '';
        try {
            $charge = Charge::create(array(
                "amount" => $payment['amount'] * 100, // amount in cents, again
                "currency" => $payment['currency'] ?? 'aud',
                "source" => $token,
                "description" => $payment['desc'].' for '.$this->payment_details['email'],
                "metadata" => array('name' => $this->payment_details['name'], 'email' => $this->payment_details['email'])
            ));
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            $success = FALSE;
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error = 'ERROR: An error occurred processing the payment. The returned message was: '.$err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an invalid data error. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error authenticating with the payment server. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error contacting the payment server.';
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        }
        if ($success) {
            // Build our data set
            $this->setPaymentDetail('timestamp', $charge['created']);
            $this->setPaymentDetail('trans_id', 'str-'.$charge['id']);
            $this->setTransCount();
            $this->setPaymentDetail('gateway_vars', array('card' => array(
                'type' => $charge['source']['brand'],
                'account' => $charge['source']['funding'],
                'ending' => $charge['source']['last4']
            )));
            // Return success
            return TRUE;
        } else {
            // Check if this was a user or system error
            if ($fatal) {
                // Message the site creators with the error message (and extra details?)
                if (method_exists($e, 'getJsonBody')) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                } else {
                    $err['type'] = '[unknown]';
                    $err['message'] = $error;
                }
                $output = array('Payment processing error' => $error,
                    'Amount' => $payment['amount'],
                    'Desc' => $payment['desc'],
                    'Request ID' => (method_exists($e, 'getRequestId') ? $e->getRequestId() : ''),
                    'Status' => (method_exists($e, 'getHttpStatus') ? $e->getHttpStatus()  : ''),
                    'Type' => $err['type'],
                    'Code' => (isset($err['code']) ? $err['code'] : ''),
                    'Param' => (isset($err['param']) ? $err['param'] : ''),
                    'Message' => (isset($err['message']) ? $err['message'] : '')
                );
                // Send our email
                $this->sendAdminEmail($output, 'Payment processing error!', $this->admin_email);
                // Add an extra bit to our returned message
                $error .= ' Your payment has not been processed. Please try again later.';
            }
            Log::error($error);
            Session::flash('payment_errors', $error);
            return FALSE;
        }

    }

    public function processStripeCustomer($name, $email, $token) {
        $success = TRUE;
        $fatal = FALSE;
        $error = '';

        // Set our secret key
        Stripe::setApiKey($this->stripe_priv_key);

        // Create the customer on Stripe's servers - this will store the customer but not charge anything
        try {
            $customer = Customer::create(array(
                "source" => $token,
                "email" => $email,
                "description" => $name
            ));
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            $success = FALSE;
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error = 'ERROR: An error occurred processing the payment. The returned message was: '.$err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an invalid data error. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error authenticating with the payment server. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error contacting the payment server.';
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        }
        if ($success) {
            return $customer;
        } else {
            // Check if this was a user or system error
            if ($fatal) {
                // Message the site creators with the error message (and extra details?)
                if (method_exists($e, 'getJsonBody')) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                } else {
                    $err['type'] = '[unknown]';
                    $err['message'] = $error;
                }
                $output = array('Payment processing error' => $error,
                    'Subscription Plan' => $plan,
                    'Request ID' => (method_exists($e, 'getRequestId') ? $e->getRequestId() : ''),
                    'Status' => (method_exists($e, 'getHttpStatus') ? $e->getHttpStatus()  : ''),
                    'Type' => $err['type'],
                    'Code' => (isset($err['code']) ? $err['code'] : ''),
                    'Param' => (isset($err['param']) ? $err['param'] : ''),
                    'Message' => (isset($err['message']) ? $err['message'] : '')
                );
                // Send our email
                $this->sendAdminEmail($output, 'Payment processing error!', $this->admin_email);
                // Add an extra bit to our returned message
                $error .= ' Your payment has not been processed. Please try again later.';
            }
            Log::error($error);
            return $error;
        }
    }

    public function processStripeSubscription($customer, $plan, $quantity) {
        // Set our secret key
        Stripe::setApiKey($this->stripe_priv_key);

        // Create the charge on Stripe's servers - this will charge the user's card
        $success = TRUE;
        $fatal = FALSE;
        $error = '';
        try {
            $subscription = Subscription::create([
              "customer" => $customer,
              "items" => [
                [
                  "plan" => $plan,
                  "quantity" => $quantity
                ],
              ],
              "metadata" => [
                'name' => $this->payment_details['name'],
                'email' => $this->payment_details['email']
              ]
            ]);
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            $success = FALSE;
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error = 'ERROR: An error occurred processing the payment. The returned message was: '.$err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an invalid data error. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error authenticating with the payment server. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: There was an error contacting the payment server.';
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $success = FALSE;
            $fatal = TRUE;
            $error = 'ERROR: Unknown error occurred processing the payment. We have been notified of this error and will attempt to fix it as soon as possible.';
        }
        if ($success) {
            // Build our data set
            $this->setPaymentDetail('timestamp', $subscription['created']);
            $this->setPaymentDetail('trans_id', 'str-'.$subscription['id']);
            $this->setTransCount();
            $this->setPaymentDetail('gateway_vars', array(
                'customer' => $subscription['customer'],
                'subscription' => $subscription['items']['data'][0]['subscription'],
                'quantity' => $subscription['items']['data'][0]['quantity'],
                'plan' => array(
                    'id' => $subscription['plan']['id'],
                    'amount' => $subscription['plan']['amount'],
                    'product' => $subscription['plan']['product'],
                    'interval' => $subscription['plan']['interval'],
                    'interval_count' => $subscription['plan']['interval_count']
                )
            ));
            // Return success
            return TRUE;
        } else {
            // Check if this was a user or system error
            if ($fatal) {
                // Message the site creators with the error message (and extra details?)
                if (method_exists($e, 'getJsonBody')) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                } else {
                    $err['type'] = '[unknown]';
                    $err['message'] = $error;
                }
                $output = array('Payment processing error' => $error,
                    'Customer' => $customer,
                    'Subscription Plan' => $plan,
                    'Request ID' => (method_exists($e, 'getRequestId') ? $e->getRequestId() : ''),
                    'Status' => (method_exists($e, 'getHttpStatus') ? $e->getHttpStatus()  : ''),
                    'Type' => $err['type'],
                    'Code' => (isset($err['code']) ? $err['code'] : ''),
                    'Param' => (isset($err['param']) ? $err['param'] : ''),
                    'Message' => (isset($err['message']) ? $err['message'] : '')
                );
                // Send our email
                $this->sendAdminEmail($output, 'Payment processing error!', $this->admin_email);
                // Add an extra bit to our returned message
                $error .= ' Your payment has not been processed. Please try again later.';
            }
            Log::error($error);
            Session::flash('payment_errors', $error);
            return FALSE;
        }

    }

    public function updateTransaction($data) {
        // Grab our filename and remove it from the array
        $filename = $data['filename'];
        unset($data['filename']);
        // Ensure our updated data is trimmed
        array_walk_recursive($data, 'self::trimArray');
        // Update this transaction
        $this->storage->putYAML($filename, $data);
    }

    public function getTransactions() {
        $transactions = [];
        $gateways = [];
        foreach (GlobalSet::whereHandle('gvcustomform')->get('cf_payment_gateways') as $gateway) {
            $gateways[$gateway['gateway_value']] = $gateway;
        }
        // Get our list of transaction record files
        $files = Folder::disk('storage')->getFiles('/addons/Payamic/');
        // Load just our transactions
        foreach ($files as $file) {
            if (substr($file, 15, 6) == 'trans-') {
                $transaction = $this->storage->getYAML(substr($file, 15, -5));
                $transaction['filename'] = substr($file, 15, -5);
                $transaction['gateway_pretty'] = $gateways[$transaction['gateway']]['gateway_name'] ?? ucfirst($transaction['gateway']);
                $transactions[$transaction['trans_id']] = $transaction;
            }
        }
        return $transactions;
    }

    public function getOrders() {
        // Load our transactions
        $transactions = collect($this->getTransactions());
        // Filter the list for just our orders
        return $transactions->filter(function ($value) {
            return isset($value['order_status']);
        });
    }

    public function updateOrder($id, $status) {
        // Load our orders
        $transactions = collect($this->getTransactions());
        // Find our order
        $order = $transactions->get($id);
        if (empty($order)) {
            $this->error = 'Order ID not found';
            return FALSE;
        }
        // Update the status field
        $order['order_status'] = $status;
        // Save our file
        $this->updateTransaction($order);
        return TRUE;
    }

    public function getOrderStatuses() {
        // Attempt to get our globals location from the config
        list($global, $var) = explode('.', $this->getConfig('order_statuses_global', '.'));
        if (isset($global) && $global != '' && isset($var) && $var != '') {
            // Attempt to load any vouchers from the global
            return collect(GlobalSet::whereHandle($global)->get($var, []));
        }
        return collect([]);
    }

    public function updateItemQuantities() {
        // Check if this is a cart or single item payment
        if ($this->payment_details['item_id'] == 'cart') {
            // Loop over our cart items and attempt to update them
            $cart = collect(request()->session()->get('cart'));
            $cart->each(function($item, $key) {
                if (isset($item['itemid'])) {
                    $this->updateItemQuantity($item['itemid'], $item['quantity']);
                }
            });
        } else {
            $this->updateItemQuantity($this->payment_details['item_id'], $this->payment_details['quantity']);
        }
    }

    private function updateItemQuantity($item, $inQty) {
        // Check if this item is an entry
        $entry = \Statamic\API\Content::find($item);
        if (!$entry) {
            // Not an entry so ignore it
            return;
        }
        // Check if we have a quantity field and it is greater than 0
        $qty = $entry->get('quantity');
        if ($qty <= 0) {
            return;
        }
        $outQty = $qty - $inQty;
        if ($outQty < 0) {
            // We shouldn't get here, but wth
            $outQty = 0;
        }
        // Update and save our entry
        $entry->set('quantity', $outQty)
            ->save();
    }

    public function updateUser($in_type = '', $user = NULL, $gateway) {
        // Build a list of our memebership types
        $types = $this->getMembershipPrices();

        // Check if we have a type set
        if (is_null($user)) {
            $user = User::getCurrent();
        }
        $type = $user->get('membership_type');
        if (is_null($type) || $type == '') {
            // There is no type set yet, so set it based on the item
            $user->set('membership_type', $in_type);
            // Set our membership as valid for 1 year from now
            $valid_until = new DateTime();
        } elseif (!isset($types[$type]['anniversary'])) {
            // Calculate our new date as 1 year from the current subscription end
            $valid_until = new DateTime($user->get('valid_until'));
        } else {
            // Calculate our new date as 1 year from the base date
            $valid_until = DateTime::createFromFormat('m-d', $types[$type]['anniversary']);
        }
        $valid_until->add(new DateInterval('P'.$types[$type]['duration']));
        $user->set('valid_until', $valid_until->format('Y-m-d'));
        // Update our user status
        $user->set('member_status', $gateway == 'stripe' ? 'active' : 'pending');
        // Save our updated user
        $user->save();
    }

    public function createBooking($payment) {
        // Create our new entry with the requested data
        $cottage = \Statamic\API\Entry::find(request()->input('cottage_id'), 'cottages');
        $title = $cottage->get('title').' for '.$this->getName(request()->input()).' '.request()->input('start_date').' to '.request()->input('end_date');
        $entry = \Statamic\API\Entry::create(str_slug($title, '-'))
            ->collection('bookings')
            ->with([
                    'title' => $title,
                    'cottage' => request()->input('cottage_id'),
                    'start' => request()->input('start_date'),
                    'end' => request()->input('end_date'),
                    'booking_status' => ($this->payment_details['gateway'] == 'offline' ? 'pending' : 'active'),
                    'price' => $payment['amount'],
                    'name' => $this->getName(request()->input()),
                    'email' => request()->input('email'),
                    'phone' => request()->input('phone'),
                    'address' => request()->input('address'),
                    'num_guests' => request()->input('num_guests'),
                    'pets' => request()->input('pets'),
                    'special_reqs' => request()->input('special_reqs'),
                    'booking_date' => Carbon::now()->format('Y-m-d')
                ])
            ->date(request()->input('start_date'))
            ->get();
        // Save this entry
        $entry->save();
    }

    public function checkBookings($date) {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            $error = 'ERROR! Failure loading our vars. Please check the logs for details.';
            Log::error($error);
            echo $error;
            return;
        }

        // Get our booking expiry times
        $times = explode(',', $this->getConfig('reminder_times', '7D, 2D'));
        // Check we have both times
        if (empty($times)) {
            $times = ['7D, 2D'];
        } elseif (!isset($times[1])) {
            $times[1] = '2D';
        }

        // Get our pending bookings
        $bookings = \Statamic\API\Entry::whereCollection('bookings')
            ->conditions(['booking_status:is' => 'pending']);
        echo "Pending bookings: ".count($bookings).PHP_EOL.PHP_EOL;

        // Check if we need to expire or send a notification for any bookings
        $today = Carbon::now()->startOfDay();
        $notify = FALSE;
        foreach ($bookings as $booking) {
            echo $booking->get('title').PHP_EOL;
            // Check if we need to notify for this booking
            $bdate = Carbon::parse($booking->get('booking-date'));
            if ($today->format('Y-m-d') == $bdate->add(new CarbonInterval('P'.trim($times[1])))->format('Y-m-d')) {
                $notify = TRUE;
            }

            // Check if this booking needs to be expired
            $bdate = Carbon::parse($booking->get('booking-date'))->startOfDay();
            if ($today >= $bdate->add(new CarbonInterval('P'.trim($times[0])))) {
                // Expire this booking
                echo " - Expiring this booking".PHP_EOL;
                $booking-set('booking_status', 'cancelled');
                $booking->save();
            }
        }

        if ($notify) {
            // Send our notification email
            echo PHP_EOL."Sending reminder email".PHP_EOL;
            $email = Email::create();
            $email->to($this->admin_email)
                ->subject('Pending Bookings Reminder')
                ->with($bookings->toArray())
                ->template($this->email_template_bookings_reminder)
                ->from($this->from_email, $this->from_name)
                ->send();
        }
    }

    public function checkSubs($date) {
        // Load our global vars
        if (!$this->loadVars()) {
            // There was an error loading our vars so crash
            $error = 'ERROR! Failure loading our vars. Please check the logs for details.';
            Log::error($error);
            echo $error;
            return;
        }

        // Get our subs reminder times
        $times = explode(',', $this->getConfig('subs_reminder_times', '30D, 14D, 7D'));

        // Get a list of our memebership types and check for anniversary reminders
        $types = $this->getMembershipPrices();
        $bases = [];
        foreach ($types as $type) {
            // Check if this type has a common anniversary and if this is a reminder date
            if (isset($type['anniversary'])) {
                // Check if the input date matches one of the reminder times before our base dates
                $base = DateTime::createFromFormat('m-d', $type['anniversary']);
                echo "Date: ".$base->format('Y-m-d')."\n";
                foreach ($times as $time) {
                    $base_test = clone $base;
                    if ($date->format('Y-m-d') == $base_test->sub(new DateInterval('P'.trim($time)))->format('Y-m-d')) {
                        echo "Test Date: ".$base_test->format('Y-m-d')."\n";
                        $bases[$type['value']] = [
                            'date' => $base,
                            'time' => $time
                        ];
                    }
                }
            }
        }
        print_r($bases);

        // Loop over all the members
        foreach (User::all() as $member) {
            $type = $member->get('membership_type');
            echo $member->username()." - ".$type."\n";
            // Check this is a member type we are checking and if they are active (not expired or with a payment awaiting confirmation)
            if (!array_key_exists($type, $types) || $member->get('member_status') != 'active') {
                echo "Not our type, or inactive, bailing.\n";
                continue;
            }
            $valid_until = new DateTime($member->get('valid_until'));
            echo 'User valid_until: '.$valid_until->format('Y-m-d')."\n";
            // Check for subscription reminders
            if (!isset($types[$type]['anniversary'])) {
                echo "Got normal user\n";
                // Loop over all the reminder dates
                foreach($times as $time) {
                    // Check if $date is $reminder_time before their $valid_until date
                    $reminder = clone $valid_until;
                    $reminder->sub(new DateInterval('P'.trim($time)));
                    if ($date->format('Y-m-d') == $reminder->format('Y-m-d')) {
                        // Send our notification to this member
                        echo "This is ".$member->username()."'s ".$time." reminder date (".$reminder->format('d-m-Y').") so send the reminder.\n";
                        $this->sendSubsReminder($member, $valid_until);
                        break;
                    }
                }
            } else {
                echo "Got special user\n";
                // Check if this is a special reminder day for this member type, and if our valid until date is within 180 days (due or expired)
                if (isset($bases[$type])) {
                    // Send our notification to this member
                    echo "This is a special member (".$member->username()." - ".$type.") and today is the ".$bases[$type]['time']." special reminder day, so send their reminder\n";
                    $this->sendSubsReminder($member, $bases[$type]['date']);
                }
            }
            // Check for expired memberships
            if ($valid_until < $date) {
                echo "Until: ".$valid_until->format('d-m-Y').', Date: '.$date->format('d-m-Y')."\n";
                // Set our member status to "inactive"
                echo $member->username()."'s membership has just expired so flag them inactive and send the reminder.\n";
                $member->set('member_status', 'inactive');
                $member->save();
                $this->sendSubsReminder($member, $valid_until);
            }
        }
    }

    public function sendSubsReminder($member, $date) {
        // Build our email
        print "Sending email with this template: ".$this->email_template_subscription_due."\n";
        $prices = $this->getMembershipPrices();
        $amount = money_format('%.2n', $prices[$member->get('membership_type')]['price']);
        $email = Email::create();
        $email->to($member->email())
            ->subject($this->site_name.' Membership Reminder')
            ->with(array_merge(
                $member->data(),
                array('email' => $member->email(), 'due_date' => $date->format('d-m-Y'), 'amount' => $amount)
            ))
            ->template($this->email_template_subscription_due);
        if ($this->from_name != '') {
            $email->from($this->from_email, $this->from_name);
        } elseif ($this->admin_name != '') {
            $email->from($this->admin_email, $this->admin_name);
        } else {
            $email->from($this->admin_email);
        }
        $email->send();
    }

    /**
     * Calculate the amount and total for this booking and build the payment string
     *
     * @return array
     */
    public function calcBookingDetails() {
        $error = '';
        // Check if our booking is valid (right details and available dates)
        $error = $this->validateBooking();
        // Calculate our price details
        if ($error == '') {
            // Parse our vars
            $cottage = \Statamic\API\Entry::whereSlug($this->getCottage(), 'cottages');
            $start = Carbon::parse(request()->input('min_date'))->startOfDay();
            $end = Carbon::parse(request()->input('max_date'))->startOfDay()->subDay();

            $rates = $cottage->get('rates');
            foreach ($rates as &$rate) {
                $rate['start'] = Carbon::parse($rate['start']);
                $rate['end'] = Carbon::parse($rate['end']);
                $rate['num_days'] = $rate['start']->diffInDays($rate['end']);
            }
            $rates = collect($rates);
            // Build an array of dates for this booking
            $dates = CarbonPeriod::create($start, $end);
            // Loop over each day checking which rate block it falls under
            $num_days = $start->diffInDays($end);
            $price = 0;
            foreach ($dates as $date) {
                // Find the matching rates blocks for this date and filter to the one with the shortest period
                $dRate = $rates->filter(function($rate) use ($date) {
                    return $date->between($rate['start'], $rate['end']);
                })->sortBy('num_days')->first();
                // Check if this date is in a "special" rate section (must be booked for more than 1 day)
                if (isset($dRate['nights']) && $dRate['nights'] > 1) {
                    // This is a "special" rate so check if this is the first day of the period (either the stay
                    // or the rate)
                    if ($date == $start || $date == $dRate['start']) {
                        // Check if we have enough dates to satisfy the conditions
                        if ($date->diffInDays($end) + 1 < $dRate['nights']) {
                            $error .= 'Special rate period not covered'.PHP_EOL;
                            break;
                        }
                    }
                }
                // Check if this day is within a full week
                // 0-count Day num divided by 7 days (week 1 = 0, week 2 = 1) plus 1, multiplied by 7
                $dayNum = $start->diffInDays($date);
                if (isset($dRate['week']) && $dRate['week'] > 0 && (intdiv($dayNum, 7) + 1) * 7 <= $num_days) {
                    // Add the cost of one day of the weekly price
                    $price += $dRate['week'] / 7;
                } else {
                    // Add the price for this day
                    $price += $dRate[strtolower($date->format('D'))];
                }
            }
        }
        // Check if we have an error and build our output vars
        if ($error == '') {
            $val = $this->getConfig('stripe_levy');
            $levy = (isset($val) && $val != '' && floatval($val) > 1 ? floatval($val) : 0);

            $vars = [
                'amount' => number_format($price, 2, '.', ''),
                'total' => number_format($price + ($price * $levy / 100), 2, '.', ''),
                'desc' => 'Booking for '.$cottage->get('title').' for '.$start->format('d-m-Y').' to '.$end->addDay()->format('d-m-Y'),
                'item_id' => $start->format('Ymd').'_'.$cottage->get('id'),
                'nights' => $start->diffInDays($end),
                'error' => '',
                'fatal' => FALSE
            ];
            $vars['payment'] = $this->createPaymentStr($vars['amount'], $vars['item_id'], $vars['desc']);
        } else {
            // Set our error values
            $vars = [
                'payment' => 'Error',
                'total' => 'Error',
                'amount' => 'Error',
                'desc' => 'Error',
                'item_id' => 'Error',
                'nights' => 'Error',
                'error' => $error,
                'fatal' => TRUE
            ];
        }
        return $vars;
    }

    public function validateBooking() {
        $error = '';
        // Check we have a valid cottage ID
        $cottage = \Statamic\API\Entry::whereSlug($this->getCottage(), 'cottages');
        if (is_null($cottage)) {
            $error .= 'Invalid or missing Cottage ID'.PHP_EOL;
        }
        // Check we have a start date
        if (is_null(request()->input('min_date'))) {
            $error .= 'Start date is invalid'.PHP_EOL;
        } else {
            $start = Carbon::createFromFormat('Y-m-d', request()->input('min_date'));
            if (is_null($start)) {
                $error .= 'Start date is invalid'.PHP_EOL;
            } else {
                $start = $start->startOfDay();
                // Check the start date is today or later
                if ($start < Carbon::now()->startOfDay()) {
                    $error .= 'Start date is earlier than today';
                }
            }
        }
        // Check we have an end date
        if (is_null(request()->input('max_date'))) {
            $error .= 'End date is invalid'.PHP_EOL;
        } else {
            $end = Carbon::createFromFormat('Y-m-d', request()->input('max_date'));
            if (is_null($end)) {
                $error .= 'End date is invalid'.PHP_EOL;
            } else {
                $end = $end->startOfDay();
                // Check the end date is after the start date
                if ($end <= $start) {
                    $error .= 'End date is before or the same as start date'.PHP_EOL;
                }
            }
        }
        // Check we have a number of guests (do we need it?)
        // Check we have a number of rooms (do we need it?)
        // Bail our if we don't have valid values for parsing bookings
        if ($error != '') {
            return $error;
        }
        // Check if the cottage is available (no overlapping booking)
        $bookings = \Statamic\API\Entry::whereCollection('bookings');
//        print "Start: ".$start.", End: ".$end."<br>\n";
        $bookings = $bookings->filter(function($booking) use ($cottage, $start, $end) {
            // Remove bookings not for this cottage
            if ($booking->get('cottage') != $cottage->id()) {
                return FALSE;
            }
            // Remove bookings that are cancelled
            if ($booking->get('booking_status') == 'cancelled') {
                return FALSE;
            }
            // Remove bookings before or after
            $bStart = Carbon::parse($booking->get('start'))->startOfDay();
            $bEnd = Carbon::parse($booking->get('end'))->startOfDay();
//            print "Start: ".$bStart.", End: ".$bEnd.", Before? ".($bEnd <= $start).", After? ".($bStart >= $end)."<br>\n";
            return !($bEnd <= $start || $bStart >= $end);
        });
        if ($bookings->count() > 0) {
            $error = 'Selected booking dates are not available';
        }
        return $error;
    }

    /**
     * Search our user list for the current highest user number and increment
     *
     * @return int
     */
    public function findNextUserNum() {
        $users = User::all();
        $num = 1000;
        foreach ($users as $user) {
            if (intval($user->get('user_num')) > $num) {
                $num = intval($user->get('user_num'));
            }
        }
        return $num + 1;
    }

    public function getName($data) {
        // Look for first and last name compoments in common field names
        $name = $data['name'] ?? '';
        if ($name == '') {
            $name = $data['firstname'] ?? '';
        }
        if ($name == '') {
            $name = $data['first_name'] ?? '';
        }
        if (isset($data['surname'])) {
            $name .= ' '.$data['surname'];
        } elseif (isset($data['lastname'])) {
            $name .= ' '.$data['lastname'] ?? '';
        } elseif (isset($data['last_name'])) {
            $name .= ' '.$data['last_name'] ?? '';
        }

        return $name;
    }

    public function getMembershipPrices() {
        $types = [];
        $prices = GlobalSet::whereHandle('gvcustommembers')->get('membership_prices');
        foreach ($prices as $price) {
            $types[$price['value']] = $price;
        }
        return $types;
    }

    public function getCottage() {
        // Look for the cottage slug in the input vars and the URL
        $segments = request()->segments();
        return request()->input('cottage', array_pop($segments));
    }

    public function parseTextBlob($text) {
        // Split our blob into lines
        $lines = preg_split("/\n|,/", $text);
        // Trim our lines
        array_walk_recursive($lines, 'self::trimArray');
        // Recombine our blob, split on the <br>'s and trim again
        $lines = explode('<br>', join(' ', $lines));
        array_walk_recursive($lines, 'self::trimArray');
        // Recombine with new lines and strip any remaining tags
        return strip_tags(join("\n", $lines));
    }

    // Trim all the data
    static function trimArray(&$item, $key) {
        $item = trim($item);
    }

    // Return our currency symbol
    static public function getCurSymbol($cur) {
        switch (strtolower($cur)) {
            case 'gbp':
                return '£';
            case 'eur':
                return '€';
            default:
                return '$';
        }
    }
}
