<?php

namespace Statamic\Addons\Variables;

use Statamic\Extend\Tags;

class VariablesTags extends Tags
{
    /**
     * Sets a variable
     *
     * {{ var:set name="key" value="value" }}
     */
    public function set()
    {
        $this->store($this->get('name'), $this->getValueParam());
    }

    /**
     * Outputs (or gets) a variable
     *
     * {{ var:output name="key" }}
     *
     * @return string
     */
    public function output()
    {
        return $this->retrieve($this->get('name'));
    }

    /**
     * Get or set a variable
     *
     * {{ var:with name="key" value="value" default="fallback" }}
     * or
     * {{ var:with name="key" default="fallback }}value{{ /var:with }}
     *
     * @return string|void
     */
    public function with()
    {
        return $this->storeOrRetrieve($this->get('name'));
    }

    /**
     * Magic method for getting/setting variables with a shorter syntax
     *
     * Just like {{ var:with }}, but the `name` parameter is replaced by the
     * second tag part, ie. {{ var:name }}
     *
     * @param string $key
     * @param array  $args
     * @return string|void
     */
    public function __call($key, $args)
    {
        $key = explode(':', $this->tag, 2)[1];

        return $this->storeOrRetrieve($key);
    }

    /**
     * Append a value to an existing variable
     *
     * {{ var:append to="key" value="value" }}
     *
     * @return void
     */
    public function append()
    {
        $key = $this->get(['name', 'to']);

        $value = $this->retrieve($key);

        $value .= $this->getValueParam();

        $this->store($key, $value);
    }

    /**
     * Show content if a variable exists
     *
     * {{ var:exists name="key" }} ... {{ /var:exists }}
     *
     * @return string|void
     */
    public function exists()
    {
        if ($this->blink->exists($this->get('name'))) {
            return $this->parse([]);
        }
    }

    /**
     * Extract variables into the current context
     *
     * {{ var:color is="green" }}
     * {{ var:extract }}
     *     Do you like my new {{ color }} tie?
     * {{ /var:extract }}
     * => Do you like my new green tie?
     *
     * @return string
     */
    public function extract()
    {
        return $this->parse($this->blink->all());
    }

    /**
     * Either store or retrieve a variable
     *
     * @param $key
     * @return string|void
     */
    protected function storeOrRetrieve($key)
    {
        $value = $this->getValueParam();
        $default = $this->get('default');
        
        if (is_null($value)) {
            return $this->retrieve($key, $default);
        } else {
            $this->store($key, $value);
        }
    }

    /**
     * Store a variable
     *
     * @param string $key
     * @param mixed  $value
     */
    protected function store($key, $value)
    {
        $this->blink->put($key, $value);
    }

    /**
     * Retrieve a variable
     *
     * @param string     $key
     * @param mixed|null $default
     * @return string
     */
    protected function retrieve($key, $default = null)
    {
        return $this->blink->get($key, $default);
    }

    /**
     * Get a value either from a parameter or by the tag pair content
     *
     * @return string
     */
    protected function getValueParam()
    {
        if ($this->isPair) {
            $value = trim($this->parse([]));
        } else {
            $value = $this->get(['is', 'value', 'val']);
        }

        return $value;
    }
}
